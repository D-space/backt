namespace backt
{

    #region Enumerations

    public enum Endianness
    {
        LittleEndian,
        BigEndian,
        Default
    }

    #endregion Enumerations
}