using System;
using System.IO;
using System.Text;
using System.Linq;

namespace backt
{
    public static class MITCHParser
    {

        public static void ParseMessage(ExtractionMessage? messageWithHeader)
        {


            using (var memoryStream = new MemoryStream(messageWithHeader.Value.Data)){
                using (var dataBinaryReader = new BinaryReader(memoryStream)){
                    // Each case handles a different type of message
                    // The Message types can be found in the attached specification.
                    switch(messageWithHeader.Value.MessageType){
                        case "53":
                            var systemEventMessageBody = System_EventMessage.BuildSystem_EventMessage(dataBinaryReader, messageWithHeader.Value.Id);
                            // var systemEventMessageBodyJSON = JsonConvert.SerializeObject(systemEventMessageBody);

                            Console.WriteLine(systemEventMessageBody);
                            Console.WriteLine(systemEventMessageBody.Message_Type);

                            // writer.WriteLine(systemEventMessageBodyJSON);
                            // writer.WriteLine(",");
                            break;
                        case "54":
                            var timeMessageBody = TimeMessage.BuildTimeMessage(dataBinaryReader, messageWithHeader.Value.Id);
                            // var timeMessageBodyJSON = JsonConvert.SerializeObject(timeMessageBody);

                            Console.WriteLine(timeMessageBody.Message_Type);
                            Console.WriteLine(timeMessageBody.Seconds);

                            // writer.WriteLine(timeMessageBodyJSON);
                            // writer.WriteLine(",");
                            break;
                        case "41":
                            var addOrderMessageBody = Add_OrderMessage.BuildAdd_OrderMessage(dataBinaryReader, messageWithHeader.Value.Id);
                            // var addOrderMessageBodyJSON = JsonConvert.SerializeObject(addOrderMessageBody);
                            //Console.WriteLine(addOrderMessageBodyJSON);
                            // Console.WriteLine(addOrderMessageBody.OrderID);
                            // Console.WriteLine(addOrderMessageBody.MessageTypeString);
                            // Console.WriteLine(addOrderMessageBody.MessageType);
                            DisplayMessage(addOrderMessageBody);

                            // Try write addorder to db
                            // qdb.TestInsertAddOrder(addOrderMessageBody, messageWithHeader.Value.Id);
                            // writer.WriteLine(",");
                            break;
                        // case "46":
                        //     var addAttributedOrderMessageBody = AddAttributedOrderMessage.BuildAddAttributedOrderMessage(dataBinaryReader, messageWithHeader.Value.Id);
                        //     var addAttributedOrderMessageBodyJSON = JsonConvert.SerializeObject(addAttributedOrderMessageBody);
                        //     //Console.WriteLine(addAttributedOrderMessageBodyJSON);
                        //     writer.WriteLine(addAttributedOrderMessageBodyJSON);
                        //     writer.WriteLine(",");
                        //     break;
                        case "52":
                            var symbolDirectoryMessageBody = Symbol_DirectoryMessage.BuildSymbol_DirectoryMessage(dataBinaryReader, messageWithHeader.Value.Id);
                            //Console.WriteLine(symbolDirectoryMessageBodyJSON);
                            // writer.WriteLine(symbolDirectoryMessageBodyJSON);
                            DisplayMessage(symbolDirectoryMessageBody);

                            // writer.WriteLine(",");
                            break;
                        // case "44":
                        //     var orderDeletedMessageBody = OrderDeletedMessage.BuildOrderDeletedMessage(dataBinaryReader, messageWithHeader.Value.Id);
                        //     var orderDeletedMessageBodyJSON = JsonConvert.SerializeObject(orderDeletedMessageBody);
                        //     //Console.WriteLine(orderDeletedMessageBodyJSON);
                        //     writer.WriteLine(orderDeletedMessageBodyJSON);
                        //     writer.WriteLine(",");
                        //     break;
                        // case "55":
                        //     var orderModifiedMessageBody = OrderModifiedMessage.BuildOrderModifiedMessage(dataBinaryReader,messageWithHeader.Value.Id);
                        //     var orderModifiedMessageBodyJSON = JsonConvert.SerializeObject(orderModifiedMessageBody);
                        //     //Console.WriteLine(orderModifiedMessageBodyJSON);
                        //     writer.WriteLine(orderModifiedMessageBodyJSON);
                        //     writer.WriteLine(",");
                        //     break;
                        // case "79":
                        //     var orderBookClearMessageBody = OrderBookClearMessage.BuildOrderBookClearMessage(dataBinaryReader, messageWithHeader.Value.Id);
                        //     var orderBookClearMessageBodyJSON =  JsonConvert.SerializeObject(orderBookClearMessageBody);
                        //     //Console.WriteLine(orderBookClearMessageBodyJSON);
                        //     writer.WriteLine(orderBookClearMessageBodyJSON);
                        //     writer.WriteLine(",");
                        //     break;
                        // case "45":
                        //     var orderExecutedMessageBody = OrderExecutedMessage.BuildOrderExecutedMessage(dataBinaryReader, messageWithHeader.Value.Id);
                        //     var orderExecutedMessageBodyJSON = JsonConvert.SerializeObject(orderExecutedMessageBody);
                        //     //Console.WriteLine(orderExecutedMessageBodyJSON);
                        //     writer.WriteLine(orderExecutedMessageBodyJSON);
                        //     writer.WriteLine(",");
                        //     break;
                        // case "43":
                        //     var orderExecutedWithPriceSizeMessageBody = OrderExecutedWithPriceSizeMessage.BuildOrderExecutedWithPriceSizeMessage(dataBinaryReader, messageWithHeader.Value.Id);
                        //     var orderExecutedWithPriceSizeMessageBodyJSON = JsonConvert.SerializeObject(orderExecutedWithPriceSizeMessageBody);
                        //     //Console.WriteLine(orderExecutedWithPriceSizeMessageBodyJSON);
                        //     writer.WriteLine(orderExecutedWithPriceSizeMessageBodyJSON);
                        //     writer.WriteLine(",");
                        //     break;
                        // case "50":
                        //     var tradeMessageBody = TradeMessage.BuildTradeMessage(dataBinaryReader, messageWithHeader.Value.Id);
                        //     var tradeMessageBodyJSON = JsonConvert.SerializeObject(tradeMessageBody);
                        //     //Console.WriteLine(tradeMessageBodyJSON);
                        //     writer.WriteLine(tradeMessageBodyJSON);
                        //     writer.WriteLine(",");
                        //     break;
                        // case "51":
                        //     var tradeAuctionMessageBody = AuctionTradeMessage.BuildAuctionTradeMessage(dataBinaryReader, messageWithHeader.Value.Id);
                        //     var tradeAuctionMessageBodyJSON = JsonConvert.SerializeObject(tradeAuctionMessageBody);
                        //     //Console.WriteLine(tradeAuctionMessageBodyJSON);
                        //     writer.WriteLine(tradeAuctionMessageBodyJSON);
                        //     writer.WriteLine(",");
                        //     break;
                        // case "78":
                        //     var offBookTradeMessageBody = OffBookTradeMessage.BuildOffBookTradeMessage(dataBinaryReader, messageWithHeader.Value.Id);
                        //     var offBookTradeMessageBodyJSON = JsonConvert.SerializeObject(offBookTradeMessageBody);
                        //     // Console.WriteLine(offBookTradeMessageBodyJSON);
                        //     writer.WriteLine(offBookTradeMessageBodyJSON);
                        //     writer.WriteLine(",");
                        //     break;
                        default:
                            Console.WriteLine("Default");
                            // var messageWithHeaderJSON = JsonConvert.SerializeObject(messageWithHeader);
                            // writer.WriteLine(messageWithHeaderJSON);
                            // writer.WriteLine(",");
                            break;
                    }
                }
            }

        }




        public static void DisplayMessage(Add_OrderMessage msg) {
            Console.WriteLine("Displaying AddOrder Message");
            var s = $@"
            Length = {msg.Length},
            MessageType = {msg.Message_Type},
            Nanosecond = {msg.Nanosecond},
            OrderID = {msg.Order_ID},
            Side = {msg.Side},
            Quantity  = {msg.Quantity},
            InstrumentID = {msg.Instrument_ID},
            Reserved = {msg.Reserved},
            Reserved1 = {msg.Reserved1},
            Price = {msg.Price},
            Flags = {msg.Flags},
            ";
            Console.WriteLine(s);
        }

        public static void DisplayMessage(Symbol_DirectoryMessage msg) {
            Console.WriteLine("Displaying SymbolDirectoryMessage Message");
            var s = $@"
            Length = {msg.Length},
            MessageType = {msg.Message_Type},
            Nanosecond = {msg.Nanosecond},
            InstrumentID = {msg.Instrument_ID},
            Reserved = {msg.Reserved},
            Reserved1 = {msg.Reserved1},
            SymbolStatus = {msg.Symbol_Status},
            ISIN = {msg.ISIN},
            Symbol = {msg.Symbol},
            TIDM = {msg.TIDM},
            Segment = {msg.Segment},
            PreviousClosePrice = {msg.Previous_Close_Price},
            ExpirationDate = {msg.Expiration_Date},
            Underlying = {msg.Underlying},
            StrikePrice = {msg.Strike_Price},
            OptionType = {msg.Option_Type},
            Issuer = {msg.Issuer},
            IssueDate = {msg.Issue_Date},
            Coupon = {msg.Coupon},
            Flags = {msg.Flags},
            SubBook = {msg.Sub_Book},
            CorporateAction = {msg.Corporate_Action},
            Leg1Symbol = {msg.Leg_1_Symbol},
            Leg2Symbol = {msg.Leg_2_Symbol},
            ContractMultiplier = {msg.Contract_Multiplier},
            SettlementMethod = {msg.Settlement_Method},
            InstrumentSubCategory = {msg.Instrument_Sub_Category},
            ";
            Console.WriteLine(s);
        }

    }
}
