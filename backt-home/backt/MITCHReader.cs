using System;
using System.IO;
using System.Text;
using System.Linq;

namespace backt
{
    public static class MITCHReader
    {

        const byte StartOfMessageIndicator = 255;
        static QuestConnGen qdb;


        static MITCHReader()
        {
            string username = "admin";
            string password = "quest";
            string database = "qdb";
            int port = 8812;

            Console.WriteLine("Connectong to quest");

            qdb = new QuestConnGen(username, password, database, port);
            // qdb.DeleteTables();
            // qdb.SetupTables();
            // qdb.Conn();
            Console.WriteLine("Done");

        }


        public static void StartCaptureSample(FileInfo deviceFile)
        {
            var messageCount = 0;
            using (var fileStream = new FileStream(deviceFile.FullName, FileMode.Open, FileAccess.Read, FileShare.Read))
            using (var binaryReader = new BinaryReader(fileStream))
            {
                do
                {
                    try
                    {
                        var messageWithHeader = GetMessageWithHeader(binaryReader);
                        if (messageWithHeader.HasValue)
                        {
                            var messageDataBuilder = new StringBuilder();
                            Array.ForEach(messageWithHeader.Value.Data, x =>
                                messageDataBuilder.Append(x));

                            Console.WriteLine("Message # : {0}", ++messageCount);

                            Console.WriteLine("MessageType : {0}, MessageLen: {1}, MessageId: {2}, Data:{3}", messageWithHeader.Value.MessageType
                                , messageWithHeader.Value.Length, messageWithHeader.Value.Id, messageWithHeader.Value.Data);


                            Console.WriteLine("Parsing mitch message");

                            MITCHParserGen.ParseMessage(messageWithHeader, qdb);

                        }
                    }
                    catch (EndOfStreamException)
                    {
                        break;
                    }
                // } while (true); Old
                } while (messageCount < 10000);
            }
        }



        public static ExtractionMessage? GetMessageWithHeader(BinaryReader binaryReader)
        {
            if (binaryReader.ReadByte() == StartOfMessageIndicator)
            {
                return ExtractionMessage.BuildExtractionMessage(binaryReader);
            }
            return null;
        }



        public static byte[] CorrectEndianness(this byte[] value, Endianness endianness)
        {
            // Method used by extraction message
            switch (endianness)
            {
                case Endianness.LittleEndian:
                    if (!BitConverter.IsLittleEndian) Array.Reverse(value);
                    break;

                case Endianness.BigEndian:
                    if (BitConverter.IsLittleEndian) Array.Reverse(value);
                    break;

                case Endianness.Default:
                    break;
            }
            return value;
        }









    }
}
