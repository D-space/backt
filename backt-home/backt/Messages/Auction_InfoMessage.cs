// This is a generated file.  Don't edit it directly!

using System;
using System.IO;
using System.Text;

namespace backt
{
    public struct Auction_InfoMessage
    {
        public ushort Length;
        public string Message_Type;
        public uint Nanosecond;
        public uint Paired_Quantity;
        public uint Reserved;
        public string Imbalance_Direction;
        public uint Instrument_ID;
        public string Reserved1;
        public string Reserved2;
        public long Price;
        public string Auction_Type;

        public static Auction_InfoMessage BuildAuction_InfoMessage(BinaryReader binaryReader, long headerId)
        {
            var UnitHeader = binaryReader.ReadBytes(8);
            

            var lengthBuffer = binaryReader.ReadBytes(2);
            var length = BitConverter.ToUInt16(lengthBuffer, 0);
            

            var message_typeBuffer = binaryReader.ReadBytes(1);
            var message_type = Encoding.ASCII.GetString(message_typeBuffer);
            

            var nanosecondBuffer = binaryReader.ReadBytes(4);
            var nanosecond = BitConverter.ToUInt32(nanosecondBuffer, 0);
            

            var paired_quantityBuffer = binaryReader.ReadBytes(4);
            var paired_quantity = BitConverter.ToUInt32(paired_quantityBuffer, 0);
            

            var reservedBuffer = binaryReader.ReadBytes(4);
            var reserved = BitConverter.ToUInt32(reservedBuffer, 0);
            

            var imbalance_directionBuffer = binaryReader.ReadBytes(1);
            var imbalance_direction = Encoding.ASCII.GetString(imbalance_directionBuffer);
            

            var instrument_idBuffer = binaryReader.ReadBytes(4);
            var instrument_id = BitConverter.ToUInt32(instrument_idBuffer, 0);
            

            var reserved1Buffer = binaryReader.ReadBytes(1);
            var reserved1 = Encoding.ASCII.GetString(reserved1Buffer);
            

            var reserved2Buffer = binaryReader.ReadBytes(1);
            var reserved2 = Encoding.ASCII.GetString(reserved2Buffer);
            

            var priceBuffer = binaryReader.ReadBytes(8);
            var price = BitConverter.ToInt64(priceBuffer);
            

            var auction_typeBuffer = binaryReader.ReadBytes(1);
            var auction_type = Encoding.ASCII.GetString(auction_typeBuffer);
            


            return new Auction_InfoMessage()
            {
                Length = length,
                Message_Type = message_type,
                Nanosecond = nanosecond,
                Paired_Quantity = paired_quantity,
                Reserved = reserved,
                Imbalance_Direction = imbalance_direction,
                Instrument_ID = instrument_id,
                Reserved1 = reserved1,
                Reserved2 = reserved2,
                Price = price,
                Auction_Type = auction_type,
            };
        }
    }
}
