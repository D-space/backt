// This is a generated file.  Don't edit it directly!

using System;
using System.IO;
using System.Text;

namespace backt
{
    public struct Login_RequestMessage
    {
        public ushort Length;
        public string Message_Type;
        public string Username;
        public string Password;

        public static Login_RequestMessage BuildLogin_RequestMessage(BinaryReader binaryReader, long headerId)
        {
            var UnitHeader = binaryReader.ReadBytes(8);
            

            var lengthBuffer = binaryReader.ReadBytes(2);
            var length = BitConverter.ToUInt16(lengthBuffer, 0);
            

            var message_typeBuffer = binaryReader.ReadBytes(1);
            var message_type = Encoding.ASCII.GetString(message_typeBuffer);
            

            var usernameBuffer = binaryReader.ReadBytes(6);
            var username = Encoding.ASCII.GetString(usernameBuffer);
            

            var passwordBuffer = binaryReader.ReadBytes(10);
            var password = Encoding.ASCII.GetString(passwordBuffer);
            


            return new Login_RequestMessage()
            {
                Length = length,
                Message_Type = message_type,
                Username = username,
                Password = password,
            };
        }
    }
}
