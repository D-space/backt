// This is a generated file.  Don't edit it directly!

using System;
using System.IO;
using System.Text;

namespace backt
{
    public struct Logout_RequestMessage
    {
        public ushort Length;
        public string Message_Type;

        public static Logout_RequestMessage BuildLogout_RequestMessage(BinaryReader binaryReader, long headerId)
        {
            var UnitHeader = binaryReader.ReadBytes(8);
            

            var lengthBuffer = binaryReader.ReadBytes(2);
            var length = BitConverter.ToUInt16(lengthBuffer, 0);
            

            var message_typeBuffer = binaryReader.ReadBytes(1);
            var message_type = Encoding.ASCII.GetString(message_typeBuffer);
            


            return new Logout_RequestMessage()
            {
                Length = length,
                Message_Type = message_type,
            };
        }
    }
}
