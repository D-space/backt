// This is a generated file.  Don't edit it directly!

using System;
using System.IO;
using System.Text;

namespace backt
{
    public struct Off_Book_TradeMessage
    {
        public ushort Length;
        public string Message_Type;
        public uint Nanosecond;
        public uint Executed_Quantity;
        public uint Instrument_ID;
        public string Reserved;
        public string Reserved1;
        public long Price;
        public ulong Trade_ID;
        public string Off_Book_Trade_Type;
        public string Trade_Time;
        public string Trade_Date;
        public long LastOptPx;
        public long Volatility;
        public long Underlying_Reference_Price;

        public static Off_Book_TradeMessage BuildOff_Book_TradeMessage(BinaryReader binaryReader, long headerId)
        {
            var UnitHeader = binaryReader.ReadBytes(8);
            

            var lengthBuffer = binaryReader.ReadBytes(2);
            var length = BitConverter.ToUInt16(lengthBuffer, 0);
            

            var message_typeBuffer = binaryReader.ReadBytes(1);
            var message_type = Encoding.ASCII.GetString(message_typeBuffer);
            

            var nanosecondBuffer = binaryReader.ReadBytes(4);
            var nanosecond = BitConverter.ToUInt32(nanosecondBuffer, 0);
            

            var executed_quantityBuffer = binaryReader.ReadBytes(4);
            var executed_quantity = BitConverter.ToUInt32(executed_quantityBuffer, 0);
            

            var instrument_idBuffer = binaryReader.ReadBytes(4);
            var instrument_id = BitConverter.ToUInt32(instrument_idBuffer, 0);
            

            var reservedBuffer = binaryReader.ReadBytes(1);
            var reserved = Encoding.ASCII.GetString(reservedBuffer);
            

            var reserved1Buffer = binaryReader.ReadBytes(1);
            var reserved1 = Encoding.ASCII.GetString(reserved1Buffer);
            

            var priceBuffer = binaryReader.ReadBytes(8);
            var price = BitConverter.ToInt64(priceBuffer);
            

            var trade_idBuffer = binaryReader.ReadBytes(8);
            var trade_id = BitConverter.ToUInt64(trade_idBuffer, 0);
            

            var off_book_trade_typeBuffer = binaryReader.ReadBytes(4);
            var off_book_trade_type = Encoding.ASCII.GetString(off_book_trade_typeBuffer);
            

            var trade_timeBuffer = binaryReader.ReadBytes(8);
            var trade_time = Encoding.ASCII.GetString(trade_timeBuffer);
            

            var trade_dateBuffer = binaryReader.ReadBytes(8);
            var trade_date = Encoding.ASCII.GetString(trade_dateBuffer);
            

            var lastoptpxBuffer = binaryReader.ReadBytes(8);
            var lastoptpx = BitConverter.ToInt64(lastoptpxBuffer);
            

            var volatilityBuffer = binaryReader.ReadBytes(8);
            var volatility = BitConverter.ToInt64(volatilityBuffer);
            

            var underlying_reference_priceBuffer = binaryReader.ReadBytes(8);
            var underlying_reference_price = BitConverter.ToInt64(underlying_reference_priceBuffer);
            


            return new Off_Book_TradeMessage()
            {
                Length = length,
                Message_Type = message_type,
                Nanosecond = nanosecond,
                Executed_Quantity = executed_quantity,
                Instrument_ID = instrument_id,
                Reserved = reserved,
                Reserved1 = reserved1,
                Price = price,
                Trade_ID = trade_id,
                Off_Book_Trade_Type = off_book_trade_type,
                Trade_Time = trade_time,
                Trade_Date = trade_date,
                LastOptPx = lastoptpx,
                Volatility = volatility,
                Underlying_Reference_Price = underlying_reference_price,
            };
        }
    }
}
