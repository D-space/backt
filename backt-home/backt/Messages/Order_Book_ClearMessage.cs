// This is a generated file.  Don't edit it directly!

using System;
using System.IO;
using System.Text;

namespace backt
{
    public struct Order_Book_ClearMessage
    {
        public ushort Length;
        public string Message_Type;
        public uint Nanosecond;
        public uint Instrument_ID;
        public byte Sub_Book;
        public string Book_Type;

        public static Order_Book_ClearMessage BuildOrder_Book_ClearMessage(BinaryReader binaryReader, long headerId)
        {
            var UnitHeader = binaryReader.ReadBytes(8);
            

            var lengthBuffer = binaryReader.ReadBytes(2);
            var length = BitConverter.ToUInt16(lengthBuffer, 0);
            

            var message_typeBuffer = binaryReader.ReadBytes(1);
            var message_type = Encoding.ASCII.GetString(message_typeBuffer);
            

            var nanosecondBuffer = binaryReader.ReadBytes(4);
            var nanosecond = BitConverter.ToUInt32(nanosecondBuffer, 0);
            

            var instrument_idBuffer = binaryReader.ReadBytes(4);
            var instrument_id = BitConverter.ToUInt32(instrument_idBuffer, 0);
            

            var sub_book = binaryReader.ReadByte();
            

            var book_typeBuffer = binaryReader.ReadBytes(1);
            var book_type = Encoding.ASCII.GetString(book_typeBuffer);
            


            return new Order_Book_ClearMessage()
            {
                Length = length,
                Message_Type = message_type,
                Nanosecond = nanosecond,
                Instrument_ID = instrument_id,
                Sub_Book = sub_book,
                Book_Type = book_type,
            };
        }
    }
}
