// This is a generated file.  Don't edit it directly!

using System;
using System.IO;
using System.Text;

namespace backt
{
    public struct StatisticsMessage
    {
        public ushort Length;
        public string Message_Type;
        public uint Nanosecond;
        public uint Instrument_ID;
        public string Reserved;
        public string Reserved1;
        public string Statistic_Type;
        public long Price;
        public string OpenClose_Indicator;
        public byte Sub_Book;

        public static StatisticsMessage BuildStatisticsMessage(BinaryReader binaryReader, long headerId)
        {
            var UnitHeader = binaryReader.ReadBytes(8);
            

            var lengthBuffer = binaryReader.ReadBytes(2);
            var length = BitConverter.ToUInt16(lengthBuffer, 0);
            

            var message_typeBuffer = binaryReader.ReadBytes(1);
            var message_type = Encoding.ASCII.GetString(message_typeBuffer);
            

            var nanosecondBuffer = binaryReader.ReadBytes(4);
            var nanosecond = BitConverter.ToUInt32(nanosecondBuffer, 0);
            

            var instrument_idBuffer = binaryReader.ReadBytes(4);
            var instrument_id = BitConverter.ToUInt32(instrument_idBuffer, 0);
            

            var reservedBuffer = binaryReader.ReadBytes(1);
            var reserved = Encoding.ASCII.GetString(reservedBuffer);
            

            var reserved1Buffer = binaryReader.ReadBytes(1);
            var reserved1 = Encoding.ASCII.GetString(reserved1Buffer);
            

            var statistic_typeBuffer = binaryReader.ReadBytes(1);
            var statistic_type = Encoding.ASCII.GetString(statistic_typeBuffer);
            

            var priceBuffer = binaryReader.ReadBytes(8);
            var price = BitConverter.ToInt64(priceBuffer);
            

            var openclose_indicatorBuffer = binaryReader.ReadBytes(1);
            var openclose_indicator = Encoding.ASCII.GetString(openclose_indicatorBuffer);
            

            var sub_book = binaryReader.ReadByte();
            


            return new StatisticsMessage()
            {
                Length = length,
                Message_Type = message_type,
                Nanosecond = nanosecond,
                Instrument_ID = instrument_id,
                Reserved = reserved,
                Reserved1 = reserved1,
                Statistic_Type = statistic_type,
                Price = price,
                OpenClose_Indicator = openclose_indicator,
                Sub_Book = sub_book,
            };
        }
    }
}
