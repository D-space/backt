// This is a generated file.  Don't edit it directly!

using System;
using System.IO;
using System.Text;

namespace backt
{
    public struct Trade_BreakMessage
    {
        public ushort Length;
        public string Message_Type;
        public uint Nanosecond;
        public ulong Trade_ID;
        public string Trade_Type;

        public static Trade_BreakMessage BuildTrade_BreakMessage(BinaryReader binaryReader, long headerId)
        {
            var UnitHeader = binaryReader.ReadBytes(8);
            

            var lengthBuffer = binaryReader.ReadBytes(2);
            var length = BitConverter.ToUInt16(lengthBuffer, 0);
            

            var message_typeBuffer = binaryReader.ReadBytes(1);
            var message_type = Encoding.ASCII.GetString(message_typeBuffer);
            

            var nanosecondBuffer = binaryReader.ReadBytes(4);
            var nanosecond = BitConverter.ToUInt32(nanosecondBuffer, 0);
            

            var trade_idBuffer = binaryReader.ReadBytes(8);
            var trade_id = BitConverter.ToUInt64(trade_idBuffer, 0);
            

            var trade_typeBuffer = binaryReader.ReadBytes(1);
            var trade_type = Encoding.ASCII.GetString(trade_typeBuffer);
            


            return new Trade_BreakMessage()
            {
                Length = length,
                Message_Type = message_type,
                Nanosecond = nanosecond,
                Trade_ID = trade_id,
                Trade_Type = trade_type,
            };
        }
    }
}
