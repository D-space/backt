﻿using System;
using System.IO;
using System.Text;
using System.Linq;


namespace backt
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var watch = new System.Diagnostics.Stopwatch();

            string inputfilename = "D:\\DWorkspace\\DWork\\qdp-backtester\\equity market data sample\\JSE_EQM_MITCHDATA_20190724.bin\\JSE_EQM_MITCHDATA_20190724.bin";
            FileInfo inputfile = new FileInfo(inputfilename);

            // MITCHReader mitchReader = new MITCHReader();
            string outputfilename = "JSE_EQM_MITCHDATA_YYYYMMDD.json";
            watch.Start();
            MITCHReader.StartCaptureSample(inputfile);
            watch.Stop();

            // QuestConn qdb = new QuestConn(username, password, database, port);


            // Console.WriteLine("JSE_EQM_MITCHDATA_20190805");
            Console.WriteLine($"Execution Time: {watch.ElapsedMilliseconds} ms");


        }
    }
}


