using System;
using System.IO;
using System.Text;
using System.Linq;

using Npgsql;


namespace backt
{
    public class QuestConn
    {

        public string connectionString;
        public NpgsqlConnection conn;


        public QuestConn(string username, string password, string database, int port)
        {

            connectionString = $"host=127.0.0.1;port={port};username={username};password={password};database={database};ServerCompatibilityMode=NoTypeLoading;";
            // Init the main connection object
            Console.WriteLine("New Quest connection");
            conn = new NpgsqlConnection(connectionString);
            conn.Open();
            Console.WriteLine("Opened");
        }



        public void SetupTables()
        {
            // Setup tables if not exist
            Console.WriteLine("Setting up tables");
            // The plan is to have each message type map to its own table
            using( var cmd = new NpgsqlCommand("CREATE TABLE IF NOT EXISTS AddOrder (seq_id SYMBOL INDEX, length INT, message_type CHAR);", conn))
            {
                cmd.ExecuteNonQuery();
            }
            Console.WriteLine("Done up tables");


        }

        public void TestWrite()
        {
            Console.WriteLine("testing write");

            using( var cmd = new NpgsqlCommand("CREATE TABLE IF NOT EXISTS AddOrder (seq_id SYMBOL INDEX, length INT, message_type CHAR);", conn))
            {
                cmd.ExecuteNonQuery();
            }
            Console.WriteLine("Done up tables");



        }

        public void TestInsertAddOrder(Add_OrderMessage addOrder, long addOrderSeqId)
        {
            Console.WriteLine("testing add order insert");
            Console.WriteLine($"MessageType={addOrder.Message_Type} SeqID={addOrderSeqId}");

            // Insert some data
            using (var cmd = new NpgsqlCommand("INSERT INTO AddOrder (seq_id, length, message_type) VALUES (@s, @l, @mt)", conn))
            {
                cmd.Parameters.AddWithValue("s", addOrderSeqId);
                cmd.Parameters.AddWithValue("l", (int) addOrder.Length);
                cmd.Parameters.AddWithValue("mt", addOrder.Message_Type);
                cmd.ExecuteNonQuery();
            }

            Console.WriteLine("Done inserting add order data");



        }


        public void Conn()
        {
            Console.WriteLine($"trying to conn {connectionString}");

            // await using NpgsqlConnection connection = new NpgsqlConnection(connectionString);
            // await connection.OpenAsync();

            // var sql = "SELECT event FROM telemetry;";

            // await using NpgsqlCommand command = new NpgsqlCommand (sql, connection);
            // await using (var reader = await command.ExecuteReaderAsync()) {
            //     while (await reader.ReadAsync())
            //     {
            //         var station = reader.GetString(0);
            //         var height = double.Parse(reader.GetString(1));
            //         var timestamp = reader.GetString(2);
            //     }
            // }

            // await using NpgsqlConnection connection = new NpgsqlConnection(connectionString);
            // var con = await connection.OpenAsync();


            Console.WriteLine("Opened");


            using (var cmd = new NpgsqlCommand("SELECT event FROM telemetry;", conn))
            using (var reader = cmd.ExecuteReader())
                while (reader.Read())
                    Console.WriteLine(reader.GetInt32(0));

        }
    }
}
