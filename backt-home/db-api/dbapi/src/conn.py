import psycopg2

class Conn:
    def __init__(self):
        self.connection = self.init_connection()

    def init_connection(self):
        print("Init connectino do qdb")
        try:
            connection = psycopg2.connect(user="admin",
                                          password="quest",
                                          host="questdb",
                                          port="8812",
                                          database="qdb")

            cursor = connection.cursor()
            # Print PostgreSQL Connection properties
            print(connection.get_dsn_parameters(), "\n")

            # Print PostgreSQL version
            cursor.execute("SELECT version();")
            record = cursor.fetchone()
            print("You are connected to - ", record, "\n")
            cursor.close()

            return connection

        except (Exception, psycopg2.Error) as error:
            print("Error while connecting to PostgreSQL", error)
            return None


    def get_symbol_directory(self):
        ls_search_data = []
        try:

            cursor = self.connection.cursor()
            query = "SELECT * FROM Symbol_Directory;"
            cursor.execute(query)
            records = cursor.fetchall()
            # d_sym = {}

            print("Print each row and it's columns values")
            for row in records:
                # print("InsID = ", row[4], " ISIN = ", row[8], " Symbol = ", row[9], "\n")
                d_sym = str(row[9]).strip()
                d_val = row[4]
                val_val = f"{str(d_sym)}::{d_val}"
                d = {
                    "value": val_val,
                    "label":d_sym
                }
                ls_search_data.append(d)




            cursor.close()
            return ls_search_data


        except (Exception, psycopg2.Error) as error:
            print("Error while connecting to PostgreSQL", error)
            return ls_search_data





    def build_order_book_for_insid(self, insid):
        print("Buildiong order book for ", insid)
        return []






    def close(self):
            #closing database connection.
        if (self.connection):
            print("Closing PostgreSQL connection")
            self.connection.close()
            print("PostgreSQL connection is closed")
        else:
            print("No PostgreSQL connection to close")





