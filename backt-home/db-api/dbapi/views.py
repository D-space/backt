from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import viewsets

# from api import serializers
# from api import models
from django.core.serializers import serialize
from django.http import JsonResponse
import pandas as pd
import numpy as np

from django.shortcuts import render

from .src.conn import Conn

# Create your views here.
global conn
conn = None


class SearchSymbolView(viewsets.ViewSet):

    # serializer_class = serializers.DataSourceSerializer

    def list(self, request, format=None):
        # all_entries = models.DataSource.objects.all()
        global conn


        conn = Conn()

        ls_data = conn.get_symbol_directory()

        # conn.close()
        return Response({'data':ls_data})



class BuildOrderBookSymbolView(viewsets.ViewSet):

    # serializer_class = serializers.DataSourceSerializer

    def create(self, request):
        # all_entries = models.DataSource.objects.all()
        global conn

        # get the requested symbol

        rdata = request.data
        symNainsidme = rdata["insid"]



        ls_data = conn.build_order_book_for_insid(symNainsidme)

        # conn.close()
        return Response({'data':ls_data})