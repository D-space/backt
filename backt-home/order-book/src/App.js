import React, { Component } from 'react';
// import logo from './logo.svg';
// import './App.css';
import './App.less';
import { Button } from 'antd';
import { connect } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import BaseRouter from './routes';
import CustomLayout from './containers/Layout';
import * as Actions from './store/actions/Auth';

class App extends Component {

  componentDidMount() {
    this.props.onTryAutoSignup();
  }


  render() {
    return (
      <div>
        <Router>
          <CustomLayout {...this.props}>
            <BaseRouter />
          </CustomLayout>
        </Router>
      </div>
    );
  }
}

//allows us to access the props
//!= checks the value
//!== checks the value and type i.e returns TRUE if they are NOT the same
const mapStateToProps = state => {
  return {
    isAuthenticated: state.token !== null

  }
}

const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignup: () => dispatch(Actions.authCheckState())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
