import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';

import { Layout, Menu, Breadcrumb, Divider, Card, Icon, Button, PageHeader, Input, Space, AutoComplete, Table, Descriptions   } from 'antd';
import {
    AppstoreOutlined,
} from '@ant-design/icons';

import CONN from '../assets/data/conn.json';

// import DataView from './DataView';

const BOLT_CONN = CONN.BOLT_CONN;
const neo4j = require('neo4j-driver');
const axios = require('axios');

const { Header, Content, Footer } = Layout;

const { Search } = Input;

class HomeView extends Component {

    state = {
        dataSourceList: [],
        options:[],
        bbgData:[],
        bbgCols:[],
        periData:[],
        periCols:[],
        totemData:[],
        totemCols:[],
        descObjs:[],
    }


    componentDidMount() {
        document.title = "DerivComp - Home";
        this.initHomeViewData();

    }

    initHomeViewData = () => {
        console.log("initing");
        axios.get('http://localhost:8000/api/app1/search/').then(resp => {
            // console.log(resp.data);
            var respData = resp.data;

            this.setState({
                options:respData.data
            });

        });
    }

    handleSearch = (val) => {
        console.log(val);

        var postData = {
            sourceName:val
        };

        // populate bloomberg
        axios.post('http://localhost:8000/api/app1/bbg/', postData).then(resp => {
            var respData = resp.data;
            // console.log(respData.data);
            // console.log(respData.length);

            this.setState({
                bbgData:respData.data,
                bbgCols:respData.columns
            });



        });


        // populate perisec
        axios.post('http://localhost:8000/api/app1/pere/', postData).then(resp => {
            var respData = resp.data;
            console.log(respData.data);
            console.log(respData.length);

            this.setState({
                periData:respData.data,
                periCols:respData.columns
            });



        });

        // populate totm 
        axios.post('http://localhost:8000/api/app1/totm/', postData).then(resp => {
            var respData = resp.data;
            console.log(respData.data);
            console.log(respData.length);

            this.setState({
                totemData:respData.data,
                totemCols:respData.columns
            });



        });

        // populate descriptions
        // populate totm 
        axios.post('http://localhost:8000/api/app1/meta/', postData).then(resp => {
            var respData = resp.data;
            console.log(respData.data);
            console.log(respData.length);

            this.setState({
                descObjs:respData.data
            });



        });

    }

    onSelect = (data) => {
      console.log('onSelect', data);
    };





    render() {


        var descItems = this.state.descObjs.map(desc => {
          return (
            <Descriptions.Item label={desc.descLabel}>{desc.descText}</Descriptions.Item>
          );
        });

        return (
            <div>
                <Layout style={{height: '100vh'}}>
                  <Card style={{margin: '10px'}} className="cool-form-wrapper">

                        <Divider><h2>Search Identifier Sources</h2></Divider>
                        <AutoComplete
                          options={this.state.options}
                          style={{
                            width: '100%',
                          }}
                          onSelect={this.handleSearch}
                          filterOption={true}
                          
                        >
                          <Search size="large" placeholder="input here" allowClear enterButton />
                        </AutoComplete>

                        <Divider style={{ fontSize: '1.5em' }}> </Divider>
                        <Descriptions bordered>
                          {descItems}
                        </Descriptions>
                    </Card>
                  

                    <Content style={{ margin: '24px 16px 0', overflow: 'auto' }}>
                        <br/>
                        <div className="cool-form-wrapper">                        
                          <Divider style={{ fontSize: '1.5em' }} orientation="left"> Bloomberg Historic Dividends </Divider>
                          <div>
                            <Table dataSource={this.state.bbgData} columns={this.state.bbgCols} />
                          </div>
                        </div>

                        <br/>
                        <div className="cool-form-wrapper">                        
                          <Divider style={{ fontSize: '1.5em' }} orientation="left"> Peresec Stock Dividend Forecast </Divider>
                          <div>
                            <Table dataSource={this.state.periData} columns={this.state.periCols} />
                          </div>
                        </div>

                        <br/>
                        <div className="cool-form-wrapper">                        
                          <Divider style={{ fontSize: '1.5em' }} orientation="left"> Totem Stock Dividend Forecast </Divider>
                          <div>
                            <Table dataSource={this.state.totemData} columns={this.state.totemCols} scroll={{ x: 1500 }} size="small"  />
                          </div>
                        </div>
                    </Content>


                </Layout>
            </div>
        );
    }
}

// map the state to props
const mapStateToProps = state => {
    return {
        token: state.token

    }
}


export default withRouter(connect(mapStateToProps)(HomeView));