import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';

import { 
  Layout,
  Menu,
  Breadcrumb,
  Divider,
  Card,
  Icon,
  Button,
  PageHeader,
  Input,
  Space,
  AutoComplete,
  Table,
  Descriptions,
  Select,
  Form
} from 'antd';
import {
    AppstoreOutlined,
} from '@ant-design/icons';

import CONN from '../assets/data/conn.json';

// import DataView from './DataView';
// import pg from 'pg'

const BOLT_CONN = CONN.BOLT_CONN;
const neo4j = require('neo4j-driver');
const axios = require('axios');

const { Header, Content, Footer } = Layout;

const { Search } = Input;

const FormItem = Form.Item;



class HomeView extends Component {

    state = {
        dataSourceList: [],
        options:[],
        bbgData:[],
        bbgCols:[],
        periData:[],
        periCols:[],
        totemData:[],
        totemCols:[],
        descObjs:[],
        frontOptions:[],
        pereOptions:[],
        totemOptions:[],
        bloomOptions:[],
        cSelection:"",
        divCompData:[],
        divCompCols:[],
    }

    formRef = React.createRef();



    componentDidMount() {
        document.title = "Order Book - Home";
        this.initHomeViewData();

    }

    initHomeViewData = () => {
        console.log("initing To quest");

        axios.get('http://127.0.0.1:8000/dbapi/search/').then(resp => {
            // console.log(resp.data);
            var respData = resp.data;

            console.log(respData);

            this.setState({
                options:respData.data
            });

        });
    }

    handleSearch = (val) => {
        console.log(val);

        var postData = {
            insid:val
        };

        axios.post('http://127.0.0.1:8000/dbapi/buildob/', postData).then(resp => {
          var respData = resp.data;
          console.log(respData);

        });


    }

    onSelect = (data) => {
      console.log('onSelect', data);
    };

    handleFrontSearch = (data) => {
        console.log('onSelect', data);
    }


    checkEmpty = (v) => {
        let type = typeof v;
        if (type === 'undefined') {
            return true;
        }
        if (type === 'boolean') {
            return !v;
        }
        if (v === null) {
            return true;
        }
        if (v === undefined) {
            return true;
        }
        if (v instanceof Array) {
            if (v.length < 1) {
                return true;
            }
        } else if (type === 'string') {
            if (v.length < 1) {
                return true;
            }
            if (v === '0') {
                return true;
            }
        } else if (type === 'object') {
            if (Object.keys(v).length < 1) {
                return true;
            }
            // check if object which has keys actually has values
            else {
                var ret = false;
                Object.values(v).forEach(x => {
                    if (x == null || x == "") {
                        ret = true;
                    }
                });
                return ret;
            }
        } else if (type === 'number') {
            if (v === 0) {
                return true;
            }
        }
        return false;
    }


    filterParams = (values) => {
        // Util func to filter out any 'null' or empty params

        // Filter the params so that we only consider non nulls and non empty strings ''
        var fVals = {};
        for (var param in values) {
            // console.log(param, values[param]);
            // if (values[param] == null || values[param] == "") {
            if (this.checkEmpty(values[param])) {
                // console.log("null param for", param);
            }
            else {
                fVals[param] = values[param];
            }
        }
        return fVals;
    }


    handleSubmitParams = (values) => {
        console.log("Vals subbed ", values);

        var fVals = this.filterParams(values);
        console.log("Filtered", fVals);

        var postData = {
            sourceName:this.state.cSelection,
            faRefDt:fVals.FrontReferenceDate,
            perRefDt:fVals.PeresecReferenceDate,
            ttmRefDt:fVals.TotemReferenceDate,
            bbgRefDt:fVals.BloombergReferenceDate,
        };

        // var fVals = this.filterParams(values);
        // console.log("Filtered", fVals);


        // var qStr = this.buildDataParamQuery(fVals);
        // this.makeRequest(qStr);

        // calculate comp table using selected dates
        axios.post('http://localhost:8000/api/app1/getDvds/', postData).then(resp => {
            var respData = resp.data;
            console.log(respData);

            console.log(respData.data);
            console.log(respData.length);

            this.setState({
                divCompData:respData.data,
                divCompCols:respData.columns,

            });



        });

    }


    resetParamSelectionForm = () => {
        console.log("resetting form");

        // we can access the default form which ant created for us
        this.formRef.current.resetFields();
    }





    render() {


        var descItems = this.state.descObjs.map(desc => {
          return (
            <Descriptions.Item label={desc.descLabel}>{desc.descText}</Descriptions.Item>
          );
        });

        return (
            <div>
                <Layout style={{height: '100vh'}}>
                  <Card style={{margin: '10px'}} className="cool-form-wrapper">

                        <Divider><h2>Select JSE Instrument</h2></Divider>
                        <AutoComplete
                          options={this.state.options}
                          style={{
                            width: '100%',
                          }}
                          onSelect={this.handleSearch}
                          filterOption={true}
                          
                        >
                          <Search size="large" placeholder="input here" allowClear enterButton />
                        </AutoComplete>

                        <Divider style={{ fontSize: '1.5em' }}> </Divider>
                        <Descriptions bordered>
                          {descItems}
                        </Descriptions>
                    </Card>
                  

                    <Content style={{ margin: '24px 16px 0', overflow: 'auto' }}>
                        <br/>
                        <div className="cool-form-wrapper">                        
                          <Divider style={{ fontSize: '1.5em' }} orientation="left"> View Order Book </Divider>
                          <div>
                          </div>
                        </div>

                    </Content>


                </Layout>
            </div>
        );
    }
}

// map the state to props
const mapStateToProps = state => {
    return {
        token: state.token

    }
}


export default withRouter(connect(mapStateToProps)(HomeView));