import React from 'react';
import { Form, Input, Button, Checkbox, Divider, Spin, Row, Col } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { connect } from 'react-redux';
import { NavLink, Link, withRouter } from 'react-router-dom';
import * as Actions from '../store/actions/Auth';
import '../App.less';

class NormalLoginForm extends React.Component {


    render() {
        const onFinish = values => {
            // console.log('Received values of form: ', values);
            this.props.onAuth(values.username, values.password);
            this.props.history.push({
                pathname: '/home',
                state: { uname: values.username, pword: values.password }
            })
        };

        let errMsg = null;
        if (this.props.error) {
            errMsg = (
                <div style={{ 'color': 'red' }}>
                    <p>
                        {this.props.error.message}
                    </p>

                </div>
            );

        }

        let loginScr = null;
        if (this.props.loading) {
            loginScr = (
                <Spin />
            );
        }
        else {
            loginScr = (
                <div className="cool-form-wrapper">
                    <Divider style={{ fontSize: '1.5em' }}> Login </Divider>
                    <Form
                        name="normal_login"
                        className="login-form"
                        initialValues={{
                            remember: true,
                        }}
                        onFinish={onFinish}
                    >
                        <Form.Item
                            name="username"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your Username!',
                                },
                            ]}
                        >
                            <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username" />
                        </Form.Item>
                        <Form.Item
                            name="password"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your Password!',
                                },
                            ]}
                        >
                            <Input
                                prefix={<LockOutlined className="site-form-item-icon" />}
                                type="password"
                                placeholder="Password"
                            />
                        </Form.Item>
                        <Form.Item>
                            <Button type="primary" htmlType="submit" className="login-form-button">
                                Log in
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
            );
        }


        return (
            <Row>
                <Col span={12} offset={6}>
                    <div style={{ display: 'block', height: '600px', marginTop: '25px' }}>
                        <div className="cool-container">
                            <div className="cool-content">
                                {loginScr}
                                {errMsg}
                            </div>
                        </div>
                    </div>
                </Col>
            </Row>


        );
    }
}

const mapStateToProps = (state) => {
    return {
        loading: state.loading,
        error: state.error
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAuth: (username, password) => dispatch(Actions.authLogin(username, password))
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(NormalLoginForm));