import React from 'react';
import ReactDOM from 'react-dom';
// import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { createStore, compose, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
//reducer that will be used in the store
import reducer from './store/reducers/Auth';

//enhancer to handle middleware - using redux thunk to handle that layer
//should install chrome ext if needed otherwise use reduc compose
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducer, composeEnhancers(
  applyMiddleware(thunk)
));

//create app to use the store!
//ie wrap the app in the provider
const app = (
  <Provider store={store} >
    <App />
  </Provider>
)

//now render the app with the store
ReactDOM.render(app, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
