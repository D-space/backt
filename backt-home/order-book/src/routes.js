import React from 'react';
import { Route } from 'react-router-dom';


import Login from './containers/Login';
import HomeView from './containers/HomeView';

const BaseRouter = () => (
    <div>
        <Route exact path="/" component={Login} />
        <Route exact path="/home" component={HomeView} />
    </div>
);

export default BaseRouter;