/* Here we define the methods that respond to the action */
import * as ActionTypes from './ActionTypes';
import CONN from '../../assets/data/conn.json';

const BOLT_CONN = CONN.BOLT_CONN;



export const authStart = () => {
    return {
        type: ActionTypes.AUTH_START
    }
}

export const authSuccess = (token, username) => {
    return {
        type: ActionTypes.AUTH_SUCCESS,
        token: token,
        username: username
    }
}

export const authFail = (error) => {
    return {
        type: ActionTypes.AUTH_FAIL,
        error: error
    }
}

//to log out we just remove the users token!
export const logout = () => {
    localStorage.removeItem('user');
    localStorage.removeItem('expirationDate');
    return {
        type: ActionTypes.AUTH_LOGOUT
    }
}

//function to check if user session has expired
//NB setTimeout is in ms so need to change from s to ms
export const checkAuthTimeout = (expirationTime) => {
    return dispatch => {
        setTimeout(() => {
            dispatch(logout());
        }, expirationTime * 1000)
    }
}

export const authLogin = (username, password) => {
    return dispatch => {
        dispatch(authStart());
        var neo4j = require("neo4j-driver");


        try {
            const driver = neo4j.driver(BOLT_CONN, neo4j.auth.basic(username, password));
            // const isconn = (async () => {
            //     await driver.verifyConnectivity();
            // })();
            const isconn = driver.verifyConnectivity();
            // return new Promise(isconn);
            isconn.then((val) => {
                console.log("On then", val);
                console.log('Driver created', isconn);
                // const token = result.data.key;
                // const token = Math.floor(Math.random() * 10) + 1;  // returns a random integer from 1 to 100
                // Token will be a encoded version of our userpass
                const tokenRaw = username + "<>" + password;
                const token = btoa(tokenRaw);

                const sixHrsMls = (6 * 3600) * 1000;
                const sixHrs = 6 * 3600;

                const expirationDate = new Date(new Date().getTime() + sixHrsMls);
                localStorage.setItem('token', token);
                localStorage.setItem('username', username);
                // Obfuscate password for later use

                localStorage.setItem('expirationDate', expirationDate);
                dispatch(authSuccess(token, username));
                dispatch(checkAuthTimeout(sixHrs));
            }).catch((err) => {
                console.log("On then ERROR.", err);
                // dispatch(authFail(err));

                // !!!!!!
                // SINCE we're just testing. Always enable Auth!
                // PLEASE REMOVE in PROD
                // !!!!!!

                const tokenRaw = username + "<>" + password;
                const token = btoa(tokenRaw);

                const sixHrsMls = (6 * 3600) * 1000;
                const sixHrs = 6 * 3600;

                const expirationDate = new Date(new Date().getTime() + sixHrsMls);
                localStorage.setItem('token', token);
                localStorage.setItem('username', username);
                // Obfuscate password for later use

                localStorage.setItem('expirationDate', expirationDate);
                dispatch(authSuccess(token, username));
                dispatch(checkAuthTimeout(sixHrs));

            })



        } catch (error) {
            console.log("connectivity verification failed.", error);
            dispatch(authFail(error));
        }
    }
}
//check if token is present in local storage
//if not, logout!
export const authCheckState = () => {
    return dispatch => {
        console.log("checking state");
        const token = localStorage.getItem('token');
        const username = localStorage.getItem('username');
        if (token === undefined) {
            console.log("undef");
            dispatch(logout());

        }
        else {
            console.log("checking state else");

            const expirationDate = new Date(localStorage.getItem('expirationDate'));
            //if exp date is smaller than right now
            //then logout!
            if (expirationDate <= new Date()) {
                console.log("checking state small date logout", expirationDate);

                dispatch(logout());
            }
            else {
                console.log("checking state sucess");
                dispatch(authSuccess(token, username));
                // dispatch(checkAuthTimeout((expirationDate.getTime - new Date().getTime()) / 1000));
            }

        }
    }
}










