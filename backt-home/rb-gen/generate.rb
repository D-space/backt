$LOAD_PATH << File.dirname(__FILE__)

require 'rubygems'
require 'mitch_dictionary'
require 'message_gen'
require 'parser_gen'
require 'quest_conn_gen'

class Generator
    # self.generate only accessible to this class
    def self.generate
        generator = Generator.new
        generator.testgen
        generator.generate_messages
        generator.generate_mitch_parser
        generator.generate_quest_connection
    end

    def initialize
        @mitchspec = MITCHDictionary.load spec()

        @src_messages_path = File.join File.dirname(__FILE__), '..', 'backt'
    end

    def spec
        File.join File.dirname(__FILE__), "..", "spec", "MITCH.xml"
    end

    def testgen
        puts "Testing generator"
        puts "MITCH unitheader = #{@mitchspec.unit_header}"
        puts "MITCH messages = #{@mitchspec.app_messages}"
    end

    def generate_messages
        msgs_path = File.join(@src_messages_path, 'Messages')
        
        # msgs_path_app = File.join(@src_messages_path, 'Messages', 'App')
        # msgs_path_admin = File.join(@src_messages_path, 'Messages', 'Admin')

        MessageGen.generate(@mitchspec.app_messages,  msgs_path)
        MessageGen.generate(@mitchspec.admin_messages,  msgs_path)
    end



    def generate_mitch_parser
        msgs_path = File.join(@src_messages_path)
        ParserGen.generate(@mitchspec.unit_header, @mitchspec.admin_messages, @mitchspec.app_messages, msgs_path)    
    end



    def generate_quest_connection
        msgs_path = File.join(@src_messages_path)
        QuestConnGen.generate(@mitchspec.unit_header, @mitchspec.admin_messages, @mitchspec.app_messages, msgs_path)    
    end
end

Generator.generate




