class MessageGen
  def self.generate messages, dir
    destdir = File.join(dir)
    Dir.mkdir(destdir) unless File.exists? destdir

    # basemsgstr = gen_basemsg(destdir)
    # basemsg_path = File.join(destdir, "Message.cs")
    # puts "generate #{basemsg_path}"
    # File.open(basemsg_path, 'w') {|f| f.puts(basemsgstr) }

    puts "generate #{destdir}*"
    messages.each do |msg| 
      # form message name

      msgstr = gen_msg(msg)
      msg_path = File.join(destdir, msg[:alias].delete(' ') + 'Message' + '.cs')
      # puts "generate #{msg_path}"
      File.open(msg_path, 'w') {|f| f.puts(msgstr) }
    end
    puts "  (generated #{messages.count} message files)"
  end




  def self.gen_msg msg
    msg_alias = msg[:alias].delete(' ') + 'Message'
    fields = msg[:fields]
    # puts "Fields #{fields}"

    s = <<HERE
// This is a generated file.  Don't edit it directly!

using System;
using System.IO;
using System.Text;

namespace backt
{
    public struct #{msg_alias}
    {
#{gen_msg_fields(msg_alias, fields, 8)}

        public static #{msg_alias} Build#{msg_alias}(BinaryReader binaryReader, long headerId)
        {
#{gen_msg_builder_uheader(msg_alias, fields, 12)}
#{gen_msg_builder_binreader(msg_alias, fields, 12)}

            return new #{msg_alias}()
            {
#{gen_msg_builder_return(msg_alias, fields, 16)}
HERE


s += <<HERE2
            };
        }
    }
}
HERE2
    s
  end




  def self.gen_msg_builder_uheader msg_name, fields, prepend_spaces
    # TODO: maybe process unitheader
    str = []
    # just read for now, dont decode. As per example
    str << "var UnitHeader = binaryReader.ReadBytes(8);"
    # Then add decode code
    # str << "var #{fld_alias} = Encoding.ASCII.GetString(#{fBuffer});"
    str << "\n"



    str.map! {|s| ' '*prepend_spaces + s}
    str.join("\n")

  end

  def self.gen_msg_builder_binreader msg_name, fields, prepend_spaces
    # here we try to build up the array of binary parsers for the fields
    # its a basic 2 step process for each field.
    # 1. var fieldBuffer = readBytes(Fieldtype)
    # 2. decode(fieldBuffer, fieldtype)
    fields.map { |fld| binary_field(msg_name, fld, prepend_spaces) }.join("\n")

  end

  def self.gen_msg_builder_return msg_name, fields, prepend_spaces
    fields.map { |fld| return_values(msg_name, fld, prepend_spaces) }.join("\n")

  end


  def self.gen_msg_fields msg_name, fields, prepend_spaces
    # TODO: maybe sort by offset to ensure that the sequence of fields is correct
    # puts "YO#{fields}"
    # puts "YO#{fields[0]}"
    fields.map { |fld| msg_field(msg_name, fld, prepend_spaces)}.join("\n")
  end


  def self.msg_field msg_name, field, prepend_spaces
    sd = ""
    fld_alias = field[:alias].delete(' ')

    if fld_alias == msg_name
      fld_alias = fld_alias + '_'
    end

    # now figure out the varname and type
    case field[:type]
    when 'alpha'
      # bitfield maps to a single byte in c#
      s = "public string #{fld_alias};"
    when 'bitfield'
      # bitfield maps to a single byte in c#
      s = "public byte #{fld_alias};"
    when 'byte'
      s = "public string #{fld_alias};"
    when 'date'
      # date maps to 8bytes
      s = "public string #{fld_alias};"
    when 'time'
      # date maps to 8bytes
      s = "public string #{fld_alias};"
    when 'price'
      # date maps to 8bytes
      s = "public long #{fld_alias};"
    when 'uint8'
      # uint8 is an 8bit unsigned int. Maps to single byte in c#
      s = "public byte #{fld_alias};"
    when 'uint16'
      s = "public ushort #{fld_alias};"
    when 'uint32'
      s = "public uint #{fld_alias};"
    when 'uint64'
      s = "public ulong #{fld_alias};"
    else
      # type unknown skip
      puts "Bad value #{field[:type]}"
      s = ""

    end

    val = ' ' * prepend_spaces + s


  end


  def self.binary_field msg_name, field, prepend_spaces
    fld_alias = field[:alias].delete(' ').downcase

    if fld_alias == msg_name
      fld_alias = fld_alias + '_'
    end

    fBuffer = "#{fld_alias}Buffer"


    # now figure out the varname and type
    case field[:type]
    when 'alpha'
      # alopha types are variable strings
      # use length to determine how many bytes to read. 
      # We could actually just do this for all fields
      str = []

      field_len = field[:length]

      str << "var #{fBuffer} = binaryReader.ReadBytes(#{field_len});"
      # Then add decode code
      str << "var #{fld_alias} = Encoding.ASCII.GetString(#{fBuffer});"
      str << "\n"


      str.map! {|s| ' '*prepend_spaces + s}
      str.join("\n")


    when 'bitfield'
      # bitfield maps to a single byte in c#
      # s = "public byte #{fld_alias};"
      str = []
      # just read for now, dont decode. As per example
      str << "var #{fld_alias} = binaryReader.ReadByte();"
      # Then add decode code
      # str << "var #{fld_alias} = Encoding.ASCII.GetString(#{fBuffer});"
      str << "\n"


      str.map! {|s| ' '*prepend_spaces + s}
      str.join("\n")

    when 'byte'
      str = []

      str << "var #{fBuffer} = binaryReader.ReadBytes(1);"
      # Then add decode code
      str << "var #{fld_alias} = Encoding.ASCII.GetString(#{fBuffer});"
      str << "\n"


      str.map! {|s| ' '*prepend_spaces + s}
      str.join("\n")

    when 'date'
      # date maps to 8bytes
      str = []

      str << "var #{fBuffer} = binaryReader.ReadBytes(8);"
      # Then add decode code
      str << "var #{fld_alias} = Encoding.ASCII.GetString(#{fBuffer});"
      str << "\n"


      str.map! {|s| ' '*prepend_spaces + s}
      str.join("\n")

    when 'time'
      str = []

      str << "var #{fBuffer} = binaryReader.ReadBytes(8);"
      # Then add decode code
      str << "var #{fld_alias} = Encoding.ASCII.GetString(#{fBuffer});"
      str << "\n"


      str.map! {|s| ' '*prepend_spaces + s}
      str.join("\n")

    when 'price'
      # hotfix: for Symbol Directory message
      # Contract multiplier only exists if Underlying is not null! See doc
      # // As per doc
      # // contract multiplier will contain 0 value if instrument does not have multipler
      # // ie dont process
      # // Use the underlying value to determine if we need to process field

      if fld_alias == "contract_multiplier"
        str = []
        str << "// As per doc"
        str << "// contract multiplier will contain 0 value if instrument does not have multipler"
        str << "// ie dont process. Use the underlying value to determine if we need to process field"

        str << "var #{fld_alias} = 0L;"
        str << "if (!string.IsNullOrWhiteSpace(underlying))"
        str << "{"
        str << "    Console.WriteLine(\"underlying is not null\");"
        str << "    var #{fBuffer} = binaryReader.ReadBytes(8);"
        str << "    contract_multiplier = BitConverter.ToInt64(#{fBuffer},0);"
        str << "}"
        str << "\n"

        str.map! {|s| ' '*prepend_spaces + s}
        str.join("\n")

      else
        # Not contract multiplier field process price normally
        str = []

        str << "var #{fBuffer} = binaryReader.ReadBytes(8);"
        # Then add decode code
        str << "var #{fld_alias} = BitConverter.ToInt64(#{fBuffer});"
        str << "\n"

        str.map! {|s| ' '*prepend_spaces + s}
        str.join("\n")

      end

    when 'uint8'
      # uint8 is an 8bit unsigned int. Maps to single byte in c#
      str = []

      # str << "var #{fld_alias} = binaryReader.ReadBytes(1);"
      str << "var #{fld_alias} = binaryReader.ReadByte();"

      # uint8 is a 8bit unsigned integer object
      # str << "var #{fld_alias} = BitConverter.ToUInt16(#{fBuffer}, 0);"
      str << "\n"


      str.map! {|s| ' '*prepend_spaces + s}
      str.join("\n")

    when 'uint16'
      # first read the raw bytes
      str = []

      str << "var #{fBuffer} = binaryReader.ReadBytes(2);"
      # Then add decode code
      str << "var #{fld_alias} = BitConverter.ToUInt16(#{fBuffer}, 0);"
      str << "\n"


      str.map! {|s| ' '*prepend_spaces + s}
      str.join("\n")

    when 'uint32'
      str = []

      str << "var #{fBuffer} = binaryReader.ReadBytes(4);"
      # Then add decode code
      str << "var #{fld_alias} = BitConverter.ToUInt32(#{fBuffer}, 0);"
      str << "\n"


      str.map! {|s| ' '*prepend_spaces + s}
      str.join("\n")

    when 'uint64'
      str = []

      str << "var #{fBuffer} = binaryReader.ReadBytes(8);"
      # Then add decode code
      str << "var #{fld_alias} = BitConverter.ToUInt64(#{fBuffer}, 0);"
      str << "\n"


      str.map! {|s| ' '*prepend_spaces + s}
      str.join("\n")

    else
      puts "Bad value"

    end

    # val = ' ' * prepend_spaces + s


  end



  def self.return_values msg_name, field, prepend_spaces
    fld_alias = field[:alias].delete(' ')
    fld_val_alias = field[:alias].delete(' ').downcase

    if fld_alias == msg_name
      fld_alias = fld_alias + '_'
    end

    # skip field if no type
    if field[:type] == ""
      s = ""
    else
      s = "#{fld_alias} = #{fld_val_alias},"
      
    end

    val = ' ' * prepend_spaces + s


  end

end