import xml.etree.ElementTree as xml
from xml.dom import minidom
from pathlib import Path
import pandas as pd




ROOT = Path("D:/DWorkspace/DWork/qdp-backtester/equity market data sample/messages")


def clean_field(field):
    return str(field).strip()

def clean_df(df_raw):
    df_ret = df_raw.replace(to_replace=[r"\\t|\\n|\\r", "\t|\n|\r"], value=" ", regex=True)
    return df_ret




def add_unitheader(root):
    print("Adding unit header")
    unitheader = xml.SubElement(root, "unitheader")
    # get unitheaqder fields from csv
    uh_dir = ROOT / Path("header")
    uh_files = uh_dir.glob("*.csv")

    uh_files_ls = list(uh_files)

    print(f"Found {len(uh_files_ls)}")
    print(uh_files_ls[0])

    df_h_raw = pd.read_csv(uh_files_ls[0], header=None)
    df_h = clean_df(df_h_raw)
    # clean up types
    df_h[3] = df_h[3].str.replace(" ", "").str.lower()

    # create space for aliases so we can handle duplicates
    df_h["alias"] = df_h[0].str.replace(" ", "_")
    # rename duplicates
    df_h["alias"] = df_h["alias"] + df_h.groupby(["alias"]).cumcount().astype(str).replace('0','')

    print(df_h)

    # for each row, append field respectively
    for i,row in df_h.iterrows():
        field_name = clean_field(row[0])
        field_offset = clean_field(row[1])
        field_length = clean_field(row[2])
        field_type = clean_field(row[3])
        field_alias = clean_field(row["alias"])
        print(f"{field_name};{field_offset};{field_length};{field_type};{field_alias}")

        field_attribs = {
            "alias": field_alias,
            "name": field_name, 
            "offset":field_offset,
            "length":field_length,
            "type":field_type
        }

        xml.SubElement(unitheader, "field", field_attribs )




def add_msg_fields(file_path, elem):
    print(f"Adding fields from file:{file_path}")
    # get unitheaqder fields from csv

    df_raw = pd.read_csv(file_path, header=None)
    df = clean_df(df_raw)
    # dirty hack to replace fowardslash with OR
    df[0] = df[0].str.replace("/", "OR")

    # clean up types
    df[3] = df[3].str.replace(" ", "").str.lower()
    df[3] = df[3].str.replace("-", "").str.lower()
    df[3] = df[3].str.replace("*", "").str.lower()

    # create space for aliases so we can handle duplicates
    df["alias"] = df[0].str.replace(" ", "_")
    # rename duplicates
    df["alias"] = df["alias"] + df.groupby(["alias"]).cumcount().astype(str).replace('0','')

    print(df)

    # for each row, append field respectively
    for i,row in df.iterrows():
        field_name = clean_field(row[0])
        field_offset = clean_field(row[1])
        field_length = clean_field(row[2])
        field_type = clean_field(row[3])
        field_alias = clean_field(row["alias"])
        print(f"{field_name};{field_offset};{field_length};{field_type};{field_alias}")

        field_attribs = {
            "alias": field_alias,
            "name": field_name, 
            "offset":field_offset,
            "length":field_length,
            "type":field_type
        }

        xml.SubElement(elem, "field", field_attribs )


def add_app_msgs(root):
    print("Adding app msgs")
    app_msgs = xml.SubElement(root, "appmessages")


    # First use the app message csv file to build up each message respectively
    app_indx = ROOT / Path("ApplicationMessages.csv")
    app_path = ROOT / Path("app")

    df_app_raw = pd.read_csv(app_indx, header=None)
    df_app = clean_df(df_app_raw)


    df_app["alias"] = df_app[0].str.replace(" ", "_")
    # rename duplicates
    df_app["alias"] = df_app["alias"] + df_app.groupby(["alias"]).cumcount().astype(str).replace('0','')
    print(df_app)

    for i,row in df_app.iterrows():
        msg_name = clean_field(row[0])
        msg_type_ascii = clean_field(row[1])
        msg_type_hex = clean_field(row[2])
        msg_alias = clean_field(row["alias"])
        print(f"{msg_name};{msg_type_ascii};{msg_type_hex};{msg_alias}")

        # now get the file in question so we can process fields
        fname = f"{msg_name}.csv"
        fpath = app_path / Path(fname)

        if fpath.exists():
            print(f"Found file: {fpath.name}")
            # create this msg element
            msg_atribs = {
                "alias":msg_alias,
                "name":msg_name,
                "messagetype":msg_type_ascii,
                "messagetypehex":msg_type_hex
            }
            app_msg = xml.SubElement(app_msgs, "message", msg_atribs)
            # Now open file and get fields for this msg
            add_msg_fields(fpath, app_msg)
        else:
            # process heartbeat special
            if msg_name == "Heartbeat":
                msg_atribs = {
                    "alias":msg_alias,
                    "name":msg_name,
                    "messagetype":msg_type_ascii,
                    "messagetypehex":msg_type_hex
                }
                xml.SubElement(app_msgs, "message", msg_atribs)

            print(f"File Not Found: {fpath.name}")




def add_admin_msgs(root):
    print("Adding admin msgs")
    admin_msgs = xml.SubElement(root, "adminmessages")


    # First use the admin message csv file to build up each message respectively
    admin_indx = ROOT / Path("Administrative Messages.csv")
    admin_path = ROOT / Path("admin")

    df_admin_raw = pd.read_csv(admin_indx, header=None)
    df_admin = clean_df(df_admin_raw)

    df_admin["alias"] = df_admin[0].str.replace(" ", "_")
    # rename duplicates
    df_admin["alias"] = df_admin["alias"] + df_admin.groupby(["alias"]).cumcount().astype(str).replace('0','')
    print(df_admin)

    for i,row in df_admin.iterrows():
        msg_name = clean_field(row[0])
        msg_type_ascii = clean_field(row[1])
        msg_type_hex = clean_field(row[2])
        msg_alias = clean_field(row["alias"])
        print(f"{msg_name};{msg_type_ascii};{msg_type_hex};{msg_alias}")

        # now get the file in question so we can process fields
        fname = f"{msg_name}.csv"
        fpath = admin_path / Path(fname)

        if fpath.exists():
            print(f"Found file: {fpath.name}")
            # create this msg element
            msg_atribs = {
                "alias":msg_alias,
                "name":msg_name,
                "messagetype":msg_type_ascii,
                "messagetypehex":msg_type_hex
            }
            admin_msg = xml.SubElement(admin_msgs, "message", msg_atribs)
            # Now open file and get fields for this msg
            add_msg_fields(fpath, admin_msg)
        else:
            # process heartbeat special
            if msg_name == "Heartbeat":
                msg_atribs = {
                    "alias":msg_alias,
                    "name":msg_name,
                    "messagetype":msg_type_ascii,
                    "messagetypehex":msg_type_hex
                }
                xml.SubElement(admin_msgs, "message", msg_atribs)

            print(f"File Not Found: {fpath.name}")




def main():
    print("Generating MITCH XML Spec from csv")


    root = xml.Element("mitch")

    # add unitheader fields
    add_unitheader(root)

    # add admin messages
    add_admin_msgs(root)

    # add app msgs
    add_app_msgs(root)




    
    # root.append(unitheader)

    xmlstr = minidom.parseString(xml.tostring(root)).toprettyxml(indent="   ")

    # tree = xml.ElementTree(root)

    with open("MITCH.xml", "w") as file:
        file.write(xmlstr)






if __name__ == '__main__':
    main()