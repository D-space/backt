using System;
using System.IO;
using System.Text;
using System.Linq;

namespace backt
{
    public struct ExtractionMessage
    {
        // see the vonnegut doc for the envelop struture
        // and datatypes 


        public ushort Length;
        public byte Crc;

        // Note the Id is of type long not uLong
        // As this is still the envelope struture and not the actual message
        // so ID on envelope will be of type long
        public long Id;
        public string MessageType;
        public byte[] Data;

        /// <summary>
        /// Constructs a ExtractionMessage structure from a list of bytes
        /// </summary>
        /// <param name="binaryReader"></param>
        /// <returns></returns>

        public static ExtractionMessage BuildExtractionMessage(BinaryReader binaryReader)
        {
            // See c# datatype docs for explanation on how bytes map to datatypes 
            // https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/built-in-types
            // bytes map to length of datatype


            // type:long=Unsigned 64-bit integer => 8 bytes
            const int bytesInLong = 8;
            // type:short=Unsigned 16-bit integer => 2 bytes
            const int bytesInShort = 2;
            // type:short=Unsigned 32-bit integer => 4 bytes
            // Note that c# uses sequence uint to repr strings
            const int bytesInMessageType = 4;
            const int bytesInUnitHeaderLength = 4;

            // Start by getting the length of this record (see vonnegut)
            var lengthBuffer = binaryReader
            .ReadBytes(bytesInShort)
            .CorrectEndianness(Endianness.LittleEndian);

            // Since the length will be 2 bytes that corrospond to the uShort datatype
            // to get the actual value we need to convert back to the uInt16
            var shortBuffer = BitConverter.ToUInt16(lengthBuffer, 0);

            // We can just do simple math to determine number of bytes of the data payload
            var payloadLength = shortBuffer - bytesInLong - bytesInMessageType - bytesInUnitHeaderLength;

            // note crc is just a single byte. 
            // A cyclic redundancy check (CRC) is an error-detecting code
            var crc = binaryReader.ReadByte();
            

            // next up will be the SeqID of the message. It will be of type long (ie. 8bytes) 
            var longBuffer = binaryReader
            .ReadBytes(bytesInLong)
            .CorrectEndianness(Endianness.LittleEndian);

            // which is just a 64bit integer
            var id = BitConverter.ToInt64(longBuffer, 0);

            // Note (from vonnegut doc). Before we can read the message type.
            // There is an unsigned short (2bytes) indicator peceeding the string. So need to handle that first
            // This will tell us the size of the string to be processed

            lengthBuffer = binaryReader
            .ReadBytes(bytesInShort)
            .CorrectEndianness(Endianness.LittleEndian);

            // Conv back to uint16 to get the actual length of the messageType
            var stringLength = BitConverter.ToUInt16(lengthBuffer, 0); //Pascal string

            // Now read the message type into an ascii string.
            // catering for the actual length of the string
            var messageType = Encoding
            .ASCII
            .GetString(binaryReader
                .ReadBytes(bytesInMessageType - stringLength));

            lengthBuffer = binaryReader
            .ReadBytes(bytesInUnitHeaderLength)
            .CorrectEndianness(Endianness.LittleEndian);

            var unitHeaderLength = BitConverter.ToUInt32(lengthBuffer, 0); //ignored
            var data = binaryReader.ReadBytes(payloadLength);

            return new ExtractionMessage()
            {
                Length = shortBuffer,
                Crc = crc,
                Id = id,
                MessageType = messageType,
                Data = data
            };
        }

        
    }
}
