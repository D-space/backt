// This is a generated file.  Don't edit it directly!

using System;
using System.IO;
using System.Text;

namespace backt
{
    public static class MITCHParserGen
    {
        public static void ParseMessage(ExtractionMessage? messageWithHeader, QuestConnGen qdb)
        {
            using (var memoryStream = new MemoryStream(messageWithHeader.Value.Data))
            {
                using (var dataBinaryReader = new BinaryReader(memoryStream))
                {
                    switch(messageWithHeader.Value.MessageType)
                    {
                        case "54":
                            var TimeMessageBody = TimeMessage.BuildTimeMessage(dataBinaryReader, messageWithHeader.Value.Id);
                            DisplayMessage(TimeMessageBody);
                            qdb.Insert_Time(TimeMessageBody, messageWithHeader.Value.Id);
                            break;

                        case "53":
                            var System_EventMessageBody = System_EventMessage.BuildSystem_EventMessage(dataBinaryReader, messageWithHeader.Value.Id);
                            // DisplayMessage(System_EventMessageBody);
                            qdb.Insert_System_Event(System_EventMessageBody, messageWithHeader.Value.Id);
                            break;

                        case "52":
                            var Symbol_DirectoryMessageBody = Symbol_DirectoryMessage.BuildSymbol_DirectoryMessage(dataBinaryReader, messageWithHeader.Value.Id);
                            // DisplayMessage(Symbol_DirectoryMessageBody);
                            qdb.Insert_Symbol_Directory(Symbol_DirectoryMessageBody, messageWithHeader.Value.Id);
                            break;

                        case "48":
                            var Symbol_StatusMessageBody = Symbol_StatusMessage.BuildSymbol_StatusMessage(dataBinaryReader, messageWithHeader.Value.Id);
                            // DisplayMessage(Symbol_StatusMessageBody);
                            qdb.Insert_Symbol_Status(Symbol_StatusMessageBody, messageWithHeader.Value.Id);
                            break;

                        case "41":
                            var Add_OrderMessageBody = Add_OrderMessage.BuildAdd_OrderMessage(dataBinaryReader, messageWithHeader.Value.Id);
                            // DisplayMessage(Add_OrderMessageBody);
                            qdb.Insert_Add_Order(Add_OrderMessageBody, messageWithHeader.Value.Id);
                            break;

                        case "46":
                            var Add_Attributed_OrderMessageBody = Add_Attributed_OrderMessage.BuildAdd_Attributed_OrderMessage(dataBinaryReader, messageWithHeader.Value.Id);
                            // DisplayMessage(Add_Attributed_OrderMessageBody);
                            qdb.Insert_Add_Attributed_Order(Add_Attributed_OrderMessageBody, messageWithHeader.Value.Id);
                            break;

                        case "44":
                            var Order_DeletedMessageBody = Order_DeletedMessage.BuildOrder_DeletedMessage(dataBinaryReader, messageWithHeader.Value.Id);
                            // DisplayMessage(Order_DeletedMessageBody);
                            qdb.Insert_Order_Deleted(Order_DeletedMessageBody, messageWithHeader.Value.Id);
                            break;

                        case "55":
                            var Order_ModifiedMessageBody = Order_ModifiedMessage.BuildOrder_ModifiedMessage(dataBinaryReader, messageWithHeader.Value.Id);
                            // DisplayMessage(Order_ModifiedMessageBody);
                            qdb.Insert_Order_Modified(Order_ModifiedMessageBody, messageWithHeader.Value.Id);
                            break;

                        case "79":
                            var Order_Book_ClearMessageBody = Order_Book_ClearMessage.BuildOrder_Book_ClearMessage(dataBinaryReader, messageWithHeader.Value.Id);
                            // DisplayMessage(Order_Book_ClearMessageBody);
                            qdb.Insert_Order_Book_Clear(Order_Book_ClearMessageBody, messageWithHeader.Value.Id);
                            break;

                        case "45":
                            var Order_ExecutedMessageBody = Order_ExecutedMessage.BuildOrder_ExecutedMessage(dataBinaryReader, messageWithHeader.Value.Id);
                            // DisplayMessage(Order_ExecutedMessageBody);
                            qdb.Insert_Order_Executed(Order_ExecutedMessageBody, messageWithHeader.Value.Id);
                            break;

                        case "43":
                            var Order_Executed_With_PriceORSizeMessageBody = Order_Executed_With_PriceORSizeMessage.BuildOrder_Executed_With_PriceORSizeMessage(dataBinaryReader, messageWithHeader.Value.Id);
                            // DisplayMessage(Order_Executed_With_PriceORSizeMessageBody);
                            qdb.Insert_Order_Executed_With_PriceORSize(Order_Executed_With_PriceORSizeMessageBody, messageWithHeader.Value.Id);
                            break;

                        case "5":
                            var TradeMessageBody = TradeMessage.BuildTradeMessage(dataBinaryReader, messageWithHeader.Value.Id);
                            // DisplayMessage(TradeMessageBody);
                            qdb.Insert_Trade(TradeMessageBody, messageWithHeader.Value.Id);
                            break;

                        case "51":
                            var Auction_TradeMessageBody = Auction_TradeMessage.BuildAuction_TradeMessage(dataBinaryReader, messageWithHeader.Value.Id);
                            // DisplayMessage(Auction_TradeMessageBody);
                            qdb.Insert_Auction_Trade(Auction_TradeMessageBody, messageWithHeader.Value.Id);
                            break;

                        case "78":
                            var Off_Book_TradeMessageBody = Off_Book_TradeMessage.BuildOff_Book_TradeMessage(dataBinaryReader, messageWithHeader.Value.Id);
                            // DisplayMessage(Off_Book_TradeMessageBody);
                            qdb.Insert_Off_Book_Trade(Off_Book_TradeMessageBody, messageWithHeader.Value.Id);
                            break;

                        case "42":
                            var Trade_BreakMessageBody = Trade_BreakMessage.BuildTrade_BreakMessage(dataBinaryReader, messageWithHeader.Value.Id);
                            // DisplayMessage(Trade_BreakMessageBody);
                            qdb.Insert_Trade_Break(Trade_BreakMessageBody, messageWithHeader.Value.Id);
                            break;

                        case "76":
                            var Recovery_TradeMessageBody = Recovery_TradeMessage.BuildRecovery_TradeMessage(dataBinaryReader, messageWithHeader.Value.Id);
                            // DisplayMessage(Recovery_TradeMessageBody);
                            qdb.Insert_Recovery_Trade(Recovery_TradeMessageBody, messageWithHeader.Value.Id);
                            break;

                        case "49":
                            var Auction_InfoMessageBody = Auction_InfoMessage.BuildAuction_InfoMessage(dataBinaryReader, messageWithHeader.Value.Id);
                            // DisplayMessage(Auction_InfoMessageBody);
                            qdb.Insert_Auction_Info(Auction_InfoMessageBody, messageWithHeader.Value.Id);
                            break;

                        case "77":
                            var StatisticsMessageBody = StatisticsMessage.BuildStatisticsMessage(dataBinaryReader, messageWithHeader.Value.Id);
                            // DisplayMessage(StatisticsMessageBody);
                            qdb.Insert_Statistics(StatisticsMessageBody, messageWithHeader.Value.Id);
                            break;

                        case "8":
                            var Extended_StatisticsMessageBody = Extended_StatisticsMessage.BuildExtended_StatisticsMessage(dataBinaryReader, messageWithHeader.Value.Id);
                            // DisplayMessage(Extended_StatisticsMessageBody);
                            qdb.Insert_Extended_Statistics(Extended_StatisticsMessageBody, messageWithHeader.Value.Id);
                            break;

                        case "75":
                            var NewsMessageBody = NewsMessage.BuildNewsMessage(dataBinaryReader, messageWithHeader.Value.Id);
                            // DisplayMessage(NewsMessageBody);
                            qdb.Insert_News(NewsMessageBody, messageWithHeader.Value.Id);
                            break;

                        case "71":
                            var Top_of_BookMessageBody = Top_of_BookMessage.BuildTop_of_BookMessage(dataBinaryReader, messageWithHeader.Value.Id);
                            // DisplayMessage(Top_of_BookMessageBody);
                            qdb.Insert_Top_of_Book(Top_of_BookMessageBody, messageWithHeader.Value.Id);
                            break;

                    }
                }
            }
        }
        public static void DisplayMessage(TimeMessage msg) 
        {
            var s = $@"
            Length = {msg.Length}

            Message_Type = {msg.Message_Type}

            Seconds = {msg.Seconds}

            ";
            Console.WriteLine(s);
        }

        public static void DisplayMessage(System_EventMessage msg) 
        {
            var s = $@"
            Length = {msg.Length}

            Message_Type = {msg.Message_Type}

            Nanosecond = {msg.Nanosecond}

            Event_Code = {msg.Event_Code}

            ";
            Console.WriteLine(s);
        }

        public static void DisplayMessage(Symbol_DirectoryMessage msg) 
        {
            var s = $@"
            Length = {msg.Length}

            Message_Type = {msg.Message_Type}

            Nanosecond = {msg.Nanosecond}

            Instrument_ID = {msg.Instrument_ID}

            Reserved = {msg.Reserved}

            Reserved1 = {msg.Reserved1}

            Symbol_Status = {msg.Symbol_Status}

            ISIN = {msg.ISIN}

            Symbol = {msg.Symbol}

            TIDM = {msg.TIDM}

            Segment = {msg.Segment}

            Previous_Close_Price = {msg.Previous_Close_Price}

            Expiration_Date = {msg.Expiration_Date}

            Underlying = {msg.Underlying}

            Strike_Price = {msg.Strike_Price}

            Option_Type = {msg.Option_Type}

            Issuer = {msg.Issuer}

            Issue_Date = {msg.Issue_Date}

            Coupon = {msg.Coupon}

            Flags = {msg.Flags}

            Sub_Book = {msg.Sub_Book}

            Corporate_Action = {msg.Corporate_Action}

            Leg_1_Symbol = {msg.Leg_1_Symbol}

            Leg_2_Symbol = {msg.Leg_2_Symbol}

            Contract_Multiplier = {msg.Contract_Multiplier}

            Settlement_Method = {msg.Settlement_Method}

            Instrument_Sub_Category = {msg.Instrument_Sub_Category}

            ";
            Console.WriteLine(s);
        }

        public static void DisplayMessage(Symbol_StatusMessage msg) 
        {
            var s = $@"
            Length = {msg.Length}

            Message_Type = {msg.Message_Type}

            Nanosecond = {msg.Nanosecond}

            Instrument_ID = {msg.Instrument_ID}

            Reserved = {msg.Reserved}

            Reserved1 = {msg.Reserved1}

            Trading_Status = {msg.Trading_Status}

            Flags = {msg.Flags}

            Reason = {msg.Reason}

            Session_Change_Reason = {msg.Session_Change_Reason}

            New_End_Time = {msg.New_End_Time}

            Book_Type = {msg.Book_Type}

            ";
            Console.WriteLine(s);
        }

        public static void DisplayMessage(Add_OrderMessage msg) 
        {
            var s = $@"
            Length = {msg.Length}

            Message_Type = {msg.Message_Type}

            Nanosecond = {msg.Nanosecond}

            Order_ID = {msg.Order_ID}

            Side = {msg.Side}

            Quantity = {msg.Quantity}

            Instrument_ID = {msg.Instrument_ID}

            Reserved = {msg.Reserved}

            Reserved1 = {msg.Reserved1}

            Price = {msg.Price}

            Flags = {msg.Flags}

            ";
            Console.WriteLine(s);
        }

        public static void DisplayMessage(Add_Attributed_OrderMessage msg) 
        {
            var s = $@"
            Length = {msg.Length}

            Message_Type = {msg.Message_Type}

            Nanosecond = {msg.Nanosecond}

            Order_ID = {msg.Order_ID}

            Side = {msg.Side}

            Quantity = {msg.Quantity}

            Instrument_ID = {msg.Instrument_ID}

            Price = {msg.Price}

            Attribution = {msg.Attribution}

            Flags = {msg.Flags}

            ";
            Console.WriteLine(s);
        }

        public static void DisplayMessage(Order_DeletedMessage msg) 
        {
            var s = $@"
            Length = {msg.Length}

            Message_Type = {msg.Message_Type}

            Nanosecond = {msg.Nanosecond}

            Order_ID = {msg.Order_ID}

            ";
            Console.WriteLine(s);
        }

        public static void DisplayMessage(Order_ModifiedMessage msg) 
        {
            var s = $@"
            Length = {msg.Length}

            Message_Type = {msg.Message_Type}

            Nanosecond = {msg.Nanosecond}

            Order_ID = {msg.Order_ID}

            New_Quantity = {msg.New_Quantity}

            New_Price = {msg.New_Price}

            Flags = {msg.Flags}

            ";
            Console.WriteLine(s);
        }

        public static void DisplayMessage(Order_Book_ClearMessage msg) 
        {
            var s = $@"
            Length = {msg.Length}

            Message_Type = {msg.Message_Type}

            Nanosecond = {msg.Nanosecond}

            Instrument_ID = {msg.Instrument_ID}

            Sub_Book = {msg.Sub_Book}

            Book_Type = {msg.Book_Type}

            ";
            Console.WriteLine(s);
        }

        public static void DisplayMessage(Order_ExecutedMessage msg) 
        {
            var s = $@"
            Length = {msg.Length}

            Message_Type = {msg.Message_Type}

            Nanosecond = {msg.Nanosecond}

            Order_ID = {msg.Order_ID}

            Executed_Quantity = {msg.Executed_Quantity}

            Trade_ID = {msg.Trade_ID}

            LastOptPx = {msg.LastOptPx}

            Volatility = {msg.Volatility}

            Underlying_Reference_Price = {msg.Underlying_Reference_Price}

            ";
            Console.WriteLine(s);
        }

        public static void DisplayMessage(Order_Executed_With_PriceORSizeMessage msg) 
        {
            var s = $@"
            Length = {msg.Length}

            Message_Type = {msg.Message_Type}

            Nanosecond = {msg.Nanosecond}

            Order_ID = {msg.Order_ID}

            Executed_Quantity = {msg.Executed_Quantity}

            Display_Quantity = {msg.Display_Quantity}

            Trade_ID = {msg.Trade_ID}

            Printable = {msg.Printable}

            Price = {msg.Price}

            LastOptPx = {msg.LastOptPx}

            Volatility = {msg.Volatility}

            Underlying_Reference_Price = {msg.Underlying_Reference_Price}

            ";
            Console.WriteLine(s);
        }

        public static void DisplayMessage(TradeMessage msg) 
        {
            var s = $@"
            Length = {msg.Length}

            Message_Type = {msg.Message_Type}

            Nanosecond = {msg.Nanosecond}

            Executed_Quantity = {msg.Executed_Quantity}

            Instrument_ID = {msg.Instrument_ID}

            Reserved = {msg.Reserved}

            Reserved1 = {msg.Reserved1}

            Price = {msg.Price}

            Trade_ID = {msg.Trade_ID}

            Sub_Book = {msg.Sub_Book}

            Flags = {msg.Flags}

            TradeSub_Type = {msg.TradeSub_Type}

            LastOptPx = {msg.LastOptPx}

            Volatility = {msg.Volatility}

            Underlying_Reference_Price = {msg.Underlying_Reference_Price}

            ";
            Console.WriteLine(s);
        }

        public static void DisplayMessage(Auction_TradeMessage msg) 
        {
            var s = $@"
            Length = {msg.Length}

            Message_Type = {msg.Message_Type}

            Nanosecond = {msg.Nanosecond}

            Quantity = {msg.Quantity}

            Instrument_ID = {msg.Instrument_ID}

            Reserved = {msg.Reserved}

            Reserved1 = {msg.Reserved1}

            Price = {msg.Price}

            Trade_ID = {msg.Trade_ID}

            Auction_Type = {msg.Auction_Type}

            LastOptPx = {msg.LastOptPx}

            Volatility = {msg.Volatility}

            Underlying_Reference_Price = {msg.Underlying_Reference_Price}

            ";
            Console.WriteLine(s);
        }

        public static void DisplayMessage(Off_Book_TradeMessage msg) 
        {
            var s = $@"
            Length = {msg.Length}

            Message_Type = {msg.Message_Type}

            Nanosecond = {msg.Nanosecond}

            Executed_Quantity = {msg.Executed_Quantity}

            Instrument_ID = {msg.Instrument_ID}

            Reserved = {msg.Reserved}

            Reserved1 = {msg.Reserved1}

            Price = {msg.Price}

            Trade_ID = {msg.Trade_ID}

            Off_Book_Trade_Type = {msg.Off_Book_Trade_Type}

            Trade_Time = {msg.Trade_Time}

            Trade_Date = {msg.Trade_Date}

            LastOptPx = {msg.LastOptPx}

            Volatility = {msg.Volatility}

            Underlying_Reference_Price = {msg.Underlying_Reference_Price}

            ";
            Console.WriteLine(s);
        }

        public static void DisplayMessage(Trade_BreakMessage msg) 
        {
            var s = $@"
            Length = {msg.Length}

            Message_Type = {msg.Message_Type}

            Nanosecond = {msg.Nanosecond}

            Trade_ID = {msg.Trade_ID}

            Trade_Type = {msg.Trade_Type}

            ";
            Console.WriteLine(s);
        }

        public static void DisplayMessage(Recovery_TradeMessage msg) 
        {
            var s = $@"
            Length = {msg.Length}

            Message_Type = {msg.Message_Type}

            Nanosecond = {msg.Nanosecond}

            Executed_Quantity = {msg.Executed_Quantity}

            Instrument_ID = {msg.Instrument_ID}

            Reserved = {msg.Reserved}

            Reserved1 = {msg.Reserved1}

            Price = {msg.Price}

            Trade_ID = {msg.Trade_ID}

            Auction_Type = {msg.Auction_Type}

            Off_BookORRFQ_Trade_Type = {msg.Off_BookORRFQ_Trade_Type}

            Trade_Time = {msg.Trade_Time}

            Trade_Date = {msg.Trade_Date}

            Action_Type = {msg.Action_Type}

            Sub_Book = {msg.Sub_Book}

            Flags = {msg.Flags}

            LastOptPx = {msg.LastOptPx}

            Volatility = {msg.Volatility}

            Underlying_Reference_Price = {msg.Underlying_Reference_Price}

            ";
            Console.WriteLine(s);
        }

        public static void DisplayMessage(Auction_InfoMessage msg) 
        {
            var s = $@"
            Length = {msg.Length}

            Message_Type = {msg.Message_Type}

            Nanosecond = {msg.Nanosecond}

            Paired_Quantity = {msg.Paired_Quantity}

            Reserved = {msg.Reserved}

            Imbalance_Direction = {msg.Imbalance_Direction}

            Instrument_ID = {msg.Instrument_ID}

            Reserved1 = {msg.Reserved1}

            Reserved2 = {msg.Reserved2}

            Price = {msg.Price}

            Auction_Type = {msg.Auction_Type}

            ";
            Console.WriteLine(s);
        }

        public static void DisplayMessage(StatisticsMessage msg) 
        {
            var s = $@"
            Length = {msg.Length}

            Message_Type = {msg.Message_Type}

            Nanosecond = {msg.Nanosecond}

            Instrument_ID = {msg.Instrument_ID}

            Reserved = {msg.Reserved}

            Reserved1 = {msg.Reserved1}

            Statistic_Type = {msg.Statistic_Type}

            Price = {msg.Price}

            OpenClose_Indicator = {msg.OpenClose_Indicator}

            Sub_Book = {msg.Sub_Book}

            ";
            Console.WriteLine(s);
        }

        public static void DisplayMessage(Extended_StatisticsMessage msg) 
        {
            var s = $@"
            Length = {msg.Length}

            Message_Type = {msg.Message_Type}

            Nanosecond = {msg.Nanosecond}

            Instrument_ID = {msg.Instrument_ID}

            High_Price = {msg.High_Price}

            Low_Price = {msg.Low_Price}

            VWAP = {msg.VWAP}

            Volume = {msg.Volume}

            Turnover = {msg.Turnover}

            Number_of_Trades = {msg.Number_of_Trades}

            Reserved_Field = {msg.Reserved_Field}

            SubBook = {msg.SubBook}

            Notional_Exposure = {msg.Notional_Exposure}

            Notional_Delta_Exposure = {msg.Notional_Delta_Exposure}

            Open_Interest = {msg.Open_Interest}

            ";
            Console.WriteLine(s);
        }

        public static void DisplayMessage(NewsMessage msg) 
        {
            var s = $@"
            Length = {msg.Length}

            Message_Type = {msg.Message_Type}

            Nanosecond = {msg.Nanosecond}

            Time = {msg.Time}

            Urgency = {msg.Urgency}

            Headline = {msg.Headline}

            Text = {msg.Text}

            Instruments = {msg.Instruments}

            Underlyings = {msg.Underlyings}

            ";
            Console.WriteLine(s);
        }

        public static void DisplayMessage(Top_of_BookMessage msg) 
        {
            var s = $@"
            Length = {msg.Length}

            Message_Type = {msg.Message_Type}

            Nanosecond = {msg.Nanosecond}

            Instrument_ID = {msg.Instrument_ID}

            Reserve_Field = {msg.Reserve_Field}

            Sub_Book = {msg.Sub_Book}

            Action = {msg.Action}

            Side = {msg.Side}

            Price = {msg.Price}

            Quantity = {msg.Quantity}

            Market_Order_Quantity = {msg.Market_Order_Quantity}

            Reserved_Field = {msg.Reserved_Field}

            ";
            Console.WriteLine(s);
        }

    }
}
