// This is a generated file.  Don't edit it directly!

using System;
using System.IO;
using System.Text;

namespace backt
{
    public struct Add_Attributed_OrderMessage
    {
        public ushort Length;
        public string Message_Type;
        public uint Nanosecond;
        public string Order_ID;
        public string Side;
        public uint Quantity;
        public uint Instrument_ID;
        public long Price;
        public string Attribution;
        public byte Flags;

        public static Add_Attributed_OrderMessage BuildAdd_Attributed_OrderMessage(BinaryReader binaryReader, long headerId)
        {
            var UnitHeader = binaryReader.ReadBytes(8);
            

            var lengthBuffer = binaryReader.ReadBytes(2);
            var length = BitConverter.ToUInt16(lengthBuffer, 0);
            

            var message_typeBuffer = binaryReader.ReadBytes(1);
            var message_type = Encoding.ASCII.GetString(message_typeBuffer);
            

            var nanosecondBuffer = binaryReader.ReadBytes(4);
            var nanosecond = BitConverter.ToUInt32(nanosecondBuffer, 0);
            

            var order_idBuffer = binaryReader.ReadBytes(8);
            var temporder_id = BitConverter.ToUInt64(order_idBuffer, 0);
            var order_id = temporder_id.ToString();
            

            var sideBuffer = binaryReader.ReadBytes(1);
            var side = Encoding.ASCII.GetString(sideBuffer);
            

            var quantityBuffer = binaryReader.ReadBytes(4);
            var quantity = BitConverter.ToUInt32(quantityBuffer, 0);
            

            var instrument_idBuffer = binaryReader.ReadBytes(4);
            var instrument_id = BitConverter.ToUInt32(instrument_idBuffer, 0);
            

            var priceBuffer = binaryReader.ReadBytes(8);
            var price = BitConverter.ToInt64(priceBuffer);
            

            var attributionBuffer = binaryReader.ReadBytes(11);
            var attribution = Encoding.ASCII.GetString(attributionBuffer);
            

            var flags = binaryReader.ReadByte();
            


            return new Add_Attributed_OrderMessage()
            {
                Length = length,
                Message_Type = message_type,
                Nanosecond = nanosecond,
                Order_ID = order_id,
                Side = side,
                Quantity = quantity,
                Instrument_ID = instrument_id,
                Price = price,
                Attribution = attribution,
                Flags = flags,
            };
        }
    }
}
