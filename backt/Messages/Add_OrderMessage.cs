// This is a generated file.  Don't edit it directly!

using System;
using System.IO;
using System.Text;

namespace backt
{
    public struct Add_OrderMessage
    {
        public ushort Length;
        public string Message_Type;
        public uint Nanosecond;
        public string Order_ID;
        public string Side;
        public uint Quantity;
        public uint Instrument_ID;
        public string Reserved;
        public string Reserved1;
        public long Price;
        public byte Flags;

        public static Add_OrderMessage BuildAdd_OrderMessage(BinaryReader binaryReader, long headerId)
        {
            var UnitHeader = binaryReader.ReadBytes(8);
            

            var lengthBuffer = binaryReader.ReadBytes(2);
            var length = BitConverter.ToUInt16(lengthBuffer, 0);
            

            var message_typeBuffer = binaryReader.ReadBytes(1);
            var message_type = Encoding.ASCII.GetString(message_typeBuffer);
            

            var nanosecondBuffer = binaryReader.ReadBytes(4);
            var nanosecond = BitConverter.ToUInt32(nanosecondBuffer, 0);
            

            var order_idBuffer = binaryReader.ReadBytes(8);
            var temporder_id = BitConverter.ToUInt64(order_idBuffer, 0);
            var order_id = temporder_id.ToString();
            

            var sideBuffer = binaryReader.ReadBytes(1);
            var side = Encoding.ASCII.GetString(sideBuffer);
            

            var quantityBuffer = binaryReader.ReadBytes(4);
            var quantity = BitConverter.ToUInt32(quantityBuffer, 0);
            

            var instrument_idBuffer = binaryReader.ReadBytes(4);
            var instrument_id = BitConverter.ToUInt32(instrument_idBuffer, 0);
            

            var reservedBuffer = binaryReader.ReadBytes(1);
            var reserved = Encoding.ASCII.GetString(reservedBuffer);
            

            var reserved1Buffer = binaryReader.ReadBytes(1);
            var reserved1 = Encoding.ASCII.GetString(reserved1Buffer);
            

            var priceBuffer = binaryReader.ReadBytes(8);
            var price = BitConverter.ToInt64(priceBuffer);
            

            var flags = binaryReader.ReadByte();
            


            return new Add_OrderMessage()
            {
                Length = length,
                Message_Type = message_type,
                Nanosecond = nanosecond,
                Order_ID = order_id,
                Side = side,
                Quantity = quantity,
                Instrument_ID = instrument_id,
                Reserved = reserved,
                Reserved1 = reserved1,
                Price = price,
                Flags = flags,
            };
        }
    }
}
