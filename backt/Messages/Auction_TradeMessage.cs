// This is a generated file.  Don't edit it directly!

using System;
using System.IO;
using System.Text;

namespace backt
{
    public struct Auction_TradeMessage
    {
        public ushort Length;
        public string Message_Type;
        public uint Nanosecond;
        public uint Quantity;
        public uint Instrument_ID;
        public string Reserved;
        public string Reserved1;
        public long Price;
        public string Trade_ID;
        public string Auction_Type;
        public long LastOptPx;
        public long Volatility;
        public long Underlying_Reference_Price;

        public static Auction_TradeMessage BuildAuction_TradeMessage(BinaryReader binaryReader, long headerId)
        {
            var UnitHeader = binaryReader.ReadBytes(8);
            

            var lengthBuffer = binaryReader.ReadBytes(2);
            var length = BitConverter.ToUInt16(lengthBuffer, 0);
            

            var message_typeBuffer = binaryReader.ReadBytes(1);
            var message_type = Encoding.ASCII.GetString(message_typeBuffer);
            

            var nanosecondBuffer = binaryReader.ReadBytes(4);
            var nanosecond = BitConverter.ToUInt32(nanosecondBuffer, 0);
            

            var quantityBuffer = binaryReader.ReadBytes(4);
            var quantity = BitConverter.ToUInt32(quantityBuffer, 0);
            

            var instrument_idBuffer = binaryReader.ReadBytes(4);
            var instrument_id = BitConverter.ToUInt32(instrument_idBuffer, 0);
            

            var reservedBuffer = binaryReader.ReadBytes(1);
            var reserved = Encoding.ASCII.GetString(reservedBuffer);
            

            var reserved1Buffer = binaryReader.ReadBytes(1);
            var reserved1 = Encoding.ASCII.GetString(reserved1Buffer);
            

            var priceBuffer = binaryReader.ReadBytes(8);
            var price = BitConverter.ToInt64(priceBuffer);
            

            var trade_idBuffer = binaryReader.ReadBytes(8);
            var temptrade_id = BitConverter.ToUInt64(trade_idBuffer, 0);
            var trade_id = temptrade_id.ToString();
            

            var auction_typeBuffer = binaryReader.ReadBytes(1);
            var auction_type = Encoding.ASCII.GetString(auction_typeBuffer);
            

            var lastoptpxBuffer = binaryReader.ReadBytes(8);
            var lastoptpx = BitConverter.ToInt64(lastoptpxBuffer);
            

            var volatilityBuffer = binaryReader.ReadBytes(8);
            var volatility = BitConverter.ToInt64(volatilityBuffer);
            

            var underlying_reference_priceBuffer = binaryReader.ReadBytes(8);
            var underlying_reference_price = BitConverter.ToInt64(underlying_reference_priceBuffer);
            


            return new Auction_TradeMessage()
            {
                Length = length,
                Message_Type = message_type,
                Nanosecond = nanosecond,
                Quantity = quantity,
                Instrument_ID = instrument_id,
                Reserved = reserved,
                Reserved1 = reserved1,
                Price = price,
                Trade_ID = trade_id,
                Auction_Type = auction_type,
                LastOptPx = lastoptpx,
                Volatility = volatility,
                Underlying_Reference_Price = underlying_reference_price,
            };
        }
    }
}
