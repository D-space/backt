// This is a generated file.  Don't edit it directly!

using System;
using System.IO;
using System.Text;

namespace backt
{
    public struct Extended_StatisticsMessage
    {
        public ushort Length;
        public string Message_Type;
        public uint Nanosecond;
        public uint Instrument_ID;
        public long High_Price;
        public long Low_Price;
        public long VWAP;
        public uint Volume;
        public long Turnover;
        public uint Number_of_Trades;
        public string Reserved_Field;
        public byte SubBook;
        public long Notional_Exposure;
        public long Notional_Delta_Exposure;
        public long Open_Interest;

        public static Extended_StatisticsMessage BuildExtended_StatisticsMessage(BinaryReader binaryReader, long headerId)
        {
            var UnitHeader = binaryReader.ReadBytes(8);
            

            var lengthBuffer = binaryReader.ReadBytes(2);
            var length = BitConverter.ToUInt16(lengthBuffer, 0);
            

            var message_typeBuffer = binaryReader.ReadBytes(1);
            var message_type = Encoding.ASCII.GetString(message_typeBuffer);
            

            var nanosecondBuffer = binaryReader.ReadBytes(4);
            var nanosecond = BitConverter.ToUInt32(nanosecondBuffer, 0);
            

            var instrument_idBuffer = binaryReader.ReadBytes(4);
            var instrument_id = BitConverter.ToUInt32(instrument_idBuffer, 0);
            

            var high_priceBuffer = binaryReader.ReadBytes(8);
            var high_price = BitConverter.ToInt64(high_priceBuffer);
            

            var low_priceBuffer = binaryReader.ReadBytes(8);
            var low_price = BitConverter.ToInt64(low_priceBuffer);
            

            var vwapBuffer = binaryReader.ReadBytes(8);
            var vwap = BitConverter.ToInt64(vwapBuffer);
            

            var volumeBuffer = binaryReader.ReadBytes(4);
            var volume = BitConverter.ToUInt32(volumeBuffer, 0);
            

            var turnoverBuffer = binaryReader.ReadBytes(8);
            var turnover = BitConverter.ToInt64(turnoverBuffer);
            

            var number_of_tradesBuffer = binaryReader.ReadBytes(4);
            var number_of_trades = BitConverter.ToUInt32(number_of_tradesBuffer, 0);
            

            var reserved_fieldBuffer = binaryReader.ReadBytes(8);
            var reserved_field = Encoding.ASCII.GetString(reserved_fieldBuffer);
            

            var subbook = binaryReader.ReadByte();
            

            var notional_exposureBuffer = binaryReader.ReadBytes(8);
            var notional_exposure = BitConverter.ToInt64(notional_exposureBuffer);
            

            var notional_delta_exposureBuffer = binaryReader.ReadBytes(8);
            var notional_delta_exposure = BitConverter.ToInt64(notional_delta_exposureBuffer);
            

            var open_interestBuffer = binaryReader.ReadBytes(8);
            var open_interest = BitConverter.ToInt64(open_interestBuffer);
            


            return new Extended_StatisticsMessage()
            {
                Length = length,
                Message_Type = message_type,
                Nanosecond = nanosecond,
                Instrument_ID = instrument_id,
                High_Price = high_price,
                Low_Price = low_price,
                VWAP = vwap,
                Volume = volume,
                Turnover = turnover,
                Number_of_Trades = number_of_trades,
                Reserved_Field = reserved_field,
                SubBook = subbook,
                Notional_Exposure = notional_exposure,
                Notional_Delta_Exposure = notional_delta_exposure,
                Open_Interest = open_interest,
            };
        }
    }
}
