// This is a generated file.  Don't edit it directly!

using System;
using System.IO;
using System.Text;

namespace backt
{
    public struct Login_ResponseMessage
    {
        public ushort Length;
        public string Message_Type;
        public string Status;

        public static Login_ResponseMessage BuildLogin_ResponseMessage(BinaryReader binaryReader, long headerId)
        {
            var UnitHeader = binaryReader.ReadBytes(8);
            

            var lengthBuffer = binaryReader.ReadBytes(2);
            var length = BitConverter.ToUInt16(lengthBuffer, 0);
            

            var message_typeBuffer = binaryReader.ReadBytes(1);
            var message_type = Encoding.ASCII.GetString(message_typeBuffer);
            

            var statusBuffer = binaryReader.ReadBytes(1);
            var status = Encoding.ASCII.GetString(statusBuffer);
            


            return new Login_ResponseMessage()
            {
                Length = length,
                Message_Type = message_type,
                Status = status,
            };
        }
    }
}
