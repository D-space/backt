// This is a generated file.  Don't edit it directly!

using System;
using System.IO;
using System.Text;

namespace backt
{
    public struct NewsMessage
    {
        public ushort Length;
        public string Message_Type;
        public uint Nanosecond;
        public string Time;
        public string Urgency;
        public string Headline;
        public string Text;
        public string Instruments;
        public string Underlyings;

        public static NewsMessage BuildNewsMessage(BinaryReader binaryReader, long headerId)
        {
            var UnitHeader = binaryReader.ReadBytes(8);
            

            var lengthBuffer = binaryReader.ReadBytes(2);
            var length = BitConverter.ToUInt16(lengthBuffer, 0);
            

            var message_typeBuffer = binaryReader.ReadBytes(1);
            var message_type = Encoding.ASCII.GetString(message_typeBuffer);
            

            var nanosecondBuffer = binaryReader.ReadBytes(4);
            var nanosecond = BitConverter.ToUInt32(nanosecondBuffer, 0);
            

            var timeBuffer = binaryReader.ReadBytes(8);
            var time = Encoding.ASCII.GetString(timeBuffer);
            

            var urgencyBuffer = binaryReader.ReadBytes(1);
            var urgency = Encoding.ASCII.GetString(urgencyBuffer);
            

            var headlineBuffer = binaryReader.ReadBytes(100);
            var headline = Encoding.ASCII.GetString(headlineBuffer);
            

            var textBuffer = binaryReader.ReadBytes(750);
            var text = Encoding.ASCII.GetString(textBuffer);
            

            var instrumentsBuffer = binaryReader.ReadBytes(100);
            var instruments = Encoding.ASCII.GetString(instrumentsBuffer);
            

            var underlyingsBuffer = binaryReader.ReadBytes(100);
            var underlyings = Encoding.ASCII.GetString(underlyingsBuffer);
            


            return new NewsMessage()
            {
                Length = length,
                Message_Type = message_type,
                Nanosecond = nanosecond,
                Time = time,
                Urgency = urgency,
                Headline = headline,
                Text = text,
                Instruments = instruments,
                Underlyings = underlyings,
            };
        }
    }
}
