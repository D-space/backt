// This is a generated file.  Don't edit it directly!

using System;
using System.IO;
using System.Text;

namespace backt
{
    public struct Order_DeletedMessage
    {
        public ushort Length;
        public string Message_Type;
        public uint Nanosecond;
        public string Order_ID;

        public static Order_DeletedMessage BuildOrder_DeletedMessage(BinaryReader binaryReader, long headerId)
        {
            var UnitHeader = binaryReader.ReadBytes(8);
            

            var lengthBuffer = binaryReader.ReadBytes(2);
            var length = BitConverter.ToUInt16(lengthBuffer, 0);
            

            var message_typeBuffer = binaryReader.ReadBytes(1);
            var message_type = Encoding.ASCII.GetString(message_typeBuffer);
            

            var nanosecondBuffer = binaryReader.ReadBytes(4);
            var nanosecond = BitConverter.ToUInt32(nanosecondBuffer, 0);
            

            var order_idBuffer = binaryReader.ReadBytes(8);
            var temporder_id = BitConverter.ToUInt64(order_idBuffer, 0);
            var order_id = temporder_id.ToString();
            


            return new Order_DeletedMessage()
            {
                Length = length,
                Message_Type = message_type,
                Nanosecond = nanosecond,
                Order_ID = order_id,
            };
        }
    }
}
