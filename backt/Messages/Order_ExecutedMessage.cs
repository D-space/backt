// This is a generated file.  Don't edit it directly!

using System;
using System.IO;
using System.Text;

namespace backt
{
    public struct Order_ExecutedMessage
    {
        public ushort Length;
        public string Message_Type;
        public uint Nanosecond;
        public string Order_ID;
        public uint Executed_Quantity;
        public string Trade_ID;
        public long LastOptPx;
        public long Volatility;
        public long Underlying_Reference_Price;

        public static Order_ExecutedMessage BuildOrder_ExecutedMessage(BinaryReader binaryReader, long headerId)
        {
            var UnitHeader = binaryReader.ReadBytes(8);
            

            var lengthBuffer = binaryReader.ReadBytes(2);
            var length = BitConverter.ToUInt16(lengthBuffer, 0);
            

            var message_typeBuffer = binaryReader.ReadBytes(1);
            var message_type = Encoding.ASCII.GetString(message_typeBuffer);
            

            var nanosecondBuffer = binaryReader.ReadBytes(4);
            var nanosecond = BitConverter.ToUInt32(nanosecondBuffer, 0);
            

            var order_idBuffer = binaryReader.ReadBytes(8);
            var temporder_id = BitConverter.ToUInt64(order_idBuffer, 0);
            var order_id = temporder_id.ToString();
            

            var executed_quantityBuffer = binaryReader.ReadBytes(4);
            var executed_quantity = BitConverter.ToUInt32(executed_quantityBuffer, 0);
            

            var trade_idBuffer = binaryReader.ReadBytes(8);
            var temptrade_id = BitConverter.ToUInt64(trade_idBuffer, 0);
            var trade_id = temptrade_id.ToString();
            

            var lastoptpxBuffer = binaryReader.ReadBytes(8);
            var lastoptpx = BitConverter.ToInt64(lastoptpxBuffer);
            

            var volatilityBuffer = binaryReader.ReadBytes(8);
            var volatility = BitConverter.ToInt64(volatilityBuffer);
            

            var underlying_reference_priceBuffer = binaryReader.ReadBytes(8);
            var underlying_reference_price = BitConverter.ToInt64(underlying_reference_priceBuffer);
            


            return new Order_ExecutedMessage()
            {
                Length = length,
                Message_Type = message_type,
                Nanosecond = nanosecond,
                Order_ID = order_id,
                Executed_Quantity = executed_quantity,
                Trade_ID = trade_id,
                LastOptPx = lastoptpx,
                Volatility = volatility,
                Underlying_Reference_Price = underlying_reference_price,
            };
        }
    }
}
