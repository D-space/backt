// This is a generated file.  Don't edit it directly!

using System;
using System.IO;
using System.Text;

namespace backt
{
    public struct Order_ModifiedMessage
    {
        public ushort Length;
        public string Message_Type;
        public uint Nanosecond;
        public string Order_ID;
        public uint New_Quantity;
        public long New_Price;
        public byte Flags;

        public static Order_ModifiedMessage BuildOrder_ModifiedMessage(BinaryReader binaryReader, long headerId)
        {
            var UnitHeader = binaryReader.ReadBytes(8);
            

            var lengthBuffer = binaryReader.ReadBytes(2);
            var length = BitConverter.ToUInt16(lengthBuffer, 0);
            

            var message_typeBuffer = binaryReader.ReadBytes(1);
            var message_type = Encoding.ASCII.GetString(message_typeBuffer);
            

            var nanosecondBuffer = binaryReader.ReadBytes(4);
            var nanosecond = BitConverter.ToUInt32(nanosecondBuffer, 0);
            

            var order_idBuffer = binaryReader.ReadBytes(8);
            var temporder_id = BitConverter.ToUInt64(order_idBuffer, 0);
            var order_id = temporder_id.ToString();
            

            var new_quantityBuffer = binaryReader.ReadBytes(4);
            var new_quantity = BitConverter.ToUInt32(new_quantityBuffer, 0);
            

            var new_priceBuffer = binaryReader.ReadBytes(8);
            var new_price = BitConverter.ToInt64(new_priceBuffer);
            

            var flags = binaryReader.ReadByte();
            


            return new Order_ModifiedMessage()
            {
                Length = length,
                Message_Type = message_type,
                Nanosecond = nanosecond,
                Order_ID = order_id,
                New_Quantity = new_quantity,
                New_Price = new_price,
                Flags = flags,
            };
        }
    }
}
