// This is a generated file.  Don't edit it directly!

using System;
using System.IO;
using System.Text;

namespace backt
{
    public struct Recovery_TradeMessage
    {
        public ushort Length;
        public string Message_Type;
        public uint Nanosecond;
        public uint Executed_Quantity;
        public uint Instrument_ID;
        public string Reserved;
        public string Reserved1;
        public long Price;
        public string Trade_ID;
        public string Auction_Type;
        public string Off_BookORRFQ_Trade_Type;
        public string Trade_Time;
        public string Trade_Date;
        public string Action_Type;
        public byte Sub_Book;
        public byte Flags;
        public long LastOptPx;
        public long Volatility;
        public long Underlying_Reference_Price;

        public static Recovery_TradeMessage BuildRecovery_TradeMessage(BinaryReader binaryReader, long headerId)
        {
            var UnitHeader = binaryReader.ReadBytes(8);
            

            var lengthBuffer = binaryReader.ReadBytes(2);
            var length = BitConverter.ToUInt16(lengthBuffer, 0);
            

            var message_typeBuffer = binaryReader.ReadBytes(1);
            var message_type = Encoding.ASCII.GetString(message_typeBuffer);
            

            var nanosecondBuffer = binaryReader.ReadBytes(4);
            var nanosecond = BitConverter.ToUInt32(nanosecondBuffer, 0);
            

            var executed_quantityBuffer = binaryReader.ReadBytes(4);
            var executed_quantity = BitConverter.ToUInt32(executed_quantityBuffer, 0);
            

            var instrument_idBuffer = binaryReader.ReadBytes(4);
            var instrument_id = BitConverter.ToUInt32(instrument_idBuffer, 0);
            

            var reservedBuffer = binaryReader.ReadBytes(1);
            var reserved = Encoding.ASCII.GetString(reservedBuffer);
            

            var reserved1Buffer = binaryReader.ReadBytes(1);
            var reserved1 = Encoding.ASCII.GetString(reserved1Buffer);
            

            var priceBuffer = binaryReader.ReadBytes(8);
            var price = BitConverter.ToInt64(priceBuffer);
            

            var trade_idBuffer = binaryReader.ReadBytes(8);
            var temptrade_id = BitConverter.ToUInt64(trade_idBuffer, 0);
            var trade_id = temptrade_id.ToString();
            

            var auction_typeBuffer = binaryReader.ReadBytes(1);
            var auction_type = Encoding.ASCII.GetString(auction_typeBuffer);
            

            var off_bookorrfq_trade_typeBuffer = binaryReader.ReadBytes(4);
            var off_bookorrfq_trade_type = Encoding.ASCII.GetString(off_bookorrfq_trade_typeBuffer);
            

            var trade_timeBuffer = binaryReader.ReadBytes(8);
            var trade_time = Encoding.ASCII.GetString(trade_timeBuffer);
            

            var trade_dateBuffer = binaryReader.ReadBytes(8);
            var trade_date = Encoding.ASCII.GetString(trade_dateBuffer);
            

            var action_typeBuffer = binaryReader.ReadBytes(1);
            var action_type = Encoding.ASCII.GetString(action_typeBuffer);
            

            var sub_book = binaryReader.ReadByte();
            

            var flags = binaryReader.ReadByte();
            

            var lastoptpxBuffer = binaryReader.ReadBytes(8);
            var lastoptpx = BitConverter.ToInt64(lastoptpxBuffer);
            

            var volatilityBuffer = binaryReader.ReadBytes(8);
            var volatility = BitConverter.ToInt64(volatilityBuffer);
            

            var underlying_reference_priceBuffer = binaryReader.ReadBytes(8);
            var underlying_reference_price = BitConverter.ToInt64(underlying_reference_priceBuffer);
            


            return new Recovery_TradeMessage()
            {
                Length = length,
                Message_Type = message_type,
                Nanosecond = nanosecond,
                Executed_Quantity = executed_quantity,
                Instrument_ID = instrument_id,
                Reserved = reserved,
                Reserved1 = reserved1,
                Price = price,
                Trade_ID = trade_id,
                Auction_Type = auction_type,
                Off_BookORRFQ_Trade_Type = off_bookorrfq_trade_type,
                Trade_Time = trade_time,
                Trade_Date = trade_date,
                Action_Type = action_type,
                Sub_Book = sub_book,
                Flags = flags,
                LastOptPx = lastoptpx,
                Volatility = volatility,
                Underlying_Reference_Price = underlying_reference_price,
            };
        }
    }
}
