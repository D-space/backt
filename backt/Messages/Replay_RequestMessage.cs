// This is a generated file.  Don't edit it directly!

using System;
using System.IO;
using System.Text;

namespace backt
{
    public struct Replay_RequestMessage
    {
        public ushort Length;
        public string Message_Type;
        public string Market_Data_Group;
        public uint First_Message;
        public ushort Count;

        public static Replay_RequestMessage BuildReplay_RequestMessage(BinaryReader binaryReader, long headerId)
        {
            var UnitHeader = binaryReader.ReadBytes(8);
            

            var lengthBuffer = binaryReader.ReadBytes(2);
            var length = BitConverter.ToUInt16(lengthBuffer, 0);
            

            var message_typeBuffer = binaryReader.ReadBytes(1);
            var message_type = Encoding.ASCII.GetString(message_typeBuffer);
            

            var market_data_groupBuffer = binaryReader.ReadBytes(1);
            var market_data_group = Encoding.ASCII.GetString(market_data_groupBuffer);
            

            var first_messageBuffer = binaryReader.ReadBytes(4);
            var first_message = BitConverter.ToUInt32(first_messageBuffer, 0);
            

            var countBuffer = binaryReader.ReadBytes(2);
            var count = BitConverter.ToUInt16(countBuffer, 0);
            


            return new Replay_RequestMessage()
            {
                Length = length,
                Message_Type = message_type,
                Market_Data_Group = market_data_group,
                First_Message = first_message,
                Count = count,
            };
        }
    }
}
