// This is a generated file.  Don't edit it directly!

using System;
using System.IO;
using System.Text;

namespace backt
{
    public struct Snapshot_CompleteMessage
    {
        public ushort Length;
        public string Message_Type;
        public uint Sequence_Number;
        public string Segment;
        public uint Instrument_ID;
        public string Reserved;
        public string Reserved1;
        public byte Sub_Book;
        public string Trading_Status;
        public byte Snapshot_Type;
        public uint Request_ID;

        public static Snapshot_CompleteMessage BuildSnapshot_CompleteMessage(BinaryReader binaryReader, long headerId)
        {
            var UnitHeader = binaryReader.ReadBytes(8);
            

            var lengthBuffer = binaryReader.ReadBytes(2);
            var length = BitConverter.ToUInt16(lengthBuffer, 0);
            

            var message_typeBuffer = binaryReader.ReadBytes(1);
            var message_type = Encoding.ASCII.GetString(message_typeBuffer);
            

            var sequence_numberBuffer = binaryReader.ReadBytes(4);
            var sequence_number = BitConverter.ToUInt32(sequence_numberBuffer, 0);
            

            var segmentBuffer = binaryReader.ReadBytes(6);
            var segment = Encoding.ASCII.GetString(segmentBuffer);
            

            var instrument_idBuffer = binaryReader.ReadBytes(4);
            var instrument_id = BitConverter.ToUInt32(instrument_idBuffer, 0);
            

            var reservedBuffer = binaryReader.ReadBytes(1);
            var reserved = Encoding.ASCII.GetString(reservedBuffer);
            

            var reserved1Buffer = binaryReader.ReadBytes(1);
            var reserved1 = Encoding.ASCII.GetString(reserved1Buffer);
            

            var sub_book = binaryReader.ReadByte();
            

            var trading_statusBuffer = binaryReader.ReadBytes(1);
            var trading_status = Encoding.ASCII.GetString(trading_statusBuffer);
            

            var snapshot_type = binaryReader.ReadByte();
            

            var request_idBuffer = binaryReader.ReadBytes(4);
            var request_id = BitConverter.ToUInt32(request_idBuffer, 0);
            


            return new Snapshot_CompleteMessage()
            {
                Length = length,
                Message_Type = message_type,
                Sequence_Number = sequence_number,
                Segment = segment,
                Instrument_ID = instrument_id,
                Reserved = reserved,
                Reserved1 = reserved1,
                Sub_Book = sub_book,
                Trading_Status = trading_status,
                Snapshot_Type = snapshot_type,
                Request_ID = request_id,
            };
        }
    }
}
