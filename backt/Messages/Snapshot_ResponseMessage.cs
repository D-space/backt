// This is a generated file.  Don't edit it directly!

using System;
using System.IO;
using System.Text;

namespace backt
{
    public struct Snapshot_ResponseMessage
    {
        public ushort Length;
        public string Message_Type;
        public uint Sequence_Number;
        public uint Order_Count;
        public string Status;
        public byte Snapshot_Type;
        public uint Request_ID;

        public static Snapshot_ResponseMessage BuildSnapshot_ResponseMessage(BinaryReader binaryReader, long headerId)
        {
            var UnitHeader = binaryReader.ReadBytes(8);
            

            var lengthBuffer = binaryReader.ReadBytes(2);
            var length = BitConverter.ToUInt16(lengthBuffer, 0);
            

            var message_typeBuffer = binaryReader.ReadBytes(1);
            var message_type = Encoding.ASCII.GetString(message_typeBuffer);
            

            var sequence_numberBuffer = binaryReader.ReadBytes(4);
            var sequence_number = BitConverter.ToUInt32(sequence_numberBuffer, 0);
            

            var order_countBuffer = binaryReader.ReadBytes(4);
            var order_count = BitConverter.ToUInt32(order_countBuffer, 0);
            

            var statusBuffer = binaryReader.ReadBytes(1);
            var status = Encoding.ASCII.GetString(statusBuffer);
            

            var snapshot_type = binaryReader.ReadByte();
            

            var request_idBuffer = binaryReader.ReadBytes(4);
            var request_id = BitConverter.ToUInt32(request_idBuffer, 0);
            


            return new Snapshot_ResponseMessage()
            {
                Length = length,
                Message_Type = message_type,
                Sequence_Number = sequence_number,
                Order_Count = order_count,
                Status = status,
                Snapshot_Type = snapshot_type,
                Request_ID = request_id,
            };
        }
    }
}
