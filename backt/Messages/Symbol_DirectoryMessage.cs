// This is a generated file.  Don't edit it directly!

using System;
using System.IO;
using System.Text;

namespace backt
{
    public struct Symbol_DirectoryMessage
    {
        public ushort Length;
        public string Message_Type;
        public uint Nanosecond;
        public uint Instrument_ID;
        public string Reserved;
        public string Reserved1;
        public string Symbol_Status;
        public string ISIN;
        public string Symbol;
        public string TIDM;
        public string Segment;
        public long Previous_Close_Price;
        public string Expiration_Date;
        public string Underlying;
        public long Strike_Price;
        public string Option_Type;
        public string Issuer;
        public string Issue_Date;
        public long Coupon;
        public byte Flags;
        public byte Sub_Book;
        public string Corporate_Action;
        public string Leg_1_Symbol;
        public string Leg_2_Symbol;
        public long Contract_Multiplier;
        public string Settlement_Method;
        public string Instrument_Sub_Category;

        public static Symbol_DirectoryMessage BuildSymbol_DirectoryMessage(BinaryReader binaryReader, long headerId)
        {
            var UnitHeader = binaryReader.ReadBytes(8);
            

            var lengthBuffer = binaryReader.ReadBytes(2);
            var length = BitConverter.ToUInt16(lengthBuffer, 0);
            

            var message_typeBuffer = binaryReader.ReadBytes(1);
            var message_type = Encoding.ASCII.GetString(message_typeBuffer);
            

            var nanosecondBuffer = binaryReader.ReadBytes(4);
            var nanosecond = BitConverter.ToUInt32(nanosecondBuffer, 0);
            

            var instrument_idBuffer = binaryReader.ReadBytes(4);
            var instrument_id = BitConverter.ToUInt32(instrument_idBuffer, 0);
            

            var reservedBuffer = binaryReader.ReadBytes(1);
            var reserved = Encoding.ASCII.GetString(reservedBuffer);
            

            var reserved1Buffer = binaryReader.ReadBytes(1);
            var reserved1 = Encoding.ASCII.GetString(reserved1Buffer);
            

            var symbol_statusBuffer = binaryReader.ReadBytes(1);
            var symbol_status = Encoding.ASCII.GetString(symbol_statusBuffer);
            

            var isinBuffer = binaryReader.ReadBytes(12);
            var isin = Encoding.ASCII.GetString(isinBuffer);
            

            var symbolBuffer = binaryReader.ReadBytes(25);
            var symbol = Encoding.ASCII.GetString(symbolBuffer);
            

            var tidmBuffer = binaryReader.ReadBytes(12);
            var tidm = Encoding.ASCII.GetString(tidmBuffer);
            

            var segmentBuffer = binaryReader.ReadBytes(6);
            var segment = Encoding.ASCII.GetString(segmentBuffer);
            

            var previous_close_priceBuffer = binaryReader.ReadBytes(8);
            var previous_close_price = BitConverter.ToInt64(previous_close_priceBuffer);
            

            var expiration_dateBuffer = binaryReader.ReadBytes(8);
            var expiration_date = Encoding.ASCII.GetString(expiration_dateBuffer);
            

            var underlyingBuffer = binaryReader.ReadBytes(25);
            var underlying = Encoding.ASCII.GetString(underlyingBuffer);
            

            var strike_priceBuffer = binaryReader.ReadBytes(8);
            var strike_price = BitConverter.ToInt64(strike_priceBuffer);
            

            var option_typeBuffer = binaryReader.ReadBytes(1);
            var option_type = Encoding.ASCII.GetString(option_typeBuffer);
            

            var issuerBuffer = binaryReader.ReadBytes(6);
            var issuer = Encoding.ASCII.GetString(issuerBuffer);
            

            var issue_dateBuffer = binaryReader.ReadBytes(8);
            var issue_date = Encoding.ASCII.GetString(issue_dateBuffer);
            

            var couponBuffer = binaryReader.ReadBytes(8);
            var coupon = BitConverter.ToInt64(couponBuffer);
            

            var flags = binaryReader.ReadByte();
            

            var sub_book = binaryReader.ReadByte();
            

            var corporate_actionBuffer = binaryReader.ReadBytes(189);
            var corporate_action = Encoding.ASCII.GetString(corporate_actionBuffer);
            

            var leg_1_symbolBuffer = binaryReader.ReadBytes(25);
            var leg_1_symbol = Encoding.ASCII.GetString(leg_1_symbolBuffer);
            

            var leg_2_symbolBuffer = binaryReader.ReadBytes(25);
            var leg_2_symbol = Encoding.ASCII.GetString(leg_2_symbolBuffer);
            

            // As per doc
            // contract multiplier will contain 0 value if instrument does not have multipler
            // ie dont process. Use the underlying value to determine if we need to process field
            var contract_multiplier = 0L;
            if (!string.IsNullOrWhiteSpace(underlying))
            {
                Console.WriteLine("underlying is not null");
                var contract_multiplierBuffer = binaryReader.ReadBytes(8);
                contract_multiplier = BitConverter.ToInt64(contract_multiplierBuffer,0);
            }
            

            var settlement_methodBuffer = binaryReader.ReadBytes(1);
            var settlement_method = Encoding.ASCII.GetString(settlement_methodBuffer);
            

            var instrument_sub_categoryBuffer = binaryReader.ReadBytes(30);
            var instrument_sub_category = Encoding.ASCII.GetString(instrument_sub_categoryBuffer);
            


            return new Symbol_DirectoryMessage()
            {
                Length = length,
                Message_Type = message_type,
                Nanosecond = nanosecond,
                Instrument_ID = instrument_id,
                Reserved = reserved,
                Reserved1 = reserved1,
                Symbol_Status = symbol_status,
                ISIN = isin,
                Symbol = symbol,
                TIDM = tidm,
                Segment = segment,
                Previous_Close_Price = previous_close_price,
                Expiration_Date = expiration_date,
                Underlying = underlying,
                Strike_Price = strike_price,
                Option_Type = option_type,
                Issuer = issuer,
                Issue_Date = issue_date,
                Coupon = coupon,
                Flags = flags,
                Sub_Book = sub_book,
                Corporate_Action = corporate_action,
                Leg_1_Symbol = leg_1_symbol,
                Leg_2_Symbol = leg_2_symbol,
                Contract_Multiplier = contract_multiplier,
                Settlement_Method = settlement_method,
                Instrument_Sub_Category = instrument_sub_category,
            };
        }
    }
}
