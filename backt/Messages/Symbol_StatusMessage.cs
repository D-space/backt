// This is a generated file.  Don't edit it directly!

using System;
using System.IO;
using System.Text;

namespace backt
{
    public struct Symbol_StatusMessage
    {
        public ushort Length;
        public string Message_Type;
        public uint Nanosecond;
        public uint Instrument_ID;
        public string Reserved;
        public string Reserved1;
        public string Trading_Status;
        public byte Flags;
        public string Reason;
        public byte Session_Change_Reason;
        public string New_End_Time;
        public byte Book_Type;

        public static Symbol_StatusMessage BuildSymbol_StatusMessage(BinaryReader binaryReader, long headerId)
        {
            var UnitHeader = binaryReader.ReadBytes(8);
            

            var lengthBuffer = binaryReader.ReadBytes(2);
            var length = BitConverter.ToUInt16(lengthBuffer, 0);
            

            var message_typeBuffer = binaryReader.ReadBytes(1);
            var message_type = Encoding.ASCII.GetString(message_typeBuffer);
            

            var nanosecondBuffer = binaryReader.ReadBytes(4);
            var nanosecond = BitConverter.ToUInt32(nanosecondBuffer, 0);
            

            var instrument_idBuffer = binaryReader.ReadBytes(4);
            var instrument_id = BitConverter.ToUInt32(instrument_idBuffer, 0);
            

            var reservedBuffer = binaryReader.ReadBytes(1);
            var reserved = Encoding.ASCII.GetString(reservedBuffer);
            

            var reserved1Buffer = binaryReader.ReadBytes(1);
            var reserved1 = Encoding.ASCII.GetString(reserved1Buffer);
            

            var trading_statusBuffer = binaryReader.ReadBytes(1);
            var trading_status = Encoding.ASCII.GetString(trading_statusBuffer);
            

            var flags = binaryReader.ReadByte();
            

            var reasonBuffer = binaryReader.ReadBytes(4);
            var reason = Encoding.ASCII.GetString(reasonBuffer);
            

            var session_change_reason = binaryReader.ReadByte();
            

            var new_end_timeBuffer = binaryReader.ReadBytes(8);
            var new_end_time = Encoding.ASCII.GetString(new_end_timeBuffer);
            

            var book_type = binaryReader.ReadByte();
            


            return new Symbol_StatusMessage()
            {
                Length = length,
                Message_Type = message_type,
                Nanosecond = nanosecond,
                Instrument_ID = instrument_id,
                Reserved = reserved,
                Reserved1 = reserved1,
                Trading_Status = trading_status,
                Flags = flags,
                Reason = reason,
                Session_Change_Reason = session_change_reason,
                New_End_Time = new_end_time,
                Book_Type = book_type,
            };
        }
    }
}
