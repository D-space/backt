// This is a generated file.  Don't edit it directly!

using System;
using System.IO;
using System.Text;

namespace backt
{
    public struct System_EventMessage
    {
        public ushort Length;
        public string Message_Type;
        public uint Nanosecond;
        public string Event_Code;

        public static System_EventMessage BuildSystem_EventMessage(BinaryReader binaryReader, long headerId)
        {
            var UnitHeader = binaryReader.ReadBytes(8);
            

            var lengthBuffer = binaryReader.ReadBytes(2);
            var length = BitConverter.ToUInt16(lengthBuffer, 0);
            

            var message_typeBuffer = binaryReader.ReadBytes(1);
            var message_type = Encoding.ASCII.GetString(message_typeBuffer);
            

            var nanosecondBuffer = binaryReader.ReadBytes(4);
            var nanosecond = BitConverter.ToUInt32(nanosecondBuffer, 0);
            

            var event_codeBuffer = binaryReader.ReadBytes(1);
            var event_code = Encoding.ASCII.GetString(event_codeBuffer);
            


            return new System_EventMessage()
            {
                Length = length,
                Message_Type = message_type,
                Nanosecond = nanosecond,
                Event_Code = event_code,
            };
        }
    }
}
