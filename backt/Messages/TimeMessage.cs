// This is a generated file.  Don't edit it directly!

using System;
using System.IO;
using System.Text;

namespace backt
{
    public struct TimeMessage
    {
        public ushort Length;
        public string Message_Type;
        public uint Seconds;

        public static TimeMessage BuildTimeMessage(BinaryReader binaryReader, long headerId)
        {
            var UnitHeader = binaryReader.ReadBytes(8);
            

            var lengthBuffer = binaryReader.ReadBytes(2);
            var length = BitConverter.ToUInt16(lengthBuffer, 0);
            

            var message_typeBuffer = binaryReader.ReadBytes(1);
            var message_type = Encoding.ASCII.GetString(message_typeBuffer);
            

            var secondsBuffer = binaryReader.ReadBytes(4);
            var seconds = BitConverter.ToUInt32(secondsBuffer, 0);
            


            return new TimeMessage()
            {
                Length = length,
                Message_Type = message_type,
                Seconds = seconds,
            };
        }
    }
}
