// This is a generated file.  Don't edit it directly!

using System;
using System.IO;
using System.Text;

namespace backt
{
    public struct Top_of_BookMessage
    {
        public ushort Length;
        public string Message_Type;
        public uint Nanosecond;
        public uint Instrument_ID;
        public ushort Reserve_Field;
        public byte Sub_Book;
        public string Action;
        public string Side;
        public long Price;
        public uint Quantity;
        public uint Market_Order_Quantity;
        public ushort Reserved_Field;

        public static Top_of_BookMessage BuildTop_of_BookMessage(BinaryReader binaryReader, long headerId)
        {
            var UnitHeader = binaryReader.ReadBytes(8);
            

            var lengthBuffer = binaryReader.ReadBytes(2);
            var length = BitConverter.ToUInt16(lengthBuffer, 0);
            

            var message_typeBuffer = binaryReader.ReadBytes(1);
            var message_type = Encoding.ASCII.GetString(message_typeBuffer);
            

            var nanosecondBuffer = binaryReader.ReadBytes(4);
            var nanosecond = BitConverter.ToUInt32(nanosecondBuffer, 0);
            

            var instrument_idBuffer = binaryReader.ReadBytes(4);
            var instrument_id = BitConverter.ToUInt32(instrument_idBuffer, 0);
            

            var reserve_fieldBuffer = binaryReader.ReadBytes(2);
            var reserve_field = BitConverter.ToUInt16(reserve_fieldBuffer, 0);
            

            var sub_book = binaryReader.ReadByte();
            

            var actionBuffer = binaryReader.ReadBytes(1);
            var action = Encoding.ASCII.GetString(actionBuffer);
            

            var sideBuffer = binaryReader.ReadBytes(1);
            var side = Encoding.ASCII.GetString(sideBuffer);
            

            var priceBuffer = binaryReader.ReadBytes(8);
            var price = BitConverter.ToInt64(priceBuffer);
            

            var quantityBuffer = binaryReader.ReadBytes(4);
            var quantity = BitConverter.ToUInt32(quantityBuffer, 0);
            

            var market_order_quantityBuffer = binaryReader.ReadBytes(4);
            var market_order_quantity = BitConverter.ToUInt32(market_order_quantityBuffer, 0);
            

            var reserved_fieldBuffer = binaryReader.ReadBytes(2);
            var reserved_field = BitConverter.ToUInt16(reserved_fieldBuffer, 0);
            


            return new Top_of_BookMessage()
            {
                Length = length,
                Message_Type = message_type,
                Nanosecond = nanosecond,
                Instrument_ID = instrument_id,
                Reserve_Field = reserve_field,
                Sub_Book = sub_book,
                Action = action,
                Side = side,
                Price = price,
                Quantity = quantity,
                Market_Order_Quantity = market_order_quantity,
                Reserved_Field = reserved_field,
            };
        }
    }
}
