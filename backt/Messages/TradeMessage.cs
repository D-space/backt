// This is a generated file.  Don't edit it directly!

using System;
using System.IO;
using System.Text;

namespace backt
{
    public struct TradeMessage
    {
        public ushort Length;
        public string Message_Type;
        public uint Nanosecond;
        public uint Executed_Quantity;
        public uint Instrument_ID;
        public string Reserved;
        public string Reserved1;
        public long Price;
        public string Trade_ID;
        public byte Sub_Book;
        public byte Flags;
        public string TradeSub_Type;
        public long LastOptPx;
        public long Volatility;
        public long Underlying_Reference_Price;

        public static TradeMessage BuildTradeMessage(BinaryReader binaryReader, long headerId)
        {
            var UnitHeader = binaryReader.ReadBytes(8);
            

            var lengthBuffer = binaryReader.ReadBytes(2);
            var length = BitConverter.ToUInt16(lengthBuffer, 0);
            

            var message_typeBuffer = binaryReader.ReadBytes(1);
            var message_type = Encoding.ASCII.GetString(message_typeBuffer);
            

            var nanosecondBuffer = binaryReader.ReadBytes(4);
            var nanosecond = BitConverter.ToUInt32(nanosecondBuffer, 0);
            

            var executed_quantityBuffer = binaryReader.ReadBytes(4);
            var executed_quantity = BitConverter.ToUInt32(executed_quantityBuffer, 0);
            

            var instrument_idBuffer = binaryReader.ReadBytes(4);
            var instrument_id = BitConverter.ToUInt32(instrument_idBuffer, 0);
            

            var reservedBuffer = binaryReader.ReadBytes(1);
            var reserved = Encoding.ASCII.GetString(reservedBuffer);
            

            var reserved1Buffer = binaryReader.ReadBytes(1);
            var reserved1 = Encoding.ASCII.GetString(reserved1Buffer);
            

            var priceBuffer = binaryReader.ReadBytes(8);
            var price = BitConverter.ToInt64(priceBuffer);
            

            var trade_idBuffer = binaryReader.ReadBytes(8);
            var temptrade_id = BitConverter.ToUInt64(trade_idBuffer, 0);
            var trade_id = temptrade_id.ToString();
            

            var sub_book = binaryReader.ReadByte();
            

            var flags = binaryReader.ReadByte();
            

            var tradesub_typeBuffer = binaryReader.ReadBytes(4);
            var tradesub_type = Encoding.ASCII.GetString(tradesub_typeBuffer);
            

            var lastoptpxBuffer = binaryReader.ReadBytes(8);
            var lastoptpx = BitConverter.ToInt64(lastoptpxBuffer);
            

            var volatilityBuffer = binaryReader.ReadBytes(8);
            var volatility = BitConverter.ToInt64(volatilityBuffer);
            

            var underlying_reference_priceBuffer = binaryReader.ReadBytes(8);
            var underlying_reference_price = BitConverter.ToInt64(underlying_reference_priceBuffer);
            


            return new TradeMessage()
            {
                Length = length,
                Message_Type = message_type,
                Nanosecond = nanosecond,
                Executed_Quantity = executed_quantity,
                Instrument_ID = instrument_id,
                Reserved = reserved,
                Reserved1 = reserved1,
                Price = price,
                Trade_ID = trade_id,
                Sub_Book = sub_book,
                Flags = flags,
                TradeSub_Type = tradesub_type,
                LastOptPx = lastoptpx,
                Volatility = volatility,
                Underlying_Reference_Price = underlying_reference_price,
            };
        }
    }
}
