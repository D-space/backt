// This is a generated file.  Don't edit it directly!

using System;
using System.IO;
using System.Text;
using Npgsql;

namespace backt
{
    public class QuestConnGen
    {

        public NpgsqlConnection conn;


        public QuestConnGen(string username, string password, string database, int port)
        {

            string connectionString = $"host=127.0.0.1;port={port};username={username};password={password};database={database};ServerCompatibilityMode=NoTypeLoading;";
            Console.WriteLine("New Quest connection");
            conn = new NpgsqlConnection(connectionString);
            conn.Open();
            Console.WriteLine("Opened");
        }


        public void DeleteTables()
        {
            using( var cmd = new NpgsqlCommand("DROP TABLE Time", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("DROP TABLE System_Event", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("DROP TABLE Symbol_Directory", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("DROP TABLE Symbol_Status", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("DROP TABLE Add_Order", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("DROP TABLE Add_Attributed_Order", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("DROP TABLE Order_Deleted", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("DROP TABLE Order_Modified", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("DROP TABLE Order_Book_Clear", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("DROP TABLE Order_Executed", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("DROP TABLE Order_Executed_With_PriceORSize", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("DROP TABLE Trade", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("DROP TABLE Auction_Trade", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("DROP TABLE Off_Book_Trade", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("DROP TABLE Trade_Break", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("DROP TABLE Recovery_Trade", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("DROP TABLE Auction_Info", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("DROP TABLE Statistics", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("DROP TABLE Extended_Statistics", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("DROP TABLE News", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("DROP TABLE Top_of_Book", conn))
            {
                cmd.ExecuteNonQuery();
            }

        }


        public void SetupTables()
        {
            using( var cmd = new NpgsqlCommand("CREATE TABLE IF NOT EXISTS Time (seq_id LONG, length INT, message_type STRING, seconds LONG);", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("CREATE TABLE IF NOT EXISTS System_Event (seq_id LONG, length INT, message_type STRING, nanosecond LONG, event_code STRING);", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("CREATE TABLE IF NOT EXISTS Symbol_Directory (seq_id LONG, length INT, message_type STRING, nanosecond LONG, instrument_id LONG, reserved STRING, reserved1 STRING, symbol_status STRING, isin STRING, symbol STRING, tidm STRING, segment STRING, previous_close_price LONG, expiration_date STRING, underlying STRING, strike_price LONG, option_type STRING, issuer STRING, issue_date STRING, coupon LONG, flags BYTE, sub_book BYTE, corporate_action STRING, leg_1_symbol STRING, leg_2_symbol STRING, contract_multiplier LONG, settlement_method STRING, instrument_sub_category STRING);", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("CREATE TABLE IF NOT EXISTS Symbol_Status (seq_id LONG, length INT, message_type STRING, nanosecond LONG, instrument_id LONG, reserved STRING, reserved1 STRING, trading_status STRING, flags BYTE, reason STRING, session_change_reason INT, new_end_time STRING, book_type INT);", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("CREATE TABLE IF NOT EXISTS Add_Order (seq_id LONG, length INT, message_type STRING, nanosecond LONG, order_id STRING, side STRING, quantity LONG, instrument_id LONG, reserved STRING, reserved1 STRING, price LONG, flags BYTE);", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("CREATE TABLE IF NOT EXISTS Add_Attributed_Order (seq_id LONG, length INT, message_type STRING, nanosecond LONG, order_id STRING, side STRING, quantity LONG, instrument_id LONG, price LONG, attribution STRING, flags BYTE);", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("CREATE TABLE IF NOT EXISTS Order_Deleted (seq_id LONG, length INT, message_type STRING, nanosecond LONG, order_id STRING);", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("CREATE TABLE IF NOT EXISTS Order_Modified (seq_id LONG, length INT, message_type STRING, nanosecond LONG, order_id STRING, new_quantity LONG, new_price LONG, flags BYTE);", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("CREATE TABLE IF NOT EXISTS Order_Book_Clear (seq_id LONG, length INT, message_type STRING, nanosecond LONG, instrument_id LONG, sub_book INT, book_type STRING);", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("CREATE TABLE IF NOT EXISTS Order_Executed (seq_id LONG, length INT, message_type STRING, nanosecond LONG, order_id STRING, executed_quantity LONG, trade_id STRING, lastoptpx LONG, volatility LONG, underlying_reference_price LONG);", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("CREATE TABLE IF NOT EXISTS Order_Executed_With_PriceORSize (seq_id LONG, length INT, message_type STRING, nanosecond LONG, order_id STRING, executed_quantity LONG, display_quantity LONG, trade_id STRING, printable STRING, price LONG, lastoptpx LONG, volatility LONG, underlying_reference_price LONG);", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("CREATE TABLE IF NOT EXISTS Trade (seq_id LONG, length INT, message_type STRING, nanosecond LONG, executed_quantity LONG, instrument_id LONG, reserved STRING, reserved1 STRING, price LONG, trade_id STRING, sub_book INT, flags BYTE, tradesub_type STRING, lastoptpx LONG, volatility LONG, underlying_reference_price LONG);", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("CREATE TABLE IF NOT EXISTS Auction_Trade (seq_id LONG, length INT, message_type STRING, nanosecond LONG, quantity LONG, instrument_id LONG, reserved STRING, reserved1 STRING, price LONG, trade_id STRING, auction_type STRING, lastoptpx LONG, volatility LONG, underlying_reference_price LONG);", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("CREATE TABLE IF NOT EXISTS Off_Book_Trade (seq_id LONG, length INT, message_type STRING, nanosecond LONG, executed_quantity LONG, instrument_id LONG, reserved STRING, reserved1 STRING, price LONG, trade_id STRING, off_book_trade_type STRING, trade_time STRING, trade_date STRING, lastoptpx LONG, volatility LONG, underlying_reference_price LONG);", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("CREATE TABLE IF NOT EXISTS Trade_Break (seq_id LONG, length INT, message_type STRING, nanosecond LONG, trade_id STRING, trade_type STRING);", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("CREATE TABLE IF NOT EXISTS Recovery_Trade (seq_id LONG, length INT, message_type STRING, nanosecond LONG, executed_quantity LONG, instrument_id LONG, reserved STRING, reserved1 STRING, price LONG, trade_id STRING, auction_type STRING, off_bookorrfq_trade_type STRING, trade_time STRING, trade_date STRING, action_type STRING, sub_book INT, flags BYTE, lastoptpx LONG, volatility LONG, underlying_reference_price LONG);", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("CREATE TABLE IF NOT EXISTS Auction_Info (seq_id LONG, length INT, message_type STRING, nanosecond LONG, paired_quantity LONG, reserved LONG, imbalance_direction STRING, instrument_id LONG, reserved1 STRING, reserved2 STRING, price LONG, auction_type STRING);", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("CREATE TABLE IF NOT EXISTS Statistics (seq_id LONG, length INT, message_type STRING, nanosecond LONG, instrument_id LONG, reserved STRING, reserved1 STRING, statistic_type STRING, price LONG, openclose_indicator STRING, sub_book INT);", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("CREATE TABLE IF NOT EXISTS Extended_Statistics (seq_id LONG, length INT, message_type STRING, nanosecond LONG, instrument_id LONG, high_price LONG, low_price LONG, vwap LONG, volume LONG, turnover LONG, number_of_trades LONG, reserved_field STRING, subbook INT, notional_exposure LONG, notional_delta_exposure LONG, open_interest LONG);", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("CREATE TABLE IF NOT EXISTS News (seq_id LONG, length INT, message_type STRING, nanosecond LONG, time STRING, urgency STRING, headline STRING, text STRING, instruments STRING, underlyings STRING);", conn))
            {
                cmd.ExecuteNonQuery();
            }

            using( var cmd = new NpgsqlCommand("CREATE TABLE IF NOT EXISTS Top_of_Book (seq_id LONG, length INT, message_type STRING, nanosecond LONG, instrument_id LONG, reserve_field INT, sub_book BYTE, action STRING, side STRING, price LONG, quantity LONG, market_order_quantity LONG, reserved_field INT);", conn))
            {
                cmd.ExecuteNonQuery();
            }

        }
        public void Insert_Time(TimeMessage msgObj, long msgOrderSeqId)
        {
            using( var cmd = new NpgsqlCommand("INSERT INTO Time (seq_id, length, message_type, seconds) VALUES (@s, @length, @message_type, @seconds) ;", conn))
            {
                cmd.Parameters.AddWithValue("s", msgOrderSeqId);

                cmd.Parameters.AddWithValue("length", (int) msgObj.Length);
                cmd.Parameters.AddWithValue("message_type", (string) msgObj.Message_Type);
                cmd.Parameters.AddWithValue("seconds", (long) msgObj.Seconds);

                cmd.ExecuteNonQuery();
            }
        }

        public void Insert_System_Event(System_EventMessage msgObj, long msgOrderSeqId)
        {
            using( var cmd = new NpgsqlCommand("INSERT INTO System_Event (seq_id, length, message_type, nanosecond, event_code) VALUES (@s, @length, @message_type, @nanosecond, @event_code) ;", conn))
            {
                cmd.Parameters.AddWithValue("s", msgOrderSeqId);

                cmd.Parameters.AddWithValue("length", (int) msgObj.Length);
                cmd.Parameters.AddWithValue("message_type", (string) msgObj.Message_Type);
                cmd.Parameters.AddWithValue("nanosecond", (long) msgObj.Nanosecond);
                cmd.Parameters.AddWithValue("event_code", (string) msgObj.Event_Code);

                cmd.ExecuteNonQuery();
            }
        }

        public void Insert_Symbol_Directory(Symbol_DirectoryMessage msgObj, long msgOrderSeqId)
        {
            using( var cmd = new NpgsqlCommand("INSERT INTO Symbol_Directory (seq_id, length, message_type, nanosecond, instrument_id, reserved, reserved1, symbol_status, isin, symbol, tidm, segment, previous_close_price, expiration_date, underlying, strike_price, option_type, issuer, issue_date, coupon, flags, sub_book, corporate_action, leg_1_symbol, leg_2_symbol, contract_multiplier, settlement_method, instrument_sub_category) VALUES (@s, @length, @message_type, @nanosecond, @instrument_id, @reserved, @reserved1, @symbol_status, @isin, @symbol, @tidm, @segment, @previous_close_price, @expiration_date, @underlying, @strike_price, @option_type, @issuer, @issue_date, @coupon, @flags, @sub_book, @corporate_action, @leg_1_symbol, @leg_2_symbol, @contract_multiplier, @settlement_method, @instrument_sub_category) ;", conn))
            {
                cmd.Parameters.AddWithValue("s", msgOrderSeqId);

                cmd.Parameters.AddWithValue("length", (int) msgObj.Length);
                cmd.Parameters.AddWithValue("message_type", (string) msgObj.Message_Type);
                cmd.Parameters.AddWithValue("nanosecond", (long) msgObj.Nanosecond);
                cmd.Parameters.AddWithValue("instrument_id", (long) msgObj.Instrument_ID);
                cmd.Parameters.AddWithValue("reserved", (string) msgObj.Reserved);
                cmd.Parameters.AddWithValue("reserved1", (string) msgObj.Reserved1);
                cmd.Parameters.AddWithValue("symbol_status", (string) msgObj.Symbol_Status);
                cmd.Parameters.AddWithValue("isin", (string) msgObj.ISIN);
                cmd.Parameters.AddWithValue("symbol", (string) msgObj.Symbol);
                cmd.Parameters.AddWithValue("tidm", (string) msgObj.TIDM);
                cmd.Parameters.AddWithValue("segment", (string) msgObj.Segment);
                cmd.Parameters.AddWithValue("previous_close_price", (long) msgObj.Previous_Close_Price);
                cmd.Parameters.AddWithValue("expiration_date", (string) msgObj.Expiration_Date);
                cmd.Parameters.AddWithValue("underlying", (string) msgObj.Underlying);
                cmd.Parameters.AddWithValue("strike_price", (long) msgObj.Strike_Price);
                cmd.Parameters.AddWithValue("option_type", (string) msgObj.Option_Type);
                cmd.Parameters.AddWithValue("issuer", (string) msgObj.Issuer);
                cmd.Parameters.AddWithValue("issue_date", (string) msgObj.Issue_Date);
                cmd.Parameters.AddWithValue("coupon", (long) msgObj.Coupon);
                cmd.Parameters.AddWithValue("flags", (byte) msgObj.Flags);
                cmd.Parameters.AddWithValue("sub_book", (byte) msgObj.Sub_Book);
                cmd.Parameters.AddWithValue("corporate_action", (string) msgObj.Corporate_Action);
                cmd.Parameters.AddWithValue("leg_1_symbol", (string) msgObj.Leg_1_Symbol);
                cmd.Parameters.AddWithValue("leg_2_symbol", (string) msgObj.Leg_2_Symbol);
                cmd.Parameters.AddWithValue("contract_multiplier", (long) msgObj.Contract_Multiplier);
                cmd.Parameters.AddWithValue("settlement_method", (string) msgObj.Settlement_Method);
                cmd.Parameters.AddWithValue("instrument_sub_category", (string) msgObj.Instrument_Sub_Category);

                cmd.ExecuteNonQuery();
            }
        }

        public void Insert_Symbol_Status(Symbol_StatusMessage msgObj, long msgOrderSeqId)
        {
            using( var cmd = new NpgsqlCommand("INSERT INTO Symbol_Status (seq_id, length, message_type, nanosecond, instrument_id, reserved, reserved1, trading_status, flags, reason, session_change_reason, new_end_time, book_type) VALUES (@s, @length, @message_type, @nanosecond, @instrument_id, @reserved, @reserved1, @trading_status, @flags, @reason, @session_change_reason, @new_end_time, @book_type) ;", conn))
            {
                cmd.Parameters.AddWithValue("s", msgOrderSeqId);

                cmd.Parameters.AddWithValue("length", (int) msgObj.Length);
                cmd.Parameters.AddWithValue("message_type", (string) msgObj.Message_Type);
                cmd.Parameters.AddWithValue("nanosecond", (long) msgObj.Nanosecond);
                cmd.Parameters.AddWithValue("instrument_id", (long) msgObj.Instrument_ID);
                cmd.Parameters.AddWithValue("reserved", (string) msgObj.Reserved);
                cmd.Parameters.AddWithValue("reserved1", (string) msgObj.Reserved1);
                cmd.Parameters.AddWithValue("trading_status", (string) msgObj.Trading_Status);
                cmd.Parameters.AddWithValue("flags", (byte) msgObj.Flags);
                cmd.Parameters.AddWithValue("reason", (string) msgObj.Reason);
                cmd.Parameters.AddWithValue("session_change_reason", (int) msgObj.Session_Change_Reason);
                cmd.Parameters.AddWithValue("new_end_time", (string) msgObj.New_End_Time);
                cmd.Parameters.AddWithValue("book_type", (int) msgObj.Book_Type);

                cmd.ExecuteNonQuery();
            }
        }

        public void Insert_Add_Order(Add_OrderMessage msgObj, long msgOrderSeqId)
        {
            using( var cmd = new NpgsqlCommand("INSERT INTO Add_Order (seq_id, length, message_type, nanosecond, order_id, side, quantity, instrument_id, reserved, reserved1, price, flags) VALUES (@s, @length, @message_type, @nanosecond, @order_id, @side, @quantity, @instrument_id, @reserved, @reserved1, @price, @flags) ;", conn))
            {
                cmd.Parameters.AddWithValue("s", msgOrderSeqId);

                cmd.Parameters.AddWithValue("length", (int) msgObj.Length);
                cmd.Parameters.AddWithValue("message_type", (string) msgObj.Message_Type);
                cmd.Parameters.AddWithValue("nanosecond", (long) msgObj.Nanosecond);
                cmd.Parameters.AddWithValue("order_id", (string) msgObj.Order_ID);
                cmd.Parameters.AddWithValue("side", (string) msgObj.Side);
                cmd.Parameters.AddWithValue("quantity", (long) msgObj.Quantity);
                cmd.Parameters.AddWithValue("instrument_id", (long) msgObj.Instrument_ID);
                cmd.Parameters.AddWithValue("reserved", (string) msgObj.Reserved);
                cmd.Parameters.AddWithValue("reserved1", (string) msgObj.Reserved1);
                cmd.Parameters.AddWithValue("price", (long) msgObj.Price);
                cmd.Parameters.AddWithValue("flags", (byte) msgObj.Flags);

                cmd.ExecuteNonQuery();
            }
        }

        public void Insert_Add_Attributed_Order(Add_Attributed_OrderMessage msgObj, long msgOrderSeqId)
        {
            using( var cmd = new NpgsqlCommand("INSERT INTO Add_Attributed_Order (seq_id, length, message_type, nanosecond, order_id, side, quantity, instrument_id, price, attribution, flags) VALUES (@s, @length, @message_type, @nanosecond, @order_id, @side, @quantity, @instrument_id, @price, @attribution, @flags) ;", conn))
            {
                cmd.Parameters.AddWithValue("s", msgOrderSeqId);

                cmd.Parameters.AddWithValue("length", (int) msgObj.Length);
                cmd.Parameters.AddWithValue("message_type", (string) msgObj.Message_Type);
                cmd.Parameters.AddWithValue("nanosecond", (long) msgObj.Nanosecond);
                cmd.Parameters.AddWithValue("order_id", (string) msgObj.Order_ID);
                cmd.Parameters.AddWithValue("side", (string) msgObj.Side);
                cmd.Parameters.AddWithValue("quantity", (long) msgObj.Quantity);
                cmd.Parameters.AddWithValue("instrument_id", (long) msgObj.Instrument_ID);
                cmd.Parameters.AddWithValue("price", (long) msgObj.Price);
                cmd.Parameters.AddWithValue("attribution", (string) msgObj.Attribution);
                cmd.Parameters.AddWithValue("flags", (byte) msgObj.Flags);

                cmd.ExecuteNonQuery();
            }
        }

        public void Insert_Order_Deleted(Order_DeletedMessage msgObj, long msgOrderSeqId)
        {
            using( var cmd = new NpgsqlCommand("INSERT INTO Order_Deleted (seq_id, length, message_type, nanosecond, order_id) VALUES (@s, @length, @message_type, @nanosecond, @order_id) ;", conn))
            {
                cmd.Parameters.AddWithValue("s", msgOrderSeqId);

                cmd.Parameters.AddWithValue("length", (int) msgObj.Length);
                cmd.Parameters.AddWithValue("message_type", (string) msgObj.Message_Type);
                cmd.Parameters.AddWithValue("nanosecond", (long) msgObj.Nanosecond);
                cmd.Parameters.AddWithValue("order_id", (string) msgObj.Order_ID);

                cmd.ExecuteNonQuery();
            }
        }

        public void Insert_Order_Modified(Order_ModifiedMessage msgObj, long msgOrderSeqId)
        {
            using( var cmd = new NpgsqlCommand("INSERT INTO Order_Modified (seq_id, length, message_type, nanosecond, order_id, new_quantity, new_price, flags) VALUES (@s, @length, @message_type, @nanosecond, @order_id, @new_quantity, @new_price, @flags) ;", conn))
            {
                cmd.Parameters.AddWithValue("s", msgOrderSeqId);

                cmd.Parameters.AddWithValue("length", (int) msgObj.Length);
                cmd.Parameters.AddWithValue("message_type", (string) msgObj.Message_Type);
                cmd.Parameters.AddWithValue("nanosecond", (long) msgObj.Nanosecond);
                cmd.Parameters.AddWithValue("order_id", (string) msgObj.Order_ID);
                cmd.Parameters.AddWithValue("new_quantity", (long) msgObj.New_Quantity);
                cmd.Parameters.AddWithValue("new_price", (long) msgObj.New_Price);
                cmd.Parameters.AddWithValue("flags", (byte) msgObj.Flags);

                cmd.ExecuteNonQuery();
            }
        }

        public void Insert_Order_Book_Clear(Order_Book_ClearMessage msgObj, long msgOrderSeqId)
        {
            using( var cmd = new NpgsqlCommand("INSERT INTO Order_Book_Clear (seq_id, length, message_type, nanosecond, instrument_id, sub_book, book_type) VALUES (@s, @length, @message_type, @nanosecond, @instrument_id, @sub_book, @book_type) ;", conn))
            {
                cmd.Parameters.AddWithValue("s", msgOrderSeqId);

                cmd.Parameters.AddWithValue("length", (int) msgObj.Length);
                cmd.Parameters.AddWithValue("message_type", (string) msgObj.Message_Type);
                cmd.Parameters.AddWithValue("nanosecond", (long) msgObj.Nanosecond);
                cmd.Parameters.AddWithValue("instrument_id", (long) msgObj.Instrument_ID);
                cmd.Parameters.AddWithValue("sub_book", (int) msgObj.Sub_Book);
                cmd.Parameters.AddWithValue("book_type", (string) msgObj.Book_Type);

                cmd.ExecuteNonQuery();
            }
        }

        public void Insert_Order_Executed(Order_ExecutedMessage msgObj, long msgOrderSeqId)
        {
            using( var cmd = new NpgsqlCommand("INSERT INTO Order_Executed (seq_id, length, message_type, nanosecond, order_id, executed_quantity, trade_id, lastoptpx, volatility, underlying_reference_price) VALUES (@s, @length, @message_type, @nanosecond, @order_id, @executed_quantity, @trade_id, @lastoptpx, @volatility, @underlying_reference_price) ;", conn))
            {
                cmd.Parameters.AddWithValue("s", msgOrderSeqId);

                cmd.Parameters.AddWithValue("length", (int) msgObj.Length);
                cmd.Parameters.AddWithValue("message_type", (string) msgObj.Message_Type);
                cmd.Parameters.AddWithValue("nanosecond", (long) msgObj.Nanosecond);
                cmd.Parameters.AddWithValue("order_id", (string) msgObj.Order_ID);
                cmd.Parameters.AddWithValue("executed_quantity", (long) msgObj.Executed_Quantity);
                cmd.Parameters.AddWithValue("trade_id", (string) msgObj.Trade_ID);
                cmd.Parameters.AddWithValue("lastoptpx", (long) msgObj.LastOptPx);
                cmd.Parameters.AddWithValue("volatility", (long) msgObj.Volatility);
                cmd.Parameters.AddWithValue("underlying_reference_price", (long) msgObj.Underlying_Reference_Price);

                cmd.ExecuteNonQuery();
            }
        }

        public void Insert_Order_Executed_With_PriceORSize(Order_Executed_With_PriceORSizeMessage msgObj, long msgOrderSeqId)
        {
            using( var cmd = new NpgsqlCommand("INSERT INTO Order_Executed_With_PriceORSize (seq_id, length, message_type, nanosecond, order_id, executed_quantity, display_quantity, trade_id, printable, price, lastoptpx, volatility, underlying_reference_price) VALUES (@s, @length, @message_type, @nanosecond, @order_id, @executed_quantity, @display_quantity, @trade_id, @printable, @price, @lastoptpx, @volatility, @underlying_reference_price) ;", conn))
            {
                cmd.Parameters.AddWithValue("s", msgOrderSeqId);

                cmd.Parameters.AddWithValue("length", (int) msgObj.Length);
                cmd.Parameters.AddWithValue("message_type", (string) msgObj.Message_Type);
                cmd.Parameters.AddWithValue("nanosecond", (long) msgObj.Nanosecond);
                cmd.Parameters.AddWithValue("order_id", (string) msgObj.Order_ID);
                cmd.Parameters.AddWithValue("executed_quantity", (long) msgObj.Executed_Quantity);
                cmd.Parameters.AddWithValue("display_quantity", (long) msgObj.Display_Quantity);
                cmd.Parameters.AddWithValue("trade_id", (string) msgObj.Trade_ID);
                cmd.Parameters.AddWithValue("printable", (string) msgObj.Printable);
                cmd.Parameters.AddWithValue("price", (long) msgObj.Price);
                cmd.Parameters.AddWithValue("lastoptpx", (long) msgObj.LastOptPx);
                cmd.Parameters.AddWithValue("volatility", (long) msgObj.Volatility);
                cmd.Parameters.AddWithValue("underlying_reference_price", (long) msgObj.Underlying_Reference_Price);

                cmd.ExecuteNonQuery();
            }
        }

        public void Insert_Trade(TradeMessage msgObj, long msgOrderSeqId)
        {
            using( var cmd = new NpgsqlCommand("INSERT INTO Trade (seq_id, length, message_type, nanosecond, executed_quantity, instrument_id, reserved, reserved1, price, trade_id, sub_book, flags, tradesub_type, lastoptpx, volatility, underlying_reference_price) VALUES (@s, @length, @message_type, @nanosecond, @executed_quantity, @instrument_id, @reserved, @reserved1, @price, @trade_id, @sub_book, @flags, @tradesub_type, @lastoptpx, @volatility, @underlying_reference_price) ;", conn))
            {
                cmd.Parameters.AddWithValue("s", msgOrderSeqId);

                cmd.Parameters.AddWithValue("length", (int) msgObj.Length);
                cmd.Parameters.AddWithValue("message_type", (string) msgObj.Message_Type);
                cmd.Parameters.AddWithValue("nanosecond", (long) msgObj.Nanosecond);
                cmd.Parameters.AddWithValue("executed_quantity", (long) msgObj.Executed_Quantity);
                cmd.Parameters.AddWithValue("instrument_id", (long) msgObj.Instrument_ID);
                cmd.Parameters.AddWithValue("reserved", (string) msgObj.Reserved);
                cmd.Parameters.AddWithValue("reserved1", (string) msgObj.Reserved1);
                cmd.Parameters.AddWithValue("price", (long) msgObj.Price);
                cmd.Parameters.AddWithValue("trade_id", (string) msgObj.Trade_ID);
                cmd.Parameters.AddWithValue("sub_book", (int) msgObj.Sub_Book);
                cmd.Parameters.AddWithValue("flags", (byte) msgObj.Flags);
                cmd.Parameters.AddWithValue("tradesub_type", (string) msgObj.TradeSub_Type);
                cmd.Parameters.AddWithValue("lastoptpx", (long) msgObj.LastOptPx);
                cmd.Parameters.AddWithValue("volatility", (long) msgObj.Volatility);
                cmd.Parameters.AddWithValue("underlying_reference_price", (long) msgObj.Underlying_Reference_Price);

                cmd.ExecuteNonQuery();
            }
        }

        public void Insert_Auction_Trade(Auction_TradeMessage msgObj, long msgOrderSeqId)
        {
            using( var cmd = new NpgsqlCommand("INSERT INTO Auction_Trade (seq_id, length, message_type, nanosecond, quantity, instrument_id, reserved, reserved1, price, trade_id, auction_type, lastoptpx, volatility, underlying_reference_price) VALUES (@s, @length, @message_type, @nanosecond, @quantity, @instrument_id, @reserved, @reserved1, @price, @trade_id, @auction_type, @lastoptpx, @volatility, @underlying_reference_price) ;", conn))
            {
                cmd.Parameters.AddWithValue("s", msgOrderSeqId);

                cmd.Parameters.AddWithValue("length", (int) msgObj.Length);
                cmd.Parameters.AddWithValue("message_type", (string) msgObj.Message_Type);
                cmd.Parameters.AddWithValue("nanosecond", (long) msgObj.Nanosecond);
                cmd.Parameters.AddWithValue("quantity", (long) msgObj.Quantity);
                cmd.Parameters.AddWithValue("instrument_id", (long) msgObj.Instrument_ID);
                cmd.Parameters.AddWithValue("reserved", (string) msgObj.Reserved);
                cmd.Parameters.AddWithValue("reserved1", (string) msgObj.Reserved1);
                cmd.Parameters.AddWithValue("price", (long) msgObj.Price);
                cmd.Parameters.AddWithValue("trade_id", (string) msgObj.Trade_ID);
                cmd.Parameters.AddWithValue("auction_type", (string) msgObj.Auction_Type);
                cmd.Parameters.AddWithValue("lastoptpx", (long) msgObj.LastOptPx);
                cmd.Parameters.AddWithValue("volatility", (long) msgObj.Volatility);
                cmd.Parameters.AddWithValue("underlying_reference_price", (long) msgObj.Underlying_Reference_Price);

                cmd.ExecuteNonQuery();
            }
        }

        public void Insert_Off_Book_Trade(Off_Book_TradeMessage msgObj, long msgOrderSeqId)
        {
            using( var cmd = new NpgsqlCommand("INSERT INTO Off_Book_Trade (seq_id, length, message_type, nanosecond, executed_quantity, instrument_id, reserved, reserved1, price, trade_id, off_book_trade_type, trade_time, trade_date, lastoptpx, volatility, underlying_reference_price) VALUES (@s, @length, @message_type, @nanosecond, @executed_quantity, @instrument_id, @reserved, @reserved1, @price, @trade_id, @off_book_trade_type, @trade_time, @trade_date, @lastoptpx, @volatility, @underlying_reference_price) ;", conn))
            {
                cmd.Parameters.AddWithValue("s", msgOrderSeqId);

                cmd.Parameters.AddWithValue("length", (int) msgObj.Length);
                cmd.Parameters.AddWithValue("message_type", (string) msgObj.Message_Type);
                cmd.Parameters.AddWithValue("nanosecond", (long) msgObj.Nanosecond);
                cmd.Parameters.AddWithValue("executed_quantity", (long) msgObj.Executed_Quantity);
                cmd.Parameters.AddWithValue("instrument_id", (long) msgObj.Instrument_ID);
                cmd.Parameters.AddWithValue("reserved", (string) msgObj.Reserved);
                cmd.Parameters.AddWithValue("reserved1", (string) msgObj.Reserved1);
                cmd.Parameters.AddWithValue("price", (long) msgObj.Price);
                cmd.Parameters.AddWithValue("trade_id", (string) msgObj.Trade_ID);
                cmd.Parameters.AddWithValue("off_book_trade_type", (string) msgObj.Off_Book_Trade_Type);
                cmd.Parameters.AddWithValue("trade_time", (string) msgObj.Trade_Time);
                cmd.Parameters.AddWithValue("trade_date", (string) msgObj.Trade_Date);
                cmd.Parameters.AddWithValue("lastoptpx", (long) msgObj.LastOptPx);
                cmd.Parameters.AddWithValue("volatility", (long) msgObj.Volatility);
                cmd.Parameters.AddWithValue("underlying_reference_price", (long) msgObj.Underlying_Reference_Price);

                cmd.ExecuteNonQuery();
            }
        }

        public void Insert_Trade_Break(Trade_BreakMessage msgObj, long msgOrderSeqId)
        {
            using( var cmd = new NpgsqlCommand("INSERT INTO Trade_Break (seq_id, length, message_type, nanosecond, trade_id, trade_type) VALUES (@s, @length, @message_type, @nanosecond, @trade_id, @trade_type) ;", conn))
            {
                cmd.Parameters.AddWithValue("s", msgOrderSeqId);

                cmd.Parameters.AddWithValue("length", (int) msgObj.Length);
                cmd.Parameters.AddWithValue("message_type", (string) msgObj.Message_Type);
                cmd.Parameters.AddWithValue("nanosecond", (long) msgObj.Nanosecond);
                cmd.Parameters.AddWithValue("trade_id", (string) msgObj.Trade_ID);
                cmd.Parameters.AddWithValue("trade_type", (string) msgObj.Trade_Type);

                cmd.ExecuteNonQuery();
            }
        }

        public void Insert_Recovery_Trade(Recovery_TradeMessage msgObj, long msgOrderSeqId)
        {
            using( var cmd = new NpgsqlCommand("INSERT INTO Recovery_Trade (seq_id, length, message_type, nanosecond, executed_quantity, instrument_id, reserved, reserved1, price, trade_id, auction_type, off_bookorrfq_trade_type, trade_time, trade_date, action_type, sub_book, flags, lastoptpx, volatility, underlying_reference_price) VALUES (@s, @length, @message_type, @nanosecond, @executed_quantity, @instrument_id, @reserved, @reserved1, @price, @trade_id, @auction_type, @off_bookorrfq_trade_type, @trade_time, @trade_date, @action_type, @sub_book, @flags, @lastoptpx, @volatility, @underlying_reference_price) ;", conn))
            {
                cmd.Parameters.AddWithValue("s", msgOrderSeqId);

                cmd.Parameters.AddWithValue("length", (int) msgObj.Length);
                cmd.Parameters.AddWithValue("message_type", (string) msgObj.Message_Type);
                cmd.Parameters.AddWithValue("nanosecond", (long) msgObj.Nanosecond);
                cmd.Parameters.AddWithValue("executed_quantity", (long) msgObj.Executed_Quantity);
                cmd.Parameters.AddWithValue("instrument_id", (long) msgObj.Instrument_ID);
                cmd.Parameters.AddWithValue("reserved", (string) msgObj.Reserved);
                cmd.Parameters.AddWithValue("reserved1", (string) msgObj.Reserved1);
                cmd.Parameters.AddWithValue("price", (long) msgObj.Price);
                cmd.Parameters.AddWithValue("trade_id", (string) msgObj.Trade_ID);
                cmd.Parameters.AddWithValue("auction_type", (string) msgObj.Auction_Type);
                cmd.Parameters.AddWithValue("off_bookorrfq_trade_type", (string) msgObj.Off_BookORRFQ_Trade_Type);
                cmd.Parameters.AddWithValue("trade_time", (string) msgObj.Trade_Time);
                cmd.Parameters.AddWithValue("trade_date", (string) msgObj.Trade_Date);
                cmd.Parameters.AddWithValue("action_type", (string) msgObj.Action_Type);
                cmd.Parameters.AddWithValue("sub_book", (int) msgObj.Sub_Book);
                cmd.Parameters.AddWithValue("flags", (byte) msgObj.Flags);
                cmd.Parameters.AddWithValue("lastoptpx", (long) msgObj.LastOptPx);
                cmd.Parameters.AddWithValue("volatility", (long) msgObj.Volatility);
                cmd.Parameters.AddWithValue("underlying_reference_price", (long) msgObj.Underlying_Reference_Price);

                cmd.ExecuteNonQuery();
            }
        }

        public void Insert_Auction_Info(Auction_InfoMessage msgObj, long msgOrderSeqId)
        {
            using( var cmd = new NpgsqlCommand("INSERT INTO Auction_Info (seq_id, length, message_type, nanosecond, paired_quantity, reserved, imbalance_direction, instrument_id, reserved1, reserved2, price, auction_type) VALUES (@s, @length, @message_type, @nanosecond, @paired_quantity, @reserved, @imbalance_direction, @instrument_id, @reserved1, @reserved2, @price, @auction_type) ;", conn))
            {
                cmd.Parameters.AddWithValue("s", msgOrderSeqId);

                cmd.Parameters.AddWithValue("length", (int) msgObj.Length);
                cmd.Parameters.AddWithValue("message_type", (string) msgObj.Message_Type);
                cmd.Parameters.AddWithValue("nanosecond", (long) msgObj.Nanosecond);
                cmd.Parameters.AddWithValue("paired_quantity", (long) msgObj.Paired_Quantity);
                cmd.Parameters.AddWithValue("reserved", (long) msgObj.Reserved);
                cmd.Parameters.AddWithValue("imbalance_direction", (string) msgObj.Imbalance_Direction);
                cmd.Parameters.AddWithValue("instrument_id", (long) msgObj.Instrument_ID);
                cmd.Parameters.AddWithValue("reserved1", (string) msgObj.Reserved1);
                cmd.Parameters.AddWithValue("reserved2", (string) msgObj.Reserved2);
                cmd.Parameters.AddWithValue("price", (long) msgObj.Price);
                cmd.Parameters.AddWithValue("auction_type", (string) msgObj.Auction_Type);

                cmd.ExecuteNonQuery();
            }
        }

        public void Insert_Statistics(StatisticsMessage msgObj, long msgOrderSeqId)
        {
            using( var cmd = new NpgsqlCommand("INSERT INTO Statistics (seq_id, length, message_type, nanosecond, instrument_id, reserved, reserved1, statistic_type, price, openclose_indicator, sub_book) VALUES (@s, @length, @message_type, @nanosecond, @instrument_id, @reserved, @reserved1, @statistic_type, @price, @openclose_indicator, @sub_book) ;", conn))
            {
                cmd.Parameters.AddWithValue("s", msgOrderSeqId);

                cmd.Parameters.AddWithValue("length", (int) msgObj.Length);
                cmd.Parameters.AddWithValue("message_type", (string) msgObj.Message_Type);
                cmd.Parameters.AddWithValue("nanosecond", (long) msgObj.Nanosecond);
                cmd.Parameters.AddWithValue("instrument_id", (long) msgObj.Instrument_ID);
                cmd.Parameters.AddWithValue("reserved", (string) msgObj.Reserved);
                cmd.Parameters.AddWithValue("reserved1", (string) msgObj.Reserved1);
                cmd.Parameters.AddWithValue("statistic_type", (string) msgObj.Statistic_Type);
                cmd.Parameters.AddWithValue("price", (long) msgObj.Price);
                cmd.Parameters.AddWithValue("openclose_indicator", (string) msgObj.OpenClose_Indicator);
                cmd.Parameters.AddWithValue("sub_book", (int) msgObj.Sub_Book);

                cmd.ExecuteNonQuery();
            }
        }

        public void Insert_Extended_Statistics(Extended_StatisticsMessage msgObj, long msgOrderSeqId)
        {
            using( var cmd = new NpgsqlCommand("INSERT INTO Extended_Statistics (seq_id, length, message_type, nanosecond, instrument_id, high_price, low_price, vwap, volume, turnover, number_of_trades, reserved_field, subbook, notional_exposure, notional_delta_exposure, open_interest) VALUES (@s, @length, @message_type, @nanosecond, @instrument_id, @high_price, @low_price, @vwap, @volume, @turnover, @number_of_trades, @reserved_field, @subbook, @notional_exposure, @notional_delta_exposure, @open_interest) ;", conn))
            {
                cmd.Parameters.AddWithValue("s", msgOrderSeqId);

                cmd.Parameters.AddWithValue("length", (int) msgObj.Length);
                cmd.Parameters.AddWithValue("message_type", (string) msgObj.Message_Type);
                cmd.Parameters.AddWithValue("nanosecond", (long) msgObj.Nanosecond);
                cmd.Parameters.AddWithValue("instrument_id", (long) msgObj.Instrument_ID);
                cmd.Parameters.AddWithValue("high_price", (long) msgObj.High_Price);
                cmd.Parameters.AddWithValue("low_price", (long) msgObj.Low_Price);
                cmd.Parameters.AddWithValue("vwap", (long) msgObj.VWAP);
                cmd.Parameters.AddWithValue("volume", (long) msgObj.Volume);
                cmd.Parameters.AddWithValue("turnover", (long) msgObj.Turnover);
                cmd.Parameters.AddWithValue("number_of_trades", (long) msgObj.Number_of_Trades);
                cmd.Parameters.AddWithValue("reserved_field", (string) msgObj.Reserved_Field);
                cmd.Parameters.AddWithValue("subbook", (int) msgObj.SubBook);
                cmd.Parameters.AddWithValue("notional_exposure", (long) msgObj.Notional_Exposure);
                cmd.Parameters.AddWithValue("notional_delta_exposure", (long) msgObj.Notional_Delta_Exposure);
                cmd.Parameters.AddWithValue("open_interest", (long) msgObj.Open_Interest);

                cmd.ExecuteNonQuery();
            }
        }

        public void Insert_News(NewsMessage msgObj, long msgOrderSeqId)
        {
            using( var cmd = new NpgsqlCommand("INSERT INTO News (seq_id, length, message_type, nanosecond, time, urgency, headline, text, instruments, underlyings) VALUES (@s, @length, @message_type, @nanosecond, @time, @urgency, @headline, @text, @instruments, @underlyings) ;", conn))
            {
                cmd.Parameters.AddWithValue("s", msgOrderSeqId);

                cmd.Parameters.AddWithValue("length", (int) msgObj.Length);
                cmd.Parameters.AddWithValue("message_type", (string) msgObj.Message_Type);
                cmd.Parameters.AddWithValue("nanosecond", (long) msgObj.Nanosecond);
                cmd.Parameters.AddWithValue("time", (string) msgObj.Time);
                cmd.Parameters.AddWithValue("urgency", (string) msgObj.Urgency);
                cmd.Parameters.AddWithValue("headline", (string) msgObj.Headline);
                cmd.Parameters.AddWithValue("text", (string) msgObj.Text);
                cmd.Parameters.AddWithValue("instruments", (string) msgObj.Instruments);
                cmd.Parameters.AddWithValue("underlyings", (string) msgObj.Underlyings);

                cmd.ExecuteNonQuery();
            }
        }

        public void Insert_Top_of_Book(Top_of_BookMessage msgObj, long msgOrderSeqId)
        {
            using( var cmd = new NpgsqlCommand("INSERT INTO Top_of_Book (seq_id, length, message_type, nanosecond, instrument_id, reserve_field, sub_book, action, side, price, quantity, market_order_quantity, reserved_field) VALUES (@s, @length, @message_type, @nanosecond, @instrument_id, @reserve_field, @sub_book, @action, @side, @price, @quantity, @market_order_quantity, @reserved_field) ;", conn))
            {
                cmd.Parameters.AddWithValue("s", msgOrderSeqId);

                cmd.Parameters.AddWithValue("length", (int) msgObj.Length);
                cmd.Parameters.AddWithValue("message_type", (string) msgObj.Message_Type);
                cmd.Parameters.AddWithValue("nanosecond", (long) msgObj.Nanosecond);
                cmd.Parameters.AddWithValue("instrument_id", (long) msgObj.Instrument_ID);
                cmd.Parameters.AddWithValue("reserve_field", (int) msgObj.Reserve_Field);
                cmd.Parameters.AddWithValue("sub_book", (byte) msgObj.Sub_Book);
                cmd.Parameters.AddWithValue("action", (string) msgObj.Action);
                cmd.Parameters.AddWithValue("side", (string) msgObj.Side);
                cmd.Parameters.AddWithValue("price", (long) msgObj.Price);
                cmd.Parameters.AddWithValue("quantity", (long) msgObj.Quantity);
                cmd.Parameters.AddWithValue("market_order_quantity", (long) msgObj.Market_Order_Quantity);
                cmd.Parameters.AddWithValue("reserved_field", (int) msgObj.Reserved_Field);

                cmd.ExecuteNonQuery();
            }
        }

    }
}
