import psycopg2



class Conn:
    def __init__(self):

        # first try to check if we already have a connection object
        
        self.connection_obj = psycopg2.connect(user="admin",
            password="quest",
            host="questdb",
            port="8812",
            database="qdb"
        )

        self.connection = None

        if self.connection_obj.closed:
            print("Connection object closed, trying to open new")
            self.connection = self.init_connection()

        else:
            print("Connection already opened")
            self.connection = self.connection_obj

        print("Init conn")



    def init_connection(self):
        print("Init connectino do qdb")
        try:

            connection_obj = psycopg2.connect(user="admin",
                password="quest",
                host="questdb",
                port="8812",
                database="qdb"
            )


            cursor = connection_obj.cursor()
            # Print PostgreSQL Connection properties
            print(connection_obj.get_dsn_parameters(), "\n")

            # Print PostgreSQL version
            cursor.execute("SELECT version();")
            record = cursor.fetchone()
            print("You are connected to - ", record, "\n")
            # cursor.close()

            return connection_obj

        except (Exception, psycopg2.Error) as error:
            print("Error while connecting to PostgreSQL", error)
            return None



    def get_symbol_directory(self):
        ls_search_data = []
        try:

            cursor = self.connection.cursor()
            query = "SELECT * FROM Symbol_Directory;"
            cursor.execute(query)
            records = cursor.fetchall()
            # d_sym = {}

            print("Print each row and it's columns values")
            for row in records:
                # print("InsID = ", row[4], " ISIN = ", row[8], " Symbol = ", row[9], "\n")
                d_sym = str(row[9]).strip()
                d_val = row[4]
                val_val = f"{str(d_sym)}::{d_val}"
                d = {
                    "value": val_val,
                    "label":d_sym
                }
                ls_search_data.append(d)

            # cursor.close()
            return ls_search_data


        except (Exception, psycopg2.Error) as error:
            print("Error while connecting to PostgreSQL", error)
            return ls_search_data




    def get_time(self):
        print("Getting time messages")
        ls_data = []
        msg_type = "Time"

        try:
            colnames = self.get_colnames_for_msg(msg_type)


            cursor = self.connection.cursor()


            # query = f"SELECT * FROM Time;"
            # used for testing
            query = f"SELECT * FROM Time LIMIT 5000;"

            cursor.execute(query)
            records = cursor.fetchall()
            # d_sym = {}

            ls_data = self.build_record_list(records, colnames)



            return ls_data


        except (Exception, psycopg2.Error) as error:
            print("Error while connecting to PostgreSQL", error)
            return ls_data





    def get_colnames_for_msg(self, msg_type):
        # return back complete list of col names for table
        try:
            cursor = self.connection.cursor()
            base_q = f"Select * FROM {msg_type} LIMIT 0"
            # first get the list of expected row names for this table
            cursor.execute(base_q)
            colnames = [desc[0] for desc in cursor.description]

            return colnames

        except (Exception, psycopg2.Error) as error:
            print("Error while connecting to PostgreSQL", error)
            return []


    def build_record_list(self, records, colnames):
        # return structured list of objects
        # records is the list of records returned from the db for an executed query
        ls_data = []

        for row in records:
            # init holder dict
            d = {}
            for i in range(len(colnames)):
                key_name = colnames[i]
                d[key_name] = row[i]

            # append key-val dict to list
            ls_data.append(d)

        return ls_data





    def get_primary_msgs_for_insid(self, msg_type, insid):
        print("Getting primary message for msg_type ", msg_type)
        print("insid", insid)
        # get symbold and jse id
        sp = str(insid).split("::")
        sym = sp[0]

        jse_id = sp[-1]
        ls_data = []

        try:

            colnames = self.get_colnames_for_msg(msg_type)

            # print("Col names", colnames)

            cursor = self.connection.cursor()

            # query = f"SELECT * FROM {msg_type} WHERE instrument_id=%(jse_insid)s;"
            # testing
            query = f"SELECT * FROM {msg_type} WHERE instrument_id=%(jse_insid)s LIMIT 5000;"

            param = {"jse_insid":int(jse_id)}
            cursor.execute(query, param)
            records = cursor.fetchall()

            ls_data = self.build_record_list(records, colnames)


            return ls_data


        except (Exception, psycopg2.Error) as error:
            print("Error while connecting to PostgreSQL", error)
            return ls_data



    def get_secondary_msgs_for_insid(self, msg_type, insid):
        print("Getting secondary message for msg_type ", msg_type)
        print("insid", insid)
        # get symbold and jse id
        sp = str(insid).split("::")
        sym = sp[0]

        jse_id = sp[-1]
        ls_data = []

        try:
            cursor = self.connection.cursor()

            # print("Col names", colnamesog)
            # query0 = f"SELECT order_id FROM Add_Order WHERE instrument_id=%(jse_insid)s;"
            # Testing
            query0 = f"SELECT order_id FROM Add_Order WHERE instrument_id=%(jse_insid)s LIMIT 5000;"

            param = {"jse_insid":int(jse_id)}
            cursor.execute(query0, param)
            records0 = cursor.fetchall()
            # d_sym = {}
            ls_order_ids = []


            for row in records0:
                # just get list of order_ids
                ls_order_ids.append(row[0])
                    

            # get uniqe list of order_ids
            ls_uids = list(set(ls_order_ids))

            colnames = self.get_colnames_for_msg(msg_type)

            # print("Col names", colnames)

            # build string of order_ids
            order_ls_str_ls = []
            for uid in ls_uids:
                order_ls_str_ls.append(f"'{uid}'")

            order_ls_str = " ,".join(order_ls_str_ls)



            # query = f"SELECT * FROM {msg_type} WHERE order_id IN ({order_ls_str});"
            # testing
            query = f"SELECT * FROM {msg_type} WHERE order_id IN ({order_ls_str}) LIMIT 5000;"
            # print(query)

            # param = {"jse_insid":int(jse_id)}
            cursor.execute(query)
            records = cursor.fetchall()

            ls_data = self.build_record_list(records, colnames)



            return ls_data


        except (Exception, psycopg2.Error) as error:
            print("Error while connecting to PostgreSQL", error)
            return ls_data






    def get_symbol_status_msgs(self, insid):
        print("Getting symbol status messages for ", insid)
        # get symbold and jse id
        sp = str(insid).split("::")
        sym = sp[0]

        jse_id = sp[-1]
        ls_data = []

        try:

            cursor = self.connection.cursor()

            # first get the list of expected row names for this table
            cursor.execute("Select * FROM Symbol_Status LIMIT 0")
            colnames = [desc[0] for desc in cursor.description]

            # print("Col names", colnames)
            query = "SELECT * FROM Symbol_Status WHERE instrument_id=%(jse_insid)s;"

            param = {"jse_insid":int(jse_id)}
            cursor.execute(query, param)
            records = cursor.fetchall()
            # d_sym = {}

            for row in records:
                # print("InsID = ", row[4], " ISIN = ", row[8], " Symbol = ", row[9], "\n")
                # print(row)
                # init holder dict
                d = {}
                for i in range(len(colnames)):
                    key_name = colnames[i]
                    d[key_name] = row[i]

                ls_data.append(d)



            # cursor.close()
            return ls_data


        except (Exception, psycopg2.Error) as error:
            print("Error while connecting to PostgreSQL", error)
            return ls_data


    def get_add_order_msgs(self, insid):
        print("Getting add order messages for ", insid)
        # get symbold and jse id
        sp = str(insid).split("::")
        sym = sp[0]

        jse_id = sp[-1]
        ls_data = []

        try:

            cursor = self.connection.cursor()

            # first get the list of expected row names for this table
            cursor.execute("Select * FROM Add_Order LIMIT 0")
            colnames = [desc[0] for desc in cursor.description]

            # print("Col names", colnames)
            query = "SELECT * FROM Add_Order WHERE instrument_id=%(jse_insid)s;"

            param = {"jse_insid":int(jse_id)}
            cursor.execute(query, param)
            records = cursor.fetchall()
            # d_sym = {}

            for row in records:
                # print("InsID = ", row[4], " ISIN = ", row[8], " Symbol = ", row[9], "\n")
                # print(row)
                # init holder dict
                d = {}
                for i in range(len(colnames)):
                    key_name = colnames[i]
                    d[key_name] = row[i]
                    
                ls_data.append(d)



            # cursor.close()
            return ls_data


        except (Exception, psycopg2.Error) as error:
            print("Error while connecting to PostgreSQL", error)
            return ls_data


    def get_messages_by_type_for_insid(self, msg_type, insid):
        print("Getting message for msg_type ", msg_type)
        print("insid", insid)
        # get symbold and jse id
        sp = str(insid).split("::")
        sym = sp[0]

        jse_id = sp[-1]
        ls_data = []

        try:

            cursor = self.connection.cursor()

            base_q = f"Select * FROM {msg_type} LIMIT 0"

            # first get the list of expected row names for this table
            cursor.execute(base_q)
            colnames = [desc[0] for desc in cursor.description]

            # print("Col names", colnames)

            query = f"SELECT * FROM {msg_type} WHERE instrument_id=%(jse_insid)s;"

            param = {"jse_insid":int(jse_id)}
            cursor.execute(query, param)
            records = cursor.fetchall()
            # d_sym = {}

            for row in records:
                # print("InsID = ", row[4], " ISIN = ", row[8], " Symbol = ", row[9], "\n")
                # print(row)
                # init holder dict
                d = {}
                for i in range(len(colnames)):
                    key_name = colnames[i]
                    d[key_name] = row[i]
                    
                ls_data.append(d)



            # cursor.close()
            return ls_data


        except (Exception, psycopg2.Error) as error:
            print("Error while connecting to PostgreSQL", error)
            return ls_data


    def get_add_order_msgs_between_seqids(self, insid, start, end):
        print("Getting add order messages for ", insid)
        # get symbold and jse id
        sp = str(insid).split("::")
        sym = sp[0]

        jse_id = sp[-1]
        ls_data = []

        ls_data2 = []

        try:

            cursor = self.connection.cursor()

            # first get the list of expected row names for this table
            cursor.execute("Select * FROM Add_Order LIMIT 0")
            colnames = [desc[0] for desc in cursor.description]

            # print("Col names", colnames)
            query = "SELECT * FROM Add_Order WHERE instrument_id=%(jse_insid)s AND seq_id >= %(seqid_start)s AND seq_id < %(seqid_end)s;"

            param = {
                "jse_insid":int(jse_id),
                "seqid_start":int(start),
                "seqid_end":int(end),
            }
            cursor.execute(query, param)
            records = cursor.fetchall()
            # d_sym = {}

            # HACK. TODO: FIX

            ls_order_ids = []


            for row in records:
                # print("InsID = ", row[4], " ISIN = ", row[8], " Symbol = ", row[9], "\n")
                # print(row)
                # init holder dict
                d = {}
                for i in range(len(colnames)):
                    key_name = colnames[i]
                    d[key_name] = row[i]
                    
                ls_data.append(d)
                ls_order_ids.append(d['order_id'])





            # cursor.close()
            return ls_data


        except (Exception, psycopg2.Error) as error:
            print("Error while connecting to PostgreSQL", error)
            return ls_data



    def get_exec_order_msgs_between_seqids(self, insid, start, end):
        print("Getting exec order messages for ", insid)
        # get symbold and jse id
        sp = str(insid).split("::")
        sym = sp[0]

        jse_id = sp[-1]
        ls_data = []

        ls_data2 = []

        try:

            cursor = self.connection.cursor()

            # first get the list of expected row names for this table
            cursor.execute("Select * FROM Add_Order LIMIT 0")
            colnames = [desc[0] for desc in cursor.description]

            # print("Col names", colnames)
            query = "SELECT *  FROM Add_Order WHERE instrument_id=%(jse_insid)s AND seq_id < %(seqid_end)s;"

            param = {
                "jse_insid":int(jse_id),
                "seqid_start":int(start),
                "seqid_end":int(end),
            }
            cursor.execute(query, param)
            records = cursor.fetchall()
            print("Got through first")
            # d_sym = {}

            # HACK. TODO: FIX

            ls_order_ids = []


            for row in records:
                # print("InsID = ", row[4], " ISIN = ", row[8], " Symbol = ", row[9], "\n")
                # print(row)
                # init holder dict
                d = {}
                for i in range(len(colnames)):
                    key_name = colnames[i]
                    d[key_name] = row[i]
                    
                ls_data.append(d)
                ls_order_ids.append(d['order_id'])

            ls_uids = list(set(ls_order_ids))

            base_q = f"Select * FROM Order_Executed LIMIT 0"

            # first get the list of expected row names for this table
            cursor.execute(base_q)
            colnames2 = [desc[0] for desc in cursor.description]

            # print("Col names", colnames)

            order_ls_str_ls = []
            for uid in ls_uids:
                order_ls_str_ls.append(f"'{uid}'")

            order_ls_str = " ,".join(order_ls_str_ls)



            query2 = f"SELECT * FROM Order_Executed WHERE seq_id < %(seqid_end)s AND order_id IN ({order_ls_str});"
            print(query2)

            cursor.execute(query2, param)
            records2 = cursor.fetchall()
            # d_sym = {}
            print("Got through second")


            for row in records2:
                # print("InsID = ", row[4], " ISIN = ", row[8], " Symbol = ", row[9], "\n")
                # print(row)
                # init holder dict
                d = {}
                for i in range(len(colnames2)):
                    key_name = colnames2[i]
                    d[key_name] = row[i]
                    
                ls_data2.append(d)

            print("Executed")
            print(ls_data2)





            # cursor.close()
            return ls_data2


        except (Exception, psycopg2.Error) as error:
            print("Error while connecting to PostgreSQL", error)
            return ls_data2




    def get_test(self, msg_type, insid):
        print("Getting test for msg_type ", msg_type)
        print("insid", insid)
        # get symbold and jse id
        sp = str(insid).split("::")
        sym = sp[0]

        jse_id = sp[-1]
        ls_data = []

        try:

            cursor = self.connection.cursor()

            # first get all add order messages
            # and track list of all order_ids

            cursor.execute("Select * FROM Add_Order LIMIT 0")
            colnamesog = [desc[0] for desc in cursor.description]

            # print("Col names", colnamesog)
            queryog = "SELECT * FROM Add_Order WHERE instrument_id=%(jse_insid)s;"

            param = {"jse_insid":int(jse_id)}
            cursor.execute(queryog, param)
            records = cursor.fetchall()
            # d_sym = {}
            ls_order_ids = []



            for row in records:
                # print("InsID = ", row[4], " ISIN = ", row[8], " Symbol = ", row[9], "\n")
                # print(row)
                # init holder dict
                d = {}
                for i in range(len(colnamesog)):
                    key_name = colnamesog[i]
                    d[key_name] = row[i]
                    
                ls_order_ids.append(d['order_id'])

            # get uniqe list of order_ids
            ls_uids = list(set(ls_order_ids))

            base_q = f"Select * FROM {msg_type} LIMIT 0"

            # first get the list of expected row names for this table
            cursor.execute(base_q)
            colnames = [desc[0] for desc in cursor.description]

            # print("Col names", colnames)

            order_ls_str_ls = []
            for uid in ls_uids:
                order_ls_str_ls.append(f"'{uid}'")

            order_ls_str = " ,".join(order_ls_str_ls)



            query = f"SELECT * FROM {msg_type} WHERE order_id IN ({order_ls_str});"
            # print(query)

            # param = {"jse_insid":int(jse_id)}
            cursor.execute(query)
            records = cursor.fetchall()
            # d_sym = {}

            for row in records:
                # print("InsID = ", row[4], " ISIN = ", row[8], " Symbol = ", row[9], "\n")
                # print(row)
                # init holder dict
                d = {}
                for i in range(len(colnames)):
                    key_name = colnames[i]
                    d[key_name] = row[i]
                    
                ls_data.append(d)



            # cursor.close()
            return ls_data


        except (Exception, psycopg2.Error) as error:
            print("Error while connecting to PostgreSQL", error)
            return ls_data









    def close(self):
            #closing database connection.
        if (self.connection):
            print("Closing PostgreSQL connection")
            self.connection.close()
            print("PostgreSQL connection is closed")
        else:
            print("No PostgreSQL connection to close")





# try create gen connection
# print("In conn")
# connection = Conn()
# print("Done")



