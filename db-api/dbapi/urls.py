from django.urls import path, include
from rest_framework.routers import DefaultRouter
from dbapi import views


router = DefaultRouter()


router.register('search', views.SearchSymbolView, basename='search')

router.register('getprimarymsgsinsid', views.GetPrimaryMsgsForInsidView, basename='getprimarymsgsinsid')
router.register('getsecondarymsgsinsid', views.GetSecondaryMsgsForInsidView, basename='getsecondarymsgsinsid')

router.register('getsymstatus', views.GetSymbolStatusView, basename='getsymstatus')
router.register('getaddorders', views.GetAddOrdersView, basename='getaddorders')
router.register('getmsgs', views.GetMessagesByTypeView, basename='getmsgs')
router.register('test', views.TestView, basename='test')
router.register('time', views.TimeView, basename='time')
router.register('getaddordersseqids', views.GetAddOrdersSeqIDView, basename='getaddordersseqids')
router.register('getexecordersseqids', views.GetExecOrdersSeqIDView, basename='getexecordersseqids')

# router.register('data-sources', views.DataSourceViewSet, basename='data-sources')
# router.register('data-sources', views.DataSourceModelViewSet)
# router.register('feeds', views.SourceFeedView, basename='feeds')
# router.register('feed-data', views.FeedDataView, basename='feed-data')
# router.register('app1/search', views.App1_Search_View, basename='app1/search')
# router.register('app1/bbg', views.App1_Bbg_View, basename='app1/bbg')
# router.register('app1/pere', views.App1_Pere_View, basename='app1/pere')
# router.register('app1/totm', views.App1_Totm_View, basename='app1/totm')
# router.register('app1/meta', views.App1_Meta_View, basename='app1/meta')
# router.register('app1/getDvds', views.App1_Dvds_View, basename='app1/getDvds')

urlpatterns = [
    path('', include(router.urls)),
]
