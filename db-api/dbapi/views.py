from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import viewsets

# from api import serializers
# from api import models
from django.core.serializers import serialize
from django.http import JsonResponse
import pandas as pd
import numpy as np

from django.shortcuts import render

from .src.conn import Conn
# from .src.conn import connection

# Create your views here.
# global conn
# conn = None


#########################
# UTILS
#########################

def build_js_table(ls_data):
    # get the rows and colnames from this list of data

    ls_cols = []
    ls_rows = []

    # we can just conv to pandas dataframe as we have list of dicts
    df = pd.DataFrame(ls_data)
    for i, row in df.iterrows():
        # build up dict of column names to vals with key for table
        d = {
            "key":str(i),
        }
        for col in df.columns:
            d[str(col)] = row[str(col)]

        ls_rows.append(d)


    # now build up list of column objects
    for col in df.columns:
        dcols  = {
            "title":str(col),
            "dataIndex":str(col),
            "key":str(col)
        }
        ls_cols.append(dcols)

    return ls_cols, ls_rows





class SearchSymbolView(viewsets.ViewSet):

    def list(self, request, format=None):

        connection = Conn()

        ls_data = connection.get_symbol_directory()

        return Response({'data':ls_data})



class GetPrimaryMsgsForInsidView(viewsets.ViewSet):
    # used to get ONLY the following message types as they rely on insid
    # SymbolStatus, Add_Order, Add_Attrib_Order
    # We could also possibly use this to get Trade, Aution_Trade and Off-BookTrade


    def create(self, request):


        rdata = request.data
        symNainsidme = rdata["insid"]
        msg_type = rdata["msgType"]


        connection = Conn()

        ls_data = connection.get_primary_msgs_for_insid(msg_type, symNainsidme)

        ls_cols, ls_rows = build_js_table(ls_data)

        return Response({'data':ls_data, "columns":ls_cols, "rows":ls_rows})



class GetSecondaryMsgsForInsidView(viewsets.ViewSet):
    # used to get ONLY the following message types as they rely on existing order_id 's
    # which means we need to first query the add order table for the symbol so we can get the entire 
    # Order_Executed, Order_ExecutedWPriceSize, Order_Modified, Order_Deleted


    def create(self, request):


        rdata = request.data
        symNainsidme = rdata["insid"]
        msg_type = rdata["msgType"]


        connection = Conn()

        ls_data = connection.get_secondary_msgs_for_insid(msg_type, symNainsidme)

        ls_cols, ls_rows = build_js_table(ls_data)


        return Response({'data':ls_data, "columns":ls_cols, "rows":ls_rows})



class GetSymbolStatusView(viewsets.ViewSet):

    # serializer_class = serializers.DataSourceSerializer

    def create(self, request):
        # all_entries = models.DataSource.objects.all()
        # global conn

        # get the requested symbol

        rdata = request.data
        print(rdata)
        symNainsidme = rdata["insid"]



        ls_data = connection.get_symbol_status_msgs(symNainsidme)

        print(ls_data)
        ls_cols = []
        ls_rows = []
        # convert to table display
        df = pd.DataFrame(ls_data)
        for i, row in df.iterrows():
            # build up dict of column names to vals with key for table
            d = {
                "key":str(i),
            }
            for col in df.columns:
                d[str(col)] = row[str(col)]

            ls_rows.append(d)


        # now build up list of column objects
        for col in df.columns:
            dcols  = {
                "title":str(col),
                "dataIndex":str(col),
                "key":str(col)
            }
            ls_cols.append(dcols)

        # conn.close()
        return Response({'data':ls_data, "columns":ls_cols, "rows":ls_rows})




class GetAddOrdersView(viewsets.ViewSet):

    # serializer_class = serializers.DataSourceSerializer

    def create(self, request):
        # all_entries = models.DataSource.objects.all()
        # global conn

        # get the requested symbol

        rdata = request.data
        print(rdata)
        symNainsidme = rdata["insid"]



        ls_data = connection.get_messages_by_type_for_insid("Add_Order", symNainsidme)

        print(ls_data)
        ls_cols = []
        ls_rows = []
        # convert to table display
        df = pd.DataFrame(ls_data)
        for i, row in df.iterrows():
            # build up dict of column names to vals with key for table
            d = {
                "key":str(i),
            }
            for col in df.columns:
                d[str(col)] = row[str(col)]

            ls_rows.append(d)


        # now build up list of column objects
        for col in df.columns:
            dcols  = {
                "title":str(col),
                "dataIndex":str(col),
                "key":str(col)
            }
            ls_cols.append(dcols)

        # conn.close()
        return Response({"columns":ls_cols, "rows":ls_rows})




class GetAddOrdersSeqIDView(viewsets.ViewSet):

    # serializer_class = serializers.DataSourceSerializer

    def create(self, request):
        # all_entries = models.DataSource.objects.all()
        # global conn

        # get the requested symbol

        rdata = request.data
        print(rdata)
        symNainsidme = rdata["insid"]
        seqIdStart = rdata["seqidStart"]
        seqIdEnd = rdata["seqidEnd"]


        # fix! For now also get the executed orders between seq ids

        ls_data  = connection.get_add_order_msgs_between_seqids(symNainsidme, seqIdStart, seqIdEnd)


        # print(ls_data)
        ls_cols = []
        ls_rows = []



        # convert to table display
        df = pd.DataFrame(ls_data)
        for i, row in df.iterrows():
            # build up dict of column names to vals with key for table
            d = {
                "key":str(i),
            }
            for col in df.columns:
                d[str(col)] = row[str(col)]

            ls_rows.append(d)


        # now build up list of column objects
        for col in df.columns:
            dcols  = {
                "title":str(col),
                "dataIndex":str(col),
                "key":str(col)
            }
            ls_cols.append(dcols)



        # conn.close()
        return Response({"columns":ls_cols, "rows":ls_rows})




class GetExecOrdersSeqIDView(viewsets.ViewSet):

    # serializer_class = serializers.DataSourceSerializer

    def create(self, request):
        # all_entries = models.DataSource.objects.all()
        # global conn

        # get the requested symbol

        rdata = request.data
        print(rdata)
        symNainsidme = rdata["insid"]
        seqIdStart = rdata["seqidStart"]
        seqIdEnd = rdata["seqidEnd"]


        # fix! For now also get the executed orders between seq ids

        ls_data = connection.get_exec_order_msgs_between_seqids(symNainsidme, seqIdStart, seqIdEnd)


        # print(ls_data)
        ls_cols = []
        ls_rows = []



        # convert to table display
        df = pd.DataFrame(ls_data)
        for i, row in df.iterrows():
            # build up dict of column names to vals with key for table
            d = {
                "key":str(i),
            }
            for col in df.columns:
                d[str(col)] = row[str(col)]

            ls_rows.append(d)


        # now build up list of column objects
        for col in df.columns:
            dcols  = {
                "title":str(col),
                "dataIndex":str(col),
                "key":str(col)
            }
            ls_cols.append(dcols)


        # conn.close()
        return Response({"columns":ls_cols, "rows":ls_rows})




class GetMessagesByTypeView(viewsets.ViewSet):

    # serializer_class = serializers.DataSourceSerializer

    def create(self, request):
        # all_entries = models.DataSource.objects.all()
        # global conn

        # get the requested symbol

        rdata = request.data
        print(rdata)
        symNainsidme = rdata["insid"]
        msg_type = rdata["msgType"]
        print(f"Message type requested: {msg_type}")



        ls_data = connection.get_messages_by_type_for_insid(msg_type, symNainsidme)

        print(ls_data)
        ls_cols = []
        ls_rows = []
        # convert to table display
        df = pd.DataFrame(ls_data)
        for i, row in df.iterrows():
            # build up dict of column names to vals with key for table
            d = {
                "key":str(i),
            }
            for col in df.columns:
                d[str(col)] = row[str(col)]

            ls_rows.append(d)


        # now build up list of column objects
        for col in df.columns:
            dcols  = {
                "title":str(col),
                "dataIndex":str(col),
                "key":str(col)
            }
            ls_cols.append(dcols)

        # conn.close()
        return Response({"columns":ls_cols, "rows":ls_rows})



class TestView(viewsets.ViewSet):

    # serializer_class = serializers.DataSourceSerializer

    def create(self, request):
        # all_entries = models.DataSource.objects.all()
        # global conn

        # get the requested symbol

        rdata = request.data
        print(rdata)
        symNainsidme = rdata["insid"]
        msg_type = rdata["msgType"]
        print(f"Message Testtype requested: {msg_type}")

        ls_data = connection.get_test(msg_type, symNainsidme)

        print(ls_data)
        ls_cols = []
        ls_rows = []
        # convert to table display
        df = pd.DataFrame(ls_data)
        for i, row in df.iterrows():
            # build up dict of column names to vals with key for table
            d = {
                "key":str(i),
            }
            for col in df.columns:
                d[str(col)] = row[str(col)]

            ls_rows.append(d)


        # now build up list of column objects
        for col in df.columns:
            dcols  = {
                "title":str(col),
                "dataIndex":str(col),
                "key":str(col)
            }
            ls_cols.append(dcols)

        # conn.close()
        return Response({"columns":ls_cols, "rows":ls_rows})



class TimeView(viewsets.ViewSet):

    # serializer_class = serializers.DataSourceSerializer

    def list(self, request, format=None):
        # all_entries = models.DataSource.objects.all()
        # global conn

        # get the requested symbol
        connection = Conn()



        ls_data = connection.get_time()

        ls_cols, ls_rows = build_js_table(ls_data)

        # conn.close()
        return Response({"columns":ls_cols, "rows":ls_rows})