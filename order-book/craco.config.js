const CracoLessPlugin = require('craco-less');

module.exports = {
    plugins: [
        {
            plugin: CracoLessPlugin,
            options: {
                lessLoaderOptions: {
                    lessOptions: {
                        modifyVars: { 
                            '@primary-color': '#457599',
                            '@layout-header-background': '#242222'
                        },
                        javascriptEnabled: true,
                    },
                },
            },
        },
    ],
};