import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';

import { Layout, Menu, Breadcrumb, Divider, Card, Icon, PageHeader, Tag, Button } from 'antd';
import {
    AppstoreOutlined,
} from '@ant-design/icons';

import CONN from '../assets/data/conn.json';

// import DataView from './DataView';

const BOLT_CONN = CONN.BOLT_CONN;
const neo4j = require('neo4j-driver');
const axios = require('axios');

const { Header, Content, Footer } = Layout;



class FeedView extends Component {

    state = {
        sourceFeedList: [],
    }


    componentDidMount() {
        document.title = "Feed View";
        // this.initHomeViewData();
        console.log("Current Feed State ", this.props.location.state);

        this.initSourceFeedViewData();



    }

    initSourceFeedViewData = () => {
        var pgHeadRaw = this.props.location.state.feedKey;
        var pgHeadSp = pgHeadRaw.split("-");
        var pgHead = pgHeadSp[1];
        var postData = {
            sourceName:pgHead
        };
        axios.post('http://localhost:8000/api/feeds/', postData).then(resp => {
            console.log(resp);
            if (resp.data.length > 0 ){
                var respData = JSON.parse(resp.data);
                console.log(respData, respData.length);

                var sf = [];
                for(var i=0; i<respData.length;i++){
                    var feedItem = respData[i];
                    sf.push(feedItem.fields);
                }

                this.setState({
                    sourceFeedList:sf
                });

            }


        });
    }





    render() {

        // console.log("Node data disp ", this.state.nodesAndFields);
        var pgHeadRaw = this.props.location.state.feedKey;
        var pgHeadSp = pgHeadRaw.split("-");
        var pgHead = pgHeadSp[1];

        var head = "Data Feeds for " + pgHead;

        var fCardItems = this.state.sourceFeedList.map(feedSrc => {
            var dsKey = feedSrc.id + "<DLM>" + feedSrc.name;
            var dsDivKey = feedSrc.id + "<DIV>" + feedSrc.name;

            var exContent = (
                <Link to={{
                    pathname:"/data",
                    state: {
                        feedKey:dsKey,
                        feedSchema:feedSrc.fields
                    }
                }}>
                    <Button>View</Button>
                </Link>
            );
            return (
                <div key={dsDivKey}>
                    <Card key={dsKey} style={{ width: '100%'}} title={feedSrc.name} extra={exContent}>
                        {feedSrc.description}
                    </Card>
                    <br></br>
                </div>
            );
        });




        return (
            <div>
                <Layout style={{height: '100vh'}}>
                    <PageHeader
                        onBack={() => window.history.back()}
                        title={head}
                        tags={<Tag color="blue">Active</Tag>}
                    >
                        <Content style={{ margin: '24px 16px 0', overflow: 'auto' }}>
                            <div>
                                {fCardItems}
                            </div>
                        </Content>
                    </PageHeader>
                </Layout>
            </div>
        );
    }
}

// map the state to props
const mapStateToProps = state => {
    return {
        token: state.token

    }
}


export default withRouter(connect(mapStateToProps)(FeedView));