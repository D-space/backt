import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import '../App.less';

import { 
  Layout,
  Menu,
  Breadcrumb,
  Divider,
  Card,
  Icon,
  Button,
  PageHeader,
  Input,
  Space,
  AutoComplete,
  Table,
  Descriptions,
  Select,
  Form,
  Slider, 
  InputNumber,
  Row,
  Col,
  Switch,
  Statistic
} from 'antd';
import {
    AppstoreOutlined,
    SearchOutlined,
    ArrowUpOutlined, 
    ArrowDownOutlined 
} from '@ant-design/icons';

import CONN from '../assets/data/conn.json';
import OrderBook from './OrderBook.js';
// import DataView from './DataView';
// import pg from 'pg'

const BOLT_CONN = CONN.BOLT_CONN;
// const neo4j = require('neo4j-driver');
const axios = require('axios');
var _ = require('lodash');

const { Header, Content, Footer } = Layout;

const { Search } = Input;

const FormItem = Form.Item;



class HomeView extends Component {

    state = {
        options:[],
        symStatusCols:[],
        symStatusRows:[],
        addOrderRows:[],
        addOrderCols:[],
        addAttribOrderRows:[],
        addAttribOrderCols:[],
        orderDelRows:[],
        orderDelCols:[],
        orderModRows:[],
        orderModCols:[],
        orderExecRows:[],
        orderExecCols:[],
        orderExecPriceSzRows:[],
        orderExecPriceSzCols:[],
        tradeRows:[],
        tradeCols:[],
        auctionTradeRows:[],
        auctionTradeCols:[],
        offBookTradeRows:[],
        offBookTradeCols:[],
        inputValue:0,
        currentTime:"",
        selectedInstrument:"",
        currentSeqStart:0,
        currentSeqEnd:0,
        obBuyRows:[],
        obBuyCols:[],
        obSellRows:[],
        obSellCols:[],
        timeRows:[],
        timeCols:[],
        auctionInfoRows:[],
        auctionInfoCols:[],
        statsRows:[],
        statsCols:[],
        isPaused:false,
        isPlaying:false,
        currentPosition:0,
        searchText: '',
        searchedColumn: '',
        intervalId:null,
        orderBook:null,
        lastExecPrice:null,
        lastStatusMsg:null,
        lastSpread:0,
    }

    formRef = React.createRef();



    getColumnSearchProps = dataIndex => ({
      filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
        <div style={{ padding: 8 }}>
          <Input
            ref={node => {
              this.searchInput = node;
            }}
            placeholder={`Search ${dataIndex}`}
            value={selectedKeys[0]}
            onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
            onPressEnter={() => this.handleSearchCols(selectedKeys, confirm, dataIndex)}
            style={{ marginBottom: 8, display: 'block' }}
          />
          <Space>
            <Button
              type="primary"
              onClick={() => this.handleSearchCols(selectedKeys, confirm, dataIndex)}
              icon={<SearchOutlined />}
              size="small"
              style={{ width: 90 }}
            >
              Search
            </Button>
            <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
              Reset
            </Button>
            <Button
              type="link"
              size="small"
              onClick={() => {
                confirm({ closeDropdown: false });
                this.setState({
                  searchText: selectedKeys[0],
                  searchedColumn: dataIndex,
                });
              }}
            >
              Filter
            </Button>
          </Space>
        </div>
      ),
      filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
      onFilter: (value, record) =>
        record[dataIndex]
          ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
          : '',
      onFilterDropdownVisibleChange: visible => {
        if (visible) {
          setTimeout(() => this.searchInput.select(), 100);
        }
      },
      render: text =>
        this.state.searchedColumn === dataIndex ? (
          <div
            style={{ backgroundColor: '#ffc069', padding: 0 }}
          >
            {text}
          </div>
        ) : (
          text
        ),
    });


    handleSearchCols = (selectedKeys, confirm, dataIndex) => {
      confirm();
      this.setState({
        searchText: selectedKeys[0],
        searchedColumn: dataIndex,
      });
    };

    handleReset = clearFilters => {
      clearFilters();
      this.setState({ searchText: '' });
    };



    componentDidMount() {
        document.title = "Order Book - Home";
        this.initHomeViewData();

    }


    buildOBCols = (side, sideCols) => {

      var fmtCols = [];



      sideCols.forEach( item => {


        if(item === "price") {

          var d = {
            title:item,
            dataIndex:item,
            key:item,
            render(text, record) {
              // Note we need to declare cellStyle var inside render func
              // otherwise it leads to animate bugs
              
              var cellStyle = {};

              // handle theme color depending on side
              if(side === "B") {
                cellStyle["background"] = "#D9EAD3";

              } else if(side === "S") {
                cellStyle["background"] = "#F4CCCC";

              }

              // console.log("In render col", text, typeof text);
              // text will be of type number
              // conv to str to get the length of the price value
              const numStr = text.toString();
              const numStrLen = numStr.length;
              var priceTxt = "";
              if(numStrLen == 1) {
                // only single digit. That means we're working with one cents
                // we want zar val so / by 100
                const zarc = text / 100;

                priceTxt = zarc.toString();
              } else {
                // we probably have atleast 8 digits
                // so extract the implied decimals ie last 8 digits
                // whats left will be our price in cents
                // conv to rands

                // ignore decimals for now
                var decim = numStr.slice(-8);
                // get the end of price digit
                const priceEndIndx = numStrLen - decim.length;
                // get price in cents
                const price =  numStr.slice(0, priceEndIndx);
                // conv to number so we can get in rands
                const numPrice = Number(price);
                const numPriceR = numPrice / 100;

                priceTxt = numPriceR.toString();

              }

              if(record.lifecycle === "DELNEXT") {
                cellStyle["borderStyle"] = "dotted";
                cellStyle["borderColor"] = "#FFA500";
                
              } else if(record.lifecycle === "MODIP") {
                cellStyle["borderStyle"] = "solid";
                cellStyle["borderColor"] = "#0000FF";

              } else if(record.lifecycle === "MODIPNEXT") {
                cellStyle["borderStyle"] = "dotted";
                cellStyle["borderColor"] = "#0000FF";

              } else if(record.lifecycle === "MOD") {
                cellStyle["borderStyle"] = "solid";
                cellStyle["borderColor"] = "#8000ff";

              } else if(record.lifecycle === "MODNEXT") {
                cellStyle["borderStyle"] = "dotted";
                cellStyle["borderColor"] = "#8000ff";

              } else if(record.lifecycle === "EXEC") {
                cellStyle["borderStyle"] = "solid";
                cellStyle["borderColor"] = "#FF0000";

              } else if(record.lifecycle === "EXECNEXT") {
                cellStyle["borderStyle"] = "dotted";
                cellStyle["borderColor"] = "#FF0000";

              }
              return {
                props: {
                  style: cellStyle
                },
                children: <div>{priceTxt}</div>
              };
            }
          };

          fmtCols.push(d);

        } 
        else if (item === "pos") {
          // just try render the index
          var d = {
            title:item,
            dataIndex:item,
            key:item,
            render(text, record,index) {
              return index + 1;
            }
          };
          fmtCols.push(d);
        }
        else {

          var d = {
            title:item,
            dataIndex:item,
            key:item,
            render(text, record) {
              // var cellStyle = {
              //   background: "#D9EAD3",
              // };

              var cellStyle = {};

              // handle theme color depending on side
              if(side === "B") {
                cellStyle["background"] = "#D9EAD3";

              } else if(side === "S") {
                cellStyle["background"] = "#F4CCCC";

              }

              if(record.lifecycle === "DELNEXT") {
                cellStyle["borderStyle"] = "dotted";
                cellStyle["borderColor"] = "#FFA500";
                
              } else if(record.lifecycle === "MODIP") {
                cellStyle["borderStyle"] = "solid";
                cellStyle["borderColor"] = "#0000FF";

              } else if(record.lifecycle === "MODIPNEXT") {
                cellStyle["borderStyle"] = "dotted";
                cellStyle["borderColor"] = "#0000FF";

              } else if(record.lifecycle === "MOD") {
                cellStyle["borderStyle"] = "solid";
                cellStyle["borderColor"] = "#8000ff";

              } else if(record.lifecycle === "MODNEXT") {
                cellStyle["borderStyle"] = "dotted";
                cellStyle["borderColor"] = "#8000ff";

              } else if(record.lifecycle === "EXEC") {
                cellStyle["borderStyle"] = "solid";
                cellStyle["borderColor"] = "#FF0000";

              } else if(record.lifecycle === "EXECNEXT") {
                cellStyle["borderStyle"] = "dotted";
                cellStyle["borderColor"] = "#FF0000";

              }
              return {
                props: {
                  style: cellStyle
                },
                children: <div>{text}</div>
              };
            }
          };

          fmtCols.push(d);


        }


      });

      return fmtCols;

    }





    initHomeViewData = () => {
        console.log("initing To quest");

        axios.get('http://127.0.0.1:8000/dbapi/search/').then(resp => {
            // console.log(resp.data);
            var respData = resp.data;

            console.log(respData);

            this.setState({
                options:respData.data
            });

        });

        axios.get('http://127.0.0.1:8000/dbapi/time/').then(resp => {

          var respData = resp.data;
          console.log(respData);

          this.setState({
              timeRows:respData.rows,
              timeCols:respData.columns,
          });

        });

        // set order book columns
        const buyCols = ["pos", "time", "nanosecond", "seq_id", "order_id", "quantity", "price"];
        // var buySideCols = []


        const sellCols = ["price", "quantity",  "order_id",  "seq_id", "nanosecond", "time", "pos"];
        // var sellSideCols = []


        var buySideCols2 = this.buildOBCols("B", buyCols);
        console.log("bs cols", buySideCols2);
        var sellSideCols2 = this.buildOBCols("S", sellCols);
        console.log("ss cols", sellSideCols2);



        this.setState({
            obBuyCols:buySideCols2,
            obSellCols:sellSideCols2,
        });


    }

    handleSearch = (val) => {
        // Fetch all the primary message types for selected ins
        console.log(val);

        axios.post('http://127.0.0.1:8000/dbapi/getprimarymsgsinsid/', {
          insid:val,
          msgType:"Symbol_Status"
        }).then(resp => {
          var respData = resp.data;
          // console.log(respData);
          this.setState({
              symStatusRows:respData.rows,
              symStatusCols:respData.columns,
          });
        });


        axios.post('http://127.0.0.1:8000/dbapi/getprimarymsgsinsid/', {
          insid:val,
          msgType:"Add_Order"
        }).then(resp => {
          var respData = resp.data;
          // console.log(respData);

          // try add search to columns
          var addColsSearch = []

          var addcols = respData.columns;

          for(let i=0; i<addcols.length;i++) {
            var colObj = addcols[i];

            var d = {
              title: colObj.title,
              dataIndex: colObj.dataIndex,
              key: colObj.key,
              ...this.getColumnSearchProps(colObj.title),
            };
            addColsSearch.push(d);

          }

          this.setState({
            addOrderRows:respData.rows,
            addOrderCols:addColsSearch,
          });
        });

        axios.post('http://127.0.0.1:8000/dbapi/getprimarymsgsinsid/', {
          insid:val,
          msgType:"Add_Attributed_Order"
        }).then(resp => {
          var respData = resp.data;
          // console.log(respData);
          this.setState({
            addAttribOrderRows:respData.rows,
            addAttribOrderCols:respData.columns,
          });
        });

        // Maybe try see if we can also get Trade msg types using primary


        axios.post('http://127.0.0.1:8000/dbapi/getprimarymsgsinsid/', {
          insid:val,
          msgType:"Trade"
        }).then(resp => {
          var respData = resp.data;
          this.setState({
              tradeRows:respData.rows,
              tradeCols:respData.columns,
          });
        });

        axios.post('http://127.0.0.1:8000/dbapi/getprimarymsgsinsid/', {
          insid:val,
          msgType:"Auction_Trade"
        }).then(resp => {
          var respData = resp.data;
          // console.log(respData);
          this.setState({
              auctionTradeRows:respData.rows,
              auctionTradeCols:respData.columns,
          });
        });

        axios.post('http://127.0.0.1:8000/dbapi/getprimarymsgsinsid/', {
          insid:val,
          msgType:"Off_Book_Trade"
        }).then(resp => {
          var respData = resp.data;
          // console.log(respData);
          this.setState({
              offBookTradeRows:respData.rows,
              offBookTradeCols:respData.columns,
          });
        });

        axios.post('http://127.0.0.1:8000/dbapi/getprimarymsgsinsid/', {
          insid:val,
          msgType:"Auction_Info"
        }).then(resp => {
          var respData = resp.data;
          // console.log(respData);
          this.setState({
              auctionInfoRows:respData.rows,
              auctionInfoCols:respData.columns,
          });
        });


        axios.post('http://127.0.0.1:8000/dbapi/getprimarymsgsinsid/', {
          insid:val,
          msgType:"Statistics"
        }).then(resp => {
          var respData = resp.data;
          // console.log(respData);
          this.setState({
              statsRows:respData.rows,
              statsCols:respData.columns,
          });
        });



        // Now fetch secondary message types 

        axios.post('http://127.0.0.1:8000/dbapi/getsecondarymsgsinsid/', {
          insid:val,
          msgType:"Order_Executed"
        }).then(resp => {
          var respData = resp.data;
          // console.log("Order Executed");
          // console.log(respData);
          // var execIds = [];
          // respData.rows.forEach((item, index)=>{
          //   execIds.push(item.order_id);

          // });
          // console.log("exec order ids", execIds);
          this.setState({
              orderExecRows:respData.rows,
              orderExecCols:respData.columns,
          });
        });

        axios.post('http://127.0.0.1:8000/dbapi/getsecondarymsgsinsid/', {
          insid:val,
          msgType:"Order_Executed_With_PriceORSize"
        }).then(resp => {
          var respData = resp.data;
          // console.log(respData);
          this.setState({
              orderExecPriceSzRows:respData.rows,
              orderExecPriceSzCols:respData.columns,
          });
        });

        axios.post('http://127.0.0.1:8000/dbapi/getsecondarymsgsinsid/', {
          insid:val,
          msgType:"Order_Modified"
        }).then(resp => {
          var respData = resp.data;
          // console.log(respData);
          this.setState({
              orderModRows:respData.rows,
              orderModCols:respData.columns,
          });
        });

        axios.post('http://127.0.0.1:8000/dbapi/getsecondarymsgsinsid/', {
          insid:val,
          msgType:"Order_Deleted"
        }).then(resp => {
          var respData = resp.data;
          // console.log(respData);
          this.setState({
              orderDelRows:respData.rows,
              orderDelCols:respData.columns,
          });
        });


        var obInstance = new OrderBook(val);


        this.setState({
          selectedInstrument:val,
          orderBook:obInstance,
        });






    }



    onSelect = (data) => {
      console.log('onSelect', data);
    };

    handleFrontSearch = (data) => {
        console.log('onSelect', data);
    }


    checkEmpty = (v) => {
        let type = typeof v;
        if (type === 'undefined') {
            return true;
        }
        if (type === 'boolean') {
            return !v;
        }
        if (v === null) {
            return true;
        }
        if (v === undefined) {
            return true;
        }
        if (v instanceof Array) {
            if (v.length < 1) {
                return true;
            }
        } else if (type === 'string') {
            if (v.length < 1) {
                return true;
            }
            if (v === '0') {
                return true;
            }
        } else if (type === 'object') {
            if (Object.keys(v).length < 1) {
                return true;
            }
            // check if object which has keys actually has values
            else {
                var ret = false;
                Object.values(v).forEach(x => {
                    if (x == null || x == "") {
                        ret = true;
                    }
                });
                return ret;
            }
        } else if (type === 'number') {
            if (v === 0) {
                return true;
            }
        }
        return false;
    }


    filterParams = (values) => {
        // Util func to filter out any 'null' or empty params

        // Filter the params so that we only consider non nulls and non empty strings ''
        var fVals = {};
        for (var param in values) {
            // console.log(param, values[param]);
            // if (values[param] == null || values[param] == "") {
            if (this.checkEmpty(values[param])) {
                // console.log("null param for", param);
            }
            else {
                fVals[param] = values[param];
            }
        }
        return fVals;
    }




    resetParamSelectionForm = () => {
        console.log("resetting form");

        // we can access the default form which ant created for us
        this.formRef.current.resetFields();
    }




    handleClickStepBack = (event) => {
      console.log("Stepping back")
      // get current input value and decrease val by 1
      const cVal = this.state.inputValue;
      const back = cVal - 1;

      if (back >= 0 ) {

        // trigger slider change event
        this.sliderOnChange(back);
      }
      else {
        console.log("Cant go below 0");
      }


    }


    handleClickStepFwd = (event) => {
      console.log("Stepping Fwd")
      // get current input value and decrease val by 1
      const cVal = this.state.inputValue;
      console.log("Input value:",cVal,"Running Play");
      // run the play once
      this.runPlay();
      const cVal2 = this.state.inputValue;
      console.log("Done Play. Input value at:", cVal2);


    }




    sliderOnChange = (value) => {
      console.log("New slider value", value);

      // When the slider is changed. We want to build up a new order book from scratch
      // Up to but not including value

      // create and init ob object
      // we can create these on the fly as we go
      // will help us insert and update order book state for each time step

      var obInstance = new OrderBook(this.state.selectedInstrument);

      // get the current time object
      const timei = this.state.timeRows[value];
      var timeip1 = null;

      // get seconds in readible fmt
      var date = new Date(null);
      date.setSeconds(timei.seconds);
      var result = date.toISOString().substr(11, 8);

      console.log("Current time", result);



      // NEW
      // First get all relavent msgs that fall within sequenceid
      // up to but not incl the current time

      // const timeMsgs = this.state.timeRows.filter((function(value) {
      //   return value.seq_id < timei.seq_id;
      // }));

      // console.log(timeMsgs, timeMsgs.length);

      const timeMsgsCheck = this.state.timeRows.slice(0,value);
      console.log(timeMsgsCheck, timeMsgsCheck.length);

      // NOTE! Looks like we can just slice between the 0th and valueth index
      // This is the same as filtering between sequence ids


      // Now we can loop through the array of timeMsgs in range
      // and update order book state 
      const timeMsgsLen = timeMsgsCheck.length;

      var updatedStateObj = null;


      for (var i=0; i<timeMsgsLen; i++) {

        // iterate and update the ib state object
        updatedStateObj = this.updateOBState(obInstance, i);
        // maintain obInstance object
        obInstance = updatedStateObj.orderBook;

        // build OB on second last time instance
        // So we can get proper state updates
        // backwards and fowards
        // if we dont do this we have states that persist incorrectly
        if (i === timeMsgsLen - 2) {
          console.log("Second last",i);
          const obRowsAlmost = obInstance.buildOrderBook();
        }


      }

      const obInstanceLast = updatedStateObj.orderBook;
      const timeInstance = updatedStateObj.time;
      const nextPos = updatedStateObj.nextPos;

      
      const currentSeqId = updatedStateObj.currentTimeSequence;
      const nextSeqId = updatedStateObj.nextTimeSequence;

      const obRows = obInstanceLast.buildOrderBook();
      console.log("Built ob from slider", obRows);

      const lastExec = obRows.lastExec;
      const lastStatus = obRows.lastStatus;

      const baSpread = obRows.spread;

      const numStr = baSpread.toString();
      const numStrLen = numStr.length;
      var priceTxt = "";
      if(numStrLen == 1) {
        // only single digit. That means we're working with one cents
        // we want zar val so / by 100
        const zarc = baSpread / 100;

        priceTxt = zarc.toString();
      } else {
        // we probably have atleast 8 digits
        // so extract the implied decimals ie last 8 digits
        // whats left will be our price in cents
        // conv to rands

        // ignore decimals for now
        var decim = numStr.slice(-8);
        // get the end of price digit
        const priceEndIndx = numStrLen - decim.length;
        // get price in cents
        const price =  numStr.slice(0, priceEndIndx);
        // conv to number so we can get in rands
        const numPrice = Number(price);
        const numPriceR = numPrice / 100;

        priceTxt = numPriceR.toString();

      }

      // check if we have last exec object
      var lastExecPrice = null;
      if(lastExec.hasOwnProperty("time") === true) {
        console.log("Got last exec obj", lastExec);
        const execOrder = lastExec.execOrder;
        lastExecPrice = execOrder.price;

      }

      var lastStatusMsg = null;
      if(lastStatus.hasOwnProperty("time") === true) {
        console.log("Got last status obj", lastStatus);
        // const statusObj = lastStatus.statusMsg;
        lastStatusMsg = lastStatus.statusText;

      }

      this.setState({
        inputValue: value,
        currentTime:timeInstance,
        obBuyRows:obRows.buySide,
        obSellRows:obRows.sellSide,
        orderBook:obInstanceLast,
        lastExecPrice:lastExecPrice,
        lastStatusMsg:lastStatusMsg,
        currentSeqStart:currentSeqId,
        currentSeqEnd:nextSeqId,
        lastSpread:priceTxt,

      });





    }








    handleClickPlay = (event) => {
      console.log("Clicked play");
      // todo: disable slider
      // get current input value of slider. Build ob to that point
      // then maintain ob object
      const cVal = this.state.inputValue;
      console.log(cVal, typeof cVal);

      const fwd = cVal + 1;
      console.log("Playing from", cVal);

      // maybe check if we've reached end of time messages before continuing

      const timei = this.state.timeRows[cVal];
      // var timeip1 = null;

      const timeLimit = this.state.timeRows.length;

      // first create the obInstance and add to state
      // var obInstance = new OrderBook(this.state.selectedInstrument);

      // var obInstance = this.state.orderBook;

      var intervalId = setInterval(this.runPlay, 100); 


      // set the interval id so we can clear when required 

      this.setState({
        intervalId:intervalId,
        // orderBook:obInstance
      });

      console.log("Done play");


    }


    runPlay = () => {
      var startTime = performance.now();
      const cpos = this.state.inputValue;
      console.log("Executing play at position", cpos);




      // get current ob state
      // update 
      // re-assign

      var curObInstance = this.state.orderBook;

      // we will get back an object containing the updated ob 
      // as well as the next position and the current time string
      var updatedStateObj = this.updateOBState(curObInstance, cpos);

      var obInstance = updatedStateObj.orderBook;
      // next pos will contain the pointer to the next time message to use
      const timeInstance = updatedStateObj.time;
      const nextPos = updatedStateObj.nextPos;

      const currentSeqId = updatedStateObj.currentTimeSequence;
      const nextSeqId = updatedStateObj.nextTimeSequence;

      const obRows = obInstance.buildOrderBook();
      // console.log("Built ob", obRows);

      const lastExec = obRows.lastExec;
      const lastStatus = obRows.lastStatus;

      const baSpread = obRows.spread;

      const numStr = baSpread.toString();
      const numStrLen = numStr.length;
      var priceTxt = "";
      if(numStrLen == 1) {
        // only single digit. That means we're working with one cents
        // we want zar val so / by 100
        const zarc = baSpread / 100;

        priceTxt = zarc.toString();
      } else {
        // we probably have atleast 8 digits
        // so extract the implied decimals ie last 8 digits
        // whats left will be our price in cents
        // conv to rands

        // ignore decimals for now
        var decim = numStr.slice(-8);
        // get the end of price digit
        const priceEndIndx = numStrLen - decim.length;
        // get price in cents
        const price =  numStr.slice(0, priceEndIndx);
        // conv to number so we can get in rands
        const numPrice = Number(price);
        const numPriceR = numPrice / 100;

        priceTxt = numPriceR.toString();

      }


      // check if we have last exec object
      var lastExecPrice = null;
      if(lastExec.hasOwnProperty("time") === true) {
        console.log("Got last exec obj", lastExec);
        const execOrder = lastExec.execOrder;
        lastExecPrice = execOrder.price;

      }

      // check if we have last status
      var lastStatusMsg = null;
      if(lastStatus.hasOwnProperty("time") === true) {
        console.log("Got last status obj", lastStatus);
        // const statusObj = lastStatus.statusMsg;
        lastStatusMsg = lastStatus.statusText;

      }







      this.setState({
        isPaused:false,
        isPlaying:true,
        inputValue:nextPos,
        currentTime:timeInstance,
        orderBook:obInstance,
        obBuyRows:obRows.buySide,
        obSellRows:obRows.sellSide,
        lastExecPrice:lastExecPrice,
        lastStatusMsg:lastStatusMsg,
        currentSeqStart:currentSeqId,
        currentSeqEnd:nextSeqId,
        lastSpread:priceTxt,

      });

      var endTime = performance.now();


      console.log(`Call to doSomething took ${endTime - startTime} milliseconds`);



    }










    handleClickPause = (event) => {
      console.log("Clicked pause");
      // todo: enable slider
      this.setState({isPaused:true});
      clearInterval(this.state.intervalId);
      console.log("Clicked done pause");

    }


    updateOBState = (orderBook, position) => {
      // take in the current order book
      // and position. Then we want to extract and append the relavent msgs
      // the retunrn the ob so we can assign 

      var startTime = performance.now();
      console.log("Executing play");
      // pick up from input value so we can scrub then play
      const cpos = position;
      console.log("current pos",cpos);

      const timei = this.state.timeRows[cpos];
      console.log("Playing at time", timei );

      // get seconds in readible fmt
      var date = new Date(null);
      date.setSeconds(timei.seconds);
      var result = date.toISOString().substr(11, 8);


      // update the state
      const nextPos = cpos + 1;

      var timeip1 = null;

      // get the current ob state
      var obInstance = orderBook;

      // also track the start and end seq ids for time

      var curSeqId = null;
      var nxtSeqId = null;



      // check if we can access at time value + 1
      if (this.state.timeRows[nextPos] !== undefined) {
        timeip1 = this.state.timeRows[nextPos];

        // get the symbol status message first
        const cSymStatus = this.state.symStatusRows.filter( (value)=> {
          return value.seq_id >= timei.seq_id && value.seq_id < timeip1.seq_id;
        });

        const cAddOrders = this.state.addOrderRows.filter( (value)=>{
          return value.seq_id >= timei.seq_id && value.seq_id < timeip1.seq_id;
        });

        const orderDelMsgs = this.state.orderDelRows.filter((function(value) {
          return value.seq_id >= timei.seq_id && value.seq_id < timeip1.seq_id;
        }));

        const orderModMsgs = this.state.orderModRows.filter((function(value) {
          return value.seq_id >= timei.seq_id && value.seq_id < timeip1.seq_id;
        }));

        const orderExecMsgs = this.state.orderExecRows.filter((function(value) {
          return value.seq_id >= timei.seq_id && value.seq_id < timeip1.seq_id;
        }));


        // possible way to impl color indicators for orders about to be deleted, or mod
        // get msgs from >= timeip2 to < timeip1
        // then send those ids into the obinstance
        // which we can use to lookp and flag the state before we build the ob

        // todo: handle edges
        const timeip2 = this.state.timeRows[nextPos+1];

        const cSymStatusNext = this.state.symStatusRows.filter((function(value) {
          return value.seq_id >= timeip1.seq_id && value.seq_id < timeip2.seq_id;
        }));


        const orderDelMsgsNext = this.state.orderDelRows.filter((function(value) {
          return value.seq_id >= timeip1.seq_id && value.seq_id < timeip2.seq_id;
        }));

        const orderModMsgsNext = this.state.orderModRows.filter((function(value) {
          return value.seq_id >= timeip1.seq_id && value.seq_id < timeip2.seq_id;
        }));

        const orderExecMsgsNext = this.state.orderExecRows.filter((function(value) {
          return value.seq_id >= timeip1.seq_id && value.seq_id < timeip2.seq_id;
        }));



        // bug: when we mutate the add orders in the OrderBook instance. We also change the addOrder state 
        // so we need to clone before we insert into ob to avoid

        var deepAddOrders = _.cloneDeep(cAddOrders);


        // Set the symbol status

        obInstance.insertSymbolStatus(timei, cSymStatus);




        // Now insert these orders into the order book structure
        obInstance.insertAddOrders(timei, deepAddOrders);
        obInstance.insertOrderDeleted(timei, orderDelMsgs);
        obInstance.insertOrderModified(timei, orderModMsgs);
        obInstance.insertOrderExecuted(timei, orderExecMsgs);


        // note that the next state updates need to be checked after we insert the orders
        // update state for orders about to be deleted
        obInstance.insertSymbolStatusNext(timeip2, cSymStatusNext);
        obInstance.insertOrderDeletedNext(timeip2, orderDelMsgsNext);
        obInstance.insertOrderModifiedNext(timeip2, orderModMsgsNext);
        obInstance.insertOrderExecutedNext(timeip2, orderExecMsgsNext);

        // console.log("Current Time", timei,timeip1, "Current Addorders", cAddOrders);

        // assign the current and next time seqIds;
        curSeqId = timei.seq_id;
        nxtSeqId = timeip1.seq_id;

      } else {
        // we have reached the end of the timeMsg array
        // so time+1 will be undefined
        // use seqId to define end of seq search
        // console.log("Reached end of time msgs. Using latest time", timei.seq_id);
        const cAddOrders = this.state.addOrderRows.filter( (value)=>{
          return value.seq_id >= timei.seq_id && value.seq_id < timei.seq_id;
        });

        const orderDelMsgs = this.state.orderDelRows.filter((function(value) {
          return value.seq_id >= timei.seq_id && value.seq_id < timei.seq_id;
        }));


        // Now insert these orders into the order book structure
        obInstance.insertAddOrders(timei, cAddOrders);
        obInstance.insertOrderDeleted(timei, orderDelMsgs);
        

        // console.log("Current Time End", timei,timei.seq_id, "Current Addorders", cAddOrders);

        // assign the current and next time seqIds;
        // this case - our next time obj is the current time object
        curSeqId = timei.seq_id;
        nxtSeqId = timei.seq_id;



      }

      var endTime = performance.now();


      console.log(`Call to doSomething took ${endTime - startTime} milliseconds`);

      // build and return object with updated state=
      var upState = {
        orderBook:obInstance,
        time:result,
        nextPos:nextPos,
        currentTimeSequence:curSeqId,
        nextTimeSequence:nxtSeqId
      };

      return upState;


    }








    render() {

      const { timeRows } = this.state;
      const { currentTime } = this.state;
      const { currentSeqStart } = this.state;
      const { currentSeqEnd } = this.state;
      const { lastExecPrice } = this.state;


      var numTime = timeRows.length;

      // gen marks on slider every 1k
      // todo:better  - dont need to go through each item

      var marks = {};

      var markArray = timeRows.filter((item, index)=>{
        if (index % 1000 === 0) {
          marks[index] = index;
        }
      });

      const numBuys = this.state.obBuyRows.length;
      const numSells = this.state.obSellRows.length;

      // format last exec price into rands
      var lastEPrice = "";

      // first check if not null
      if(lastExecPrice !== null) {

        const numStr = lastExecPrice.toString();
        const numStrLen = numStr.length;
        if(numStrLen == 1) {
          // only single digit. That means we're working with one cents
          // we want zar val so / by 100
          const zarc = lastExecPrice / 100;

          lastEPrice = zarc.toString();
        } else {
          // we probably have atleast 8 digits
          // so extract the implied decimals ie last 8 digits
          // whats left will be our price in cents
          // conv to rands

          // ignore decimals for now
          var decim = numStr.slice(-8);
          // get the end of price digit
          const priceEndIndx = numStrLen - decim.length;
          // get price in cents
          const price =  numStr.slice(0, priceEndIndx);
          // conv to number so we can get in rands
          const numPrice = Number(price);
          const numPriceR = numPrice / 100;

          lastEPrice = numPriceR.toString();

        }
      }





        return (
            <div>
                <Layout >
                  <Card style={{margin: '10px'}} className="cool-form-wrapper">

                        <Divider><h2>Select JSE Instrument</h2></Divider>
                        <AutoComplete
                          options={this.state.options}
                          style={{
                            width: '100%',
                          }}
                          onSelect={this.handleSearch}
                          filterOption={true}
                          
                        >
                          <Search size="large" placeholder="input here" allowClear enterButton />
                        </AutoComplete>


                    </Card>




                    <Content style={{ margin: '24px 16px 0'}}>
                        <br/>
                        <div className="cool-form-wrapper">                        
                          <Divider style={{ fontSize: '1.5em' }} orientation="left"> Replay Order Book </Divider>
                          <div>
                            <Row justify="center">
                              <Col span={2}>
                                <Button  type="primary" onClick={this.handleClickPlay}> Play </Button>
                              </Col>
                              <Col span={2}>
                                <Button type="primary" onClick={this.handleClickPause} ghost> Pause </Button>
                              </Col>
                            </Row>
                            <h3> {currentTime} </h3> <h5>SeqID {currentSeqStart} to {currentSeqEnd}</h5>

                            <Row>
                              <Col span={2}>
                                <Button style={{paddingRight:'10px'}} onClick={this.handleClickStepBack}> Step Back </Button>
                              </Col>
                              <Col span={20}>
                                <Slider
                                  min={0}
                                  max={numTime}
                                  onChange={this.sliderOnChange}
                                  value={this.state.inputValue}
                                  marks={marks}
                                />
                              </Col>
                              <Col span={2}>
                                <Button style={{paddingLeft:'10px'}} onClick={this.handleClickStepFwd}> Step Forward </Button>
                              </Col>
                            </Row>
                          </div>
                        </div>

                        <br/>
                        <div className="cool-form-wrapper">                        
                          <Divider style={{ fontSize: '1.5em' }} orientation="left"> Order Book </Divider>
                          <Row>
                              <Col span={12}>
                                <Card>
                                  <Statistic
                                    title="LAST (EXEC) PRICE "
                                    value={lastEPrice}
                                    precision={2}
                                    valueStyle={{ color: '#cf1322' }}
                                    prefix="R"
                                  />
                                </Card>
                              </Col>
                          </Row>
                          <Row>
                              <Col span={6}>
                                <Card>
                                Symbol Status: {this.state.lastStatusMsg}
                                </Card>
                              </Col>
                              <Col span={6}>
                                <Card>
                                Spread: {this.state.lastSpread}
                                </Card>
                              </Col>
                          </Row>
                          <Row>
                            <Col span={12}>

                              <div>
                              Buy ({numBuys})
                              <Table dataSource={this.state.obBuyRows} columns={this.state.obBuyCols} pagination={{ pageSize: 25 }} size="small" bordered />

                              </div>
                            </Col>
                            <Col span={12}>

                              <div>
                              Sell ({numSells})
                              <Table dataSource={this.state.obSellRows} columns={this.state.obSellCols} pagination={{ pageSize: 25 }} size="small" bordered/>

                              </div>
                            </Col>

                          </Row>

                        </div>

                    </Content>





                    <Content style={{ margin: '24px 16px 0'}}>
                        <br/>
                        <div className="cool-form-wrapper">                        
                          <Divider style={{ fontSize: '1.5em' }} orientation="left"> Time Msgs </Divider>
                          <div>
                          <Table dataSource={this.state.timeRows} columns={this.state.timeCols} size="small" bordered/>

                          </div>
                        </div>

                    </Content>

                    <Content style={{ margin: '24px 16px 0'}}>
                        <br/>
                        <div className="cool-form-wrapper">                        
                          <Divider style={{ fontSize: '1.5em' }} orientation="left"> Statistics </Divider>
                          <div>
                          <Table dataSource={this.state.statsRows} columns={this.state.statsCols} size="small" bordered/>

                          </div>
                        </div>

                    </Content>


                    <Content style={{ margin: '24px 16px 0'}}>
                        <br/>
                        <div className="cool-form-wrapper">                        
                          <Divider style={{ fontSize: '1.5em' }} orientation="left"> Auction Info </Divider>
                          <div>
                          <Table dataSource={this.state.auctionInfoRows} columns={this.state.auctionInfoCols} size="small" bordered/>

                          </div>
                        </div>

                    </Content>


                    <Content style={{ margin: '24px 16px 0'}}>
                        <br/>
                        <div className="cool-form-wrapper">                        
                          <Divider style={{ fontSize: '1.5em' }} orientation="left"> Symbol Status Msgs </Divider>
                          <div>
                          <Table dataSource={this.state.symStatusRows} columns={this.state.symStatusCols} size="small" bordered/>
                          </div>
                        </div>

                    </Content>
                  


                    <Content style={{ margin: '24px 16px 0'}}>
                        <br/>
                        <div className="cool-form-wrapper">                        
                          <Divider style={{ fontSize: '1.5em' }} orientation="left"> Add Orders </Divider>
                          <div>
                          <Table dataSource={this.state.addOrderRows} columns={this.state.addOrderCols} size="small" bordered/>

                          </div>
                        </div>

                    </Content>

                    <Content style={{ margin: '24px 16px 0'}}>
                        <br/>
                        <div className="cool-form-wrapper">                        
                          <Divider style={{ fontSize: '1.5em' }} orientation="left"> Add Attributed Orders </Divider>
                          <div>
                          <Table dataSource={this.state.addAttribOrderRows} columns={this.state.addAttribOrderCols} size="small" bordered/>
                          </div>
                        </div>

                    </Content>

                    <Content style={{ margin: '24px 16px 0'}}>
                        <br/>
                        <div className="cool-form-wrapper">                        
                          <Divider style={{ fontSize: '1.5em' }} orientation="left"> Order Deleted </Divider>
                          <div>
                          <Table dataSource={this.state.orderDelRows} columns={this.state.orderDelCols} size="small" bordered/>
                          </div>
                        </div>

                    </Content>

                    <Content style={{ margin: '24px 16px 0'}}>
                        <br/>
                        <div className="cool-form-wrapper">                        
                          <Divider style={{ fontSize: '1.5em' }} orientation="left"> Order Modified </Divider>
                          <div>
                          <Table dataSource={this.state.orderModRows} columns={this.state.orderModCols} size="small" bordered/>
                          </div>
                        </div>

                    </Content>

                    <Content style={{ margin: '24px 16px 0'}}>
                        <br/>
                        <div className="cool-form-wrapper">                        
                          <Divider style={{ fontSize: '1.5em' }} orientation="left"> Order Executed </Divider>
                          <div>
                          <Table dataSource={this.state.orderExecRows} columns={this.state.orderExecCols} size="small" bordered/>
                          </div>
                        </div>

                    </Content>

                    <Content style={{ margin: '24px 16px 0'}}>
                        <br/>
                        <div className="cool-form-wrapper">                        
                          <Divider style={{ fontSize: '1.5em' }} orientation="left"> Order Executed With Price/Size </Divider>
                          <div>
                          <Table dataSource={this.state.orderExecPriceSzRows} columns={this.state.orderExecPriceSzCols} size="small" bordered/>
                          </div>
                        </div>

                    </Content>

                    <Content style={{ margin: '24px 16px 0'}}>
                        <br/>
                        <div className="cool-form-wrapper">                        
                          <Divider style={{ fontSize: '1.5em' }} orientation="left"> Trade </Divider>
                          <div>
                          <Table dataSource={this.state.tradeRows} columns={this.state.tradeCols} size="small" bordered/>
                          </div>
                        </div>

                    </Content>

                    <Content style={{ margin: '24px 16px 0'}}>
                        <br/>
                        <div className="cool-form-wrapper">                        
                          <Divider style={{ fontSize: '1.5em' }} orientation="left"> Auction Trade </Divider>
                          <div>
                          <Table dataSource={this.state.auctionTradeRows} columns={this.state.auctionTradeCols} size="small" bordered/>
                          </div>
                        </div>

                    </Content>


                    <Content style={{ margin: '24px 16px 0'}}>
                        <br/>
                        <div className="cool-form-wrapper">                        
                          <Divider style={{ fontSize: '1.5em' }} orientation="left"> Off-Book Trade </Divider>
                          <div>
                          <Table dataSource={this.state.offBookTradeRows} columns={this.state.offBookTradeCols} size="small" bordered/>
                          </div>
                        </div>

                    </Content>











                </Layout>
            </div>
        );
    }
}

// map the state to props
const mapStateToProps = state => {
    return {
        token: state.token

    }
}


export default withRouter(connect(mapStateToProps)(HomeView));