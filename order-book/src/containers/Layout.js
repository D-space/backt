import React from 'react';
import {
    Layout,
    Menu
} from 'antd';
import { UserOutlined, LaptopOutlined, NotificationOutlined } from '@ant-design/icons';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import * as Actions from '../store/actions/Auth';


import NormalLoginForm from './Login';

const { SubMenu } = Menu;
const { Header, Content, Sider, Footer  } = Layout;

class CustomLayout extends React.Component {

    constructor(props) {
        super(props);
    }


    render() {
        // console.log("Layout props is authd", this.props.isAuthenticated);
        let userLayout = null;
        //If user is authenticated - disp default layout
        // all children should have their own layout content
        // console.log("on path", this.props.location.pathname);

        var authMenuItems = null;

        // display header items depending on the path
        if (this.props.location.pathname === "/home") {
            authMenuItems = (
                <Menu theme="dark" mode="horizontal" selectable={false}>
                    <Menu.Item key="M1">
                        Order Book
                    </Menu.Item>
                    <Menu.Item key="M2">
                      <Link to={{ pathname: "/replay" }} >
                        Replay
                      </Link>
                    </Menu.Item>
                    <Menu.Item key="M3" style={{ 'float': 'right' }} onClick={this.props.logout}>
                        Logout
                    </Menu.Item>
                </Menu>

            );
        }
        else {
            authMenuItems = (
                <Menu theme="dark" mode="horizontal" selectable={false}>
                    <Menu.Item key="M1">
                        Order Book
                    </Menu.Item>
                </Menu>

            );

        }

        if (this.props.isAuthenticated) {
            userLayout = (
                <Layout>
                    <Header className="header">
                        <div className="logo" />

                        {authMenuItems}
                    </Header>
                    {this.props.children}
                </Layout>
            );

        }
        //Otherwise no auth - display the Login layout
        else {
            userLayout = (
                <Layout>
                    <Header className="header">
                        <div className="logo" />
                        <Menu theme="dark" mode="horizontal" selectable={false}>
                            <Menu.Item key="M1">
                                Order Book
                            </Menu.Item>
                        </Menu>
                    </Header>
                    <Layout style={{ padding: '0 24px 24px' }}>
                        <Content
                            className="site-layout-background"
                        >
                            <NormalLoginForm {...this.props} />
                        </Content>
                    </Layout>
                </Layout>
            );

        }


        return (
            <div>
                {userLayout}
            </div>
        );
    }

}

const mapStateToProps = state => {
    return {
        username: state.username,
        token: state.token
    }
}


const mapDispatchToProps = dispatch => {
    return {
        logout: () => dispatch(Actions.logout())
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CustomLayout));

