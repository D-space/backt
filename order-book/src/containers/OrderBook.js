var _ = require('lodash');

export default class OrderBook {


    constructor(symbol) {
        this.symbol = symbol;

        this.orderBook = [];

        // maintain object to group orders by price level
        this.priceLevelsBuy = {};
        this.priceLevelsSell = {};

        // aslso maintain lookup from orderid to priceLevel
        this.orderIdToPriceLevelBuy = {};
        this.orderIdToPriceLevelSell = {};

        // keep track of last executed order. So we can extract relevant fields
        this.lastOrderExecuted = {};
        this.lastStatus = {};

        // track next state so we can detect transitions
        this.nextStatus = {};





    }


    display() {
        console.log("Displaying Order Book for:", this.symbol);
        console.log("Price Levels");
        console.log(this.priceLevelsBuy);
        console.log(this.priceLevelsSell);

    }


    insertSymbolStatus(timeStamp, orders) {
        // for symbol status - just track the latest msg

        // orderbook trading status lookup - see spec
        const statusLookup = {
            H:"Halt",
            T:"Regular Trading/Start Trade Reporting",
            a:"Opening Auction Call",
            b:"Post-Close",
            c:"Market Close",
            d:"Closing Auction Call",
            e:"Volatility Auction Call",
            E:"EOD Volume Auction Call",
            f:"Re-Opening Auction Call",
            l:"Pause",
            p:"Futures Close Out",
            s:"Closing Price Cross",
            u:"Intra-Day Auction Call",
            v:"End Trade Reporting",
            w:"No Active Session",
            x:"End of Post Close",
            y:"Start of Trading (This indicates that continuous trading or an opening auction is about to take place)",
            z:"Closing Price Publication",
        };

        if(orders.length > 0 ){
            // sort exec orders in descending order. So largest nano diff will be at indx 0
            var sorted = orders.sort(function(a, b){return b.nanosecond-a.nanosecond});
            console.log("Sorted", sorted);
            if(sorted.length > 0) {
                // get the first elem. That wil be the latest status msg  
                const lastStatus = sorted[0];
                console.log("Latest executed order", lastStatus);
                // assign to global state object
                const statusSymbol = lastStatus.trading_status;
                const statusMeaning = statusLookup[statusSymbol];
                const statusTxt = `${statusSymbol} (${statusMeaning})`;

                // // track previous state so we can detect transitions
                // const pStatus = _.cloneDeep(this.lastStatus);
                this.lastStatus = {
                    time:timeStamp,
                    statusMsg:lastStatus,
                    statusText:statusTxt,
                    statusSymbol:statusSymbol,
                    // prevStatus:pStatus,
                };
            }
        }
    }

    insertSymbolStatusNext(timeStamp, orders) {
        // for symbol status - just track the latest msg

        // orderbook trading status lookup - see spec
        const statusLookup = {
            H:"Halt",
            T:"Regular Trading/Start Trade Reporting",
            a:"Opening Auction Call",
            b:"Post-Close",
            c:"Market Close",
            d:"Closing Auction Call",
            e:"Volatility Auction Call",
            E:"EOD Volume Auction Call",
            f:"Re-Opening Auction Call",
            l:"Pause",
            p:"Futures Close Out",
            s:"Closing Price Cross",
            u:"Intra-Day Auction Call",
            v:"End Trade Reporting",
            w:"No Active Session",
            x:"End of Post Close",
            y:"Start of Trading (This indicates that continuous trading or an opening auction is about to take place)",
            z:"Closing Price Publication",
        };

        if(orders.length > 0 ){
            // sort exec orders in descending order. So largest nano diff will be at indx 0
            var sorted = orders.sort(function(a, b){return b.nanosecond-a.nanosecond});
            console.log("Sorted", sorted);
            if(sorted.length > 0) {
                // get the first elem. That wil be the latest status msg  
                const lastStatus = sorted[0];
                console.log("future status", lastStatus);
                // assign to global state object
                const statusSymbol = lastStatus.trading_status;
                const statusMeaning = statusLookup[statusSymbol];
                const statusTxt = `${statusSymbol} (${statusMeaning})`;

                // // track previous state so we can detect transitions
                // const pStatus = _.cloneDeep(this.lastStatus);
                this.nextStatus = {
                    time:timeStamp,
                    statusMsg:lastStatus,
                    statusText:statusTxt,
                    statusSymbol:statusSymbol,
                    // prevStatus:pStatus,
                };
            }
        }
    }



    insertOrderDeletedNext(timeStamp, orders) {

        // console.log(orders);

        orders.forEach((item,index)=>{
            const nanoDiff = item.nanosecond;
            const orderId = item.order_id;

            // console.log("About to delete order", item);

            // use lookup to check which side this order is placed at
            if (this.orderIdToPriceLevelBuy.hasOwnProperty(orderId) === true) {
                // console.log("Found in buy");

                // get the respective priceLevel for this order
                const priceTimeObj = this.orderIdToPriceLevelBuy[orderId];
                // we can now lookup the order in the priceLevel object
                // to get the array of add orders
                const addOrdersAtPrcTm = this.priceLevelsBuy[priceTimeObj.priceLevel][priceTimeObj.timeStamp];
                // now just filter to get the order_id that matched the next deleted order_id

                const addOrdersAtPrcTmUpdated = addOrdersAtPrcTm.filter((itemOrd)=>{
                    if(itemOrd.order_id === orderId){
                        // we found the order that's about to be deleted
                        // update lifecycle prop
                        console.log("TO BE DEL", itemOrd.order_id);
                        itemOrd["lifecycle"]="DELNEXT";
                        return itemOrd;
                    } else {
                        itemOrd["lifecycle"]="";
                        return itemOrd;
                    }
                });
                this.priceLevelsBuy[priceTimeObj.priceLevel][priceTimeObj.timeStamp] = addOrdersAtPrcTmUpdated;

            } else if (this.orderIdToPriceLevelSell.hasOwnProperty(orderId) === true) { 
                // bug: we need to check for the orderid in the mapping. Cant assume it will be there
                // cause we might get undefined

                // console.log("Found in sell");

                // get the respective priceLevel for this order
                const priceTimeObj = this.orderIdToPriceLevelSell[orderId];
                // we can now lookup the order in the priceLevel object
                // to get the array of add orders
                const addOrdersAtPrcTm = this.priceLevelsSell[priceTimeObj.priceLevel][priceTimeObj.timeStamp];
                // now just filter out this order id and re-assign

                const addOrdersAtPrcTmUpdated = addOrdersAtPrcTm.filter((itemOrd)=>{
                    if(itemOrd.order_id === orderId){
                        // we found the order that's about to be deleted
                        // update lifecycle prop
                        itemOrd["lifecycle"]="DELNEXT";
                        return itemOrd;
                    } else {
                        itemOrd["lifecycle"]="";
                        return itemOrd;
                    }
                });


                this.priceLevelsSell[priceTimeObj.priceLevel][priceTimeObj.timeStamp] = addOrdersAtPrcTmUpdated;


            } else {
                console.log("Order ID", orderId, "Not yet inserted");
            }

        });


    }



    insertOrderDeleted(timeStamp, orders) {

        // now for order to be modified
        // todo: maybe also remove the orderIDtoPriceLevel mapping

        orders.forEach((item,index)=>{
            const nanoDiff = item.nanosecond;
            const orderId = item.order_id;

            // console.log("About to delete order", item);

            // use lookup to check which side this order is placed at
            if (this.orderIdToPriceLevelBuy.hasOwnProperty(orderId) === true) {
                // console.log("Found in buy");

                // get the respective priceLevel for this order
                const priceTimeObj = this.orderIdToPriceLevelBuy[orderId];
                // we can now lookup the order in the priceLevel object
                // to get the array of add orders
                const addOrdersAtPrcTm = this.priceLevelsBuy[priceTimeObj.priceLevel][priceTimeObj.timeStamp];
                // now just filter out this order id and re-assign

                const addOrdersAtPrcTmUpdated = addOrdersAtPrcTm.filter((item)=>{
                    return item.order_id != orderId;
                });
                this.priceLevelsBuy[priceTimeObj.priceLevel][priceTimeObj.timeStamp] = addOrdersAtPrcTmUpdated;
                // now delete the lookup
                delete this.orderIdToPriceLevelBuy[orderId];

            } else if (this.orderIdToPriceLevelSell.hasOwnProperty(orderId) === true) {
                // console.log("Found in sell");

                // get the respective priceLevel for this order
                const priceTimeObj = this.orderIdToPriceLevelSell[orderId];
                // we can now lookup the order in the priceLevel object
                // to get the array of add orders
                const addOrdersAtPrcTm = this.priceLevelsSell[priceTimeObj.priceLevel][priceTimeObj.timeStamp];
                // now just filter out this order id and re-assign

                const addOrdersAtPrcTmUpdated = addOrdersAtPrcTm.filter((item)=>{
                    return item.order_id != orderId;
                });
                this.priceLevelsSell[priceTimeObj.priceLevel][priceTimeObj.timeStamp] = addOrdersAtPrcTmUpdated;
                delete this.orderIdToPriceLevelSell[orderId];



            } else {
                console.log("Order ID", orderId, "Not yet inserted");
            }




        });



    }

    insertOrderExecuted(timeStamp, orders) {
        console.log("Inserting order executed messages", orders);

        // also update the last executed price for this timeframe
        // process: simple max search algo. Where we look for the nanoseconds furthest away
        // as that will be the last exec price for this frame.



        // keep track of executed orders and the respective exection time
        var execOrders = [];



        orders.forEach((item, index)=>{

            const newNanoDiff = item.nanosecond;
            const orderId = item.order_id;
            const execQty = item.executed_quantity;
            const tradeId = item.trade_id;

            if (this.orderIdToPriceLevelBuy.hasOwnProperty(orderId) === true) {

                const priceTimeObj = this.orderIdToPriceLevelBuy[orderId];
                // we can now lookup the order in the priceLevel object
                // to get the array of add orders
                const addOrdersAtPrcTm = this.priceLevelsBuy[priceTimeObj.priceLevel][priceTimeObj.timeStamp];

                // now we just find this order id
                // then subtract the orderExec quantity from the current quantity and reassign
                // also track if qty has reached 0 after execution. If so, remove from order book

                console.log("og order list", addOrdersAtPrcTm);



                const addOrdersAtPrcTmUpdated = addOrdersAtPrcTm.filter((itemOrd)=>{
                    if(itemOrd.order_id === orderId){
                        
                        const cQuant = itemOrd.quantity;
                        console.log("Current quantity", cQuant, "Exec quantity", execQty);
                        const updatedQty = cQuant - execQty;
                        console.log("NEW quantity", updatedQty);

                        itemOrd.quantity = updatedQty;

                        itemOrd["lifecycle"]="EXEC";

                        // push to execOrders array so we can track latest exec order
                        var dOrd = {
                            execNanosecond:newNanoDiff,
                            execOrder:itemOrd
                        };
                        execOrders.push(dOrd);

                        // now we remove all rows that have <= 0 left for the quantity
                        // ie. remove this row if after execution, we have a qty <=0
                        if(updatedQty <= 0 ) {
                            return false;
                        } else {
                            return true;
                        }

                    } else {
                        return itemOrd;
                    }
                });

                console.log("updated order list", addOrdersAtPrcTmUpdated);


                this.priceLevelsBuy[priceTimeObj.priceLevel][priceTimeObj.timeStamp] = addOrdersAtPrcTmUpdated;



            } else if (this.orderIdToPriceLevelSell.hasOwnProperty(orderId) === true) {

                const priceTimeObj = this.orderIdToPriceLevelSell[orderId];
                // we can now lookup the order in the priceLevel object
                // to get the array of add orders
                const addOrdersAtPrcTm = this.priceLevelsSell[priceTimeObj.priceLevel][priceTimeObj.timeStamp];

                // now we just find this order id
                // then subtract the orderExec quantity from the current quantity and reassign
                // also track if qty has reached 0 after execution. If so, remove from order book

                console.log("og order list", addOrdersAtPrcTm);


                const addOrdersAtPrcTmUpdated = addOrdersAtPrcTm.filter((itemOrd)=>{
                    if(itemOrd.order_id === orderId){
                        
                        const cQuant = itemOrd.quantity;
                        console.log("Current quantity", cQuant, "Exec quantity", execQty);
                        const updatedQty = cQuant - execQty;
                        console.log("NEW quantity", updatedQty);

                        itemOrd.quantity = updatedQty;

                        itemOrd["lifecycle"]="EXEC";

                        // push to execOrders array so we can track latest exec order
                        var dOrd = {
                            execNanosecond:newNanoDiff,
                            execOrder:itemOrd
                        };
                        execOrders.push(dOrd);


                        // now we remove all rows that have <= 0 left for the quantity
                        // ie. remove this row if after execution, we have a qty <=0
                        if(updatedQty <= 0 ) {
                            return false;
                        } else {
                            return true;
                        }

                    } else {
                        return itemOrd;
                    }
                });
                console.log("updated order list", addOrdersAtPrcTmUpdated);


                this.priceLevelsSell[priceTimeObj.priceLevel][priceTimeObj.timeStamp] = addOrdersAtPrcTmUpdated;

            }




        });

        // sort exec orders in descending order. So largest nano diff will be at indx 0
        var sorted = execOrders.sort(function(a, b){return b.execNanosecond-a.execNanosecond});
        console.log("Sorted", sorted);
        if(sorted.length > 0) {
            // get the first elem. That wil be the latest exec 
            const latestExec = sorted[0];
            console.log("Latest executed order", latestExec);
            // assign to global state object
            this.lastOrderExecuted = {
                time:timeStamp,
                execNanosecond:latestExec.execNanosecond,
                execOrder:latestExec.execOrder
            };
        }


    }


    insertOrderExecutedNext(timeStamp, orders) {
        // Just update the state to EXECNEXT
        // Nothing more



        orders.forEach((item, index)=>{

            const newNanoDiff = item.nanosecond;
            const orderId = item.order_id;
            const execQty = item.executed_quantity;
            const tradeId = item.trade_id;

            if (this.orderIdToPriceLevelBuy.hasOwnProperty(orderId) === true) {

                const priceTimeObj = this.orderIdToPriceLevelBuy[orderId];
                // we can now lookup the order in the priceLevel object
                // to get the array of add orders
                const addOrdersAtPrcTm = this.priceLevelsBuy[priceTimeObj.priceLevel][priceTimeObj.timeStamp];

                // now we just find this order id
                // then subtract the orderExec quantity from the current quantity and reassign
                // also track if qty has reached 0 after execution. If so, remove from order book

                console.log("og order list", addOrdersAtPrcTm);



                const addOrdersAtPrcTmUpdated = addOrdersAtPrcTm.filter((itemOrd)=>{
                    if(itemOrd.order_id === orderId){
                        
                        itemOrd["lifecycle"]="EXECNEXT";
                        return itemOrd;

                    } else {
                        return itemOrd;
                    }
                });


                this.priceLevelsBuy[priceTimeObj.priceLevel][priceTimeObj.timeStamp] = addOrdersAtPrcTmUpdated;



            } else if (this.orderIdToPriceLevelSell.hasOwnProperty(orderId) === true) {

                const priceTimeObj = this.orderIdToPriceLevelSell[orderId];
                // we can now lookup the order in the priceLevel object
                // to get the array of add orders
                const addOrdersAtPrcTm = this.priceLevelsSell[priceTimeObj.priceLevel][priceTimeObj.timeStamp];

                // now we just find this order id
                // then subtract the orderExec quantity from the current quantity and reassign
                // also track if qty has reached 0 after execution. If so, remove from order book




                const addOrdersAtPrcTmUpdated = addOrdersAtPrcTm.filter((itemOrd)=>{
                    if(itemOrd.order_id === orderId){
                        


                        itemOrd["lifecycle"]="EXECNEXT";
                        return itemOrd;
                    } else {
                        return itemOrd;
                    }
                });


                this.priceLevelsSell[priceTimeObj.priceLevel][priceTimeObj.timeStamp] = addOrdersAtPrcTmUpdated;

            } else {
                console.log("Order ID", orderId, "Not yet inserted");
            }




        });


    }

    insertOrderModified(timeStamp, orders) {

        orders.forEach((item,index)=>{
            const newNanoDiff = item.nanosecond;
            const orderId = item.order_id;
            const newQty = item.new_quantity;
            const newPrice = item.new_price;
            // this flag will tell us if we need to update the position of this order in the ob
            // if flag is 1 we need to delete the existing order with this id. Then re add the order with the new price time and qty
            const flags = item.flags;

            console.log("About to mod order", item);

            // use lookup to check which side this order is placed at
            if (this.orderIdToPriceLevelBuy.hasOwnProperty(orderId) === true) {
                // console.log("Found in buy");

                // get the respective priceLevel for this order
                const priceTimeObj = this.orderIdToPriceLevelBuy[orderId];
                // we can now lookup the order in the priceLevel object
                // to get the array of add orders
                const addOrdersAtPrcTm = this.priceLevelsBuy[priceTimeObj.priceLevel][priceTimeObj.timeStamp];
                // now just filter to get the order_id that matched the next deleted order_id


                // so check the flag to see if we simply update the price and qty (flag=0)
                // or if we need to delete, then re add order at new spot (flag=1)

                if(flags === 0){
                    console.log("no flag", orderId);

                    // just find the order in the current list and update the values

                    const addOrdersAtPrcTmUpdated = addOrdersAtPrcTm.filter((itemOrd)=>{
                        if(itemOrd.order_id === orderId){
                            
                            itemOrd.quantity = newQty;
                            itemOrd.price = newPrice;

                            itemOrd["lifecycle"]="MODIP";


                            return itemOrd;
                        } else {
                            itemOrd["lifecycle"]="";
                            return itemOrd;
                        }
                    });

                    this.priceLevelsBuy[priceTimeObj.priceLevel][priceTimeObj.timeStamp] = addOrdersAtPrcTmUpdated;


                } else if(flags === 1) {
                    console.log("got flag", orderId);

                    // first get current state of ONLY this order
                    var addOrdersAtPrcTmUpdatedCurr = addOrdersAtPrcTm.find((itemOrd)=>{
                        return itemOrd.order_id === orderId;
                    });
                    console.log("found order to mod", addOrdersAtPrcTmUpdatedCurr);

                    // now "delete" this order from the priceLevelArray
                    const addOrdersAtPrcTmUpdated = addOrdersAtPrcTm.filter((itemOrd)=>{
                        return itemOrd.order_id != orderId;
                    });

                    this.priceLevelsBuy[priceTimeObj.priceLevel][priceTimeObj.timeStamp] = addOrdersAtPrcTmUpdated;
                    
                    // now assign the updated values to the current order object

                    const timeStampMs = timeStamp.seconds * 1000;
                    // javascript only allows ms resolution. So chop off remainder for now
                    const nsToMs = Math.floor(newNanoDiff / 1000000);

                    const msgTimeMs = timeStampMs + nsToMs;

                    // now get formatted date and assign to field
                    var msgdate = new Date(msgTimeMs);
                    // date.setMilliseconds(msgTimeMs);

                    // var isoDateTime = new Date(msgdate.getTime() + (msgdate.getTimezoneOffset() * 60000)).toISOString();
                    var isoDateTime = new Date(msgdate.getTime());
                    // console.log("iso", isoDateTime);
                    var localISOTime = isoDateTime.toISOString().substr(11,12);
                    
                    // console.log("Local time for item, ",item.seq_id, "nanoseconds", nanoDiff, "time", localISOTime)  // => '2015-01-26T06:40:36.181'

                    // now copy the item and assign the Time Field
                    var itemCopy = addOrdersAtPrcTmUpdatedCurr;
                    itemCopy.quantity = newQty;
                    itemCopy.price = newPrice;
                    itemCopy.nanosecond = newNanoDiff;

                    itemCopy["lifecycle"]="MOD";
                    itemCopy["time"] = localISOTime;

                    this.insertAddOrderAtPriceLevel(this.priceLevelsBuy, timeStamp, newPrice, itemCopy);
                    // now map this oderId to it's pricelevel, timeStamp
                    this.orderIdToPriceLevelBuy[orderId] = {
                        priceLevel:newPrice,
                        timeStamp: timeStamp.seconds
                    }
                }


            } else if (this.orderIdToPriceLevelSell.hasOwnProperty(orderId) === true) { 
                // bug: we need to check for the orderid in the mapping. Cant assume it will be there
                // cause we might get undefined

                // console.log("Found in sell");

                // get the respective priceLevel for this order
                const priceTimeObj = this.orderIdToPriceLevelSell[orderId];
                // we can now lookup the order in the priceLevel object
                // to get the array of add orders
                const addOrdersAtPrcTm = this.priceLevelsSell[priceTimeObj.priceLevel][priceTimeObj.timeStamp];
                // now just filter out this order id and re-assign


                
                if(flags === 0){
                    console.log("no flag", orderId);

                    // just find the order in the current list and update the values

                    const addOrdersAtPrcTmUpdated = addOrdersAtPrcTm.filter((itemOrd)=>{
                        if(itemOrd.order_id === orderId){
                            
                            itemOrd.quantity = newQty;
                            itemOrd.price = newPrice;

                            itemOrd["lifecycle"]="MODIP";


                            return itemOrd;
                        } else {
                            itemOrd["lifecycle"]="";
                            return itemOrd;
                        }
                    });

                    this.priceLevelsSell[priceTimeObj.priceLevel][priceTimeObj.timeStamp] = addOrdersAtPrcTmUpdated;


                } else if(flags === 1) {
                    console.log("got flag", orderId);

                    // first get current state of ONLY this order
                    var addOrdersAtPrcTmUpdatedCurr = addOrdersAtPrcTm.find((itemOrd)=>{
                        return itemOrd.order_id === orderId;
                    });

                    // now "delete" this order from the priceLevelArray
                    const addOrdersAtPrcTmUpdated = addOrdersAtPrcTm.filter((itemOrd)=>{
                        return itemOrd.order_id != orderId;
                    });

                    this.priceLevelsSell[priceTimeObj.priceLevel][priceTimeObj.timeStamp] = addOrdersAtPrcTmUpdated;
                    
                    // now assign the updated values to the current order object

                    const timeStampMs = timeStamp.seconds * 1000;
                    // javascript only allows ms resolution. So chop off remainder for now
                    const nsToMs = Math.floor(newNanoDiff / 1000000);

                    const msgTimeMs = timeStampMs + nsToMs;

                    // now get formatted date and assign to field
                    var msgdate = new Date(msgTimeMs);
                    // date.setMilliseconds(msgTimeMs);

                    // var isoDateTime = new Date(msgdate.getTime() + (msgdate.getTimezoneOffset() * 60000)).toISOString();
                    var isoDateTime = new Date(msgdate.getTime());
                    // console.log("iso", isoDateTime);
                    var localISOTime = isoDateTime.toISOString().substr(11,12);
                    
                    // console.log("Local time for item, ",item.seq_id, "nanoseconds", nanoDiff, "time", localISOTime)  // => '2015-01-26T06:40:36.181'

                    // now copy the item and assign the Time Field
                    var itemCopy = addOrdersAtPrcTmUpdatedCurr;
                    itemCopy.quantity = newQty;
                    itemCopy.price = newPrice;
                    itemCopy.nanosecond = newNanoDiff;

                    itemCopy["lifecycle"]="MOD";
                    itemCopy["time"] = localISOTime;

                    this.insertAddOrderAtPriceLevel(this.priceLevelsSell, timeStamp, newPrice, itemCopy);
                    // now map this oderId to it's pricelevel, timeStamp
                    this.orderIdToPriceLevelSell[orderId] = {
                        priceLevel:newPrice,
                        timeStamp: timeStamp.seconds
                    }
                }


            } else {
                console.log("Order ID", orderId, "Not yet inserted");
            }

        });

    }



    insertOrderModifiedNext(timeStamp, orders) {

      // Just update the state. Nothing else

        orders.forEach((item,index)=>{
            const newNanoDiff = item.nanosecond;
            const orderId = item.order_id;
            const newQty = item.new_quantity;
            const newPrice = item.new_price;
            // this flag will tell us if we need to update the position of this order in the ob
            // if flag is 1 we need to delete the existing order with this id. Then re add the order with the new price time and qty
            const flags = item.flags;


            console.log("About to mod order", item);

            // use lookup to check which side this order is placed at
            if (this.orderIdToPriceLevelBuy.hasOwnProperty(orderId) === true) {
                // console.log("Found in buy");

                // get the respective priceLevel for this order
                const priceTimeObj = this.orderIdToPriceLevelBuy[orderId];
                // we can now lookup the order in the priceLevel object
                // to get the array of add orders
                const addOrdersAtPrcTm = this.priceLevelsBuy[priceTimeObj.priceLevel][priceTimeObj.timeStamp];

                if(flags === 0){
                    console.log("no flag for Next", orderId);


                    const addOrdersAtPrcTmUpdated = addOrdersAtPrcTm.filter((itemOrd)=>{
                        if(itemOrd.order_id === orderId){
                            itemOrd["lifecycle"]="MODIPNEXT";
                            return itemOrd;
                        } else {
                            // mabe also set the lifecycle state to empty
                            // like in del next. Or not?
                            itemOrd["lifecycle"]="";
                            return itemOrd;
                        }
                    });

                    this.priceLevelsBuy[priceTimeObj.priceLevel][priceTimeObj.timeStamp] = addOrdersAtPrcTmUpdated;


                } else if(flags === 1) {
                    // could probably simplify this further.
                    // just check for the flag inside the filter.
                    console.log("got flag for next", orderId);


                    const addOrdersAtPrcTmUpdated = addOrdersAtPrcTm.filter((itemOrd)=>{
                        if(itemOrd.order_id === orderId){
                            itemOrd["lifecycle"]="MODNEXT";
                            return itemOrd;
                        } else {
                            // mabe also set the lifecycle state to empty
                            // like in del next. Or not?
                            itemOrd["lifecycle"]="";
                            return itemOrd;
                        }
                    });

                    this.priceLevelsBuy[priceTimeObj.priceLevel][priceTimeObj.timeStamp] = addOrdersAtPrcTmUpdated;



                }


            } else if (this.orderIdToPriceLevelSell.hasOwnProperty(orderId) === true) { 
                // bug: we need to check for the orderid in the mapping. Cant assume it will be there
                // cause we might get undefined

                // console.log("Found in sell");

                // get the respective priceLevel for this order
                const priceTimeObj = this.orderIdToPriceLevelSell[orderId];
                // we can now lookup the order in the priceLevel object
                // to get the array of add orders
                const addOrdersAtPrcTm = this.priceLevelsSell[priceTimeObj.priceLevel][priceTimeObj.timeStamp];
                // now just filter out this order id and re-assign


                
                if(flags === 0){
                    console.log("no flag next", orderId);

                    // just find the order in the current list and update the values

                    const addOrdersAtPrcTmUpdated = addOrdersAtPrcTm.filter((itemOrd)=>{
                        if(itemOrd.order_id === orderId){

                            itemOrd["lifecycle"]="MODIPNEXT";


                            return itemOrd;
                        } else {
                            itemOrd["lifecycle"]="";
                            return itemOrd;
                        }
                    });

                    this.priceLevelsSell[priceTimeObj.priceLevel][priceTimeObj.timeStamp] = addOrdersAtPrcTmUpdated;


                } else if(flags === 1) {
                    console.log("got flag next", orderId);

                    const addOrdersAtPrcTmUpdated = addOrdersAtPrcTm.filter((itemOrd)=>{
                        if(itemOrd.order_id === orderId){

                            itemOrd["lifecycle"]="MODNEXT";

                            return itemOrd;
                        } else {
                            itemOrd["lifecycle"]="";
                            return itemOrd;
                        }
                    });

                    this.priceLevelsSell[priceTimeObj.priceLevel][priceTimeObj.timeStamp] = addOrdersAtPrcTmUpdated;

                }


            } else {
                console.log("Order ID", orderId, "Not yet inserted");
            }

        });

    }



    insertAddOrderAtPriceLevel(priceLevelSideObj, timeStamp, orderPrice, item ){

        if (priceLevelSideObj.hasOwnProperty(orderPrice) === true) {
            // console.log(orderPrice, "Already exists in priceLevels");

            // get the corrosponding object of timeStamps at this lvl
            var priceLevelTimes = priceLevelSideObj[orderPrice];

            // now check if the object of timeStamps
            // contain the provided timeStamp
            if (priceLevelTimes.hasOwnProperty(timeStamp.seconds) === true) {
                // just append this order to this list
                priceLevelSideObj[orderPrice][timeStamp.seconds].push(item);


            } else {
                // level does not contain this timeStamp. 
                // Create empty array at timestamp
                // then push item
                priceLevelSideObj[orderPrice][timeStamp.seconds]=[];
                priceLevelSideObj[orderPrice][timeStamp.seconds].push(item);

            }



        } else {
            // price does not exist. So create
            // assumption: If the price dne then the time dne. So create everything then push object
            priceLevelSideObj[orderPrice]={};
            priceLevelSideObj[orderPrice][timeStamp.seconds]=[];
            priceLevelSideObj[orderPrice][timeStamp.seconds].push(item);
        }

    }


    insertAddOrders(timeStamp, addOrders) {

        // now for each order to be added
        // check if we have that price level
        // if so, then check if we have that timeStamp at that level
        // if so, then append order object to list of addOrders for that timeStamp at that level

        addOrders.forEach((item,index)=>{
            const orderPrice = item.price;
            const nanoDiff = item.nanosecond;
            const orderId = item.order_id;
            const side = item.side;

            // try add readible date field
            // made up off the latest timestamp seconds (conv ms) + msg nanoseconds (conv ms) + remainder

            const timeStampMs = timeStamp.seconds * 1000;
            // javascript only allows ms resolution. So chop off remainder for now
            const nsToMs = Math.floor(nanoDiff / 1000000);

            const msgTimeMs = timeStampMs + nsToMs;

            // now get formatted date and assign to field
            var msgdate = new Date(msgTimeMs);
            // date.setMilliseconds(msgTimeMs);
            // console.log("Date", msgdate);



            // note only toISOString supports ms precision
            // so need to handle timezones
            // note that in our case we are GMT+2 ie 2 hours ahead of UTC
            // so our timezone offset will be -120 mins
            // Therefore to offset we need to add the tzoffset not subtract

            // for some reason if we just make a new date 
            // then conv to iso str. The timezone works out properly


            // var isoDateTime = new Date(msgdate.getTime() + (msgdate.getTimezoneOffset() * 60000)).toISOString();
            var isoDateTime = new Date(msgdate.getTime());
            // console.log("iso", isoDateTime);
            var localISOTime = isoDateTime.toISOString().substr(11,12);
            
            // console.log("Local time for item, ",item.seq_id, "nanoseconds", nanoDiff, "time", localISOTime)  // => '2015-01-26T06:40:36.181'

            // now copy the item and assign the Time Field
            var itemCopy = item;
            itemCopy["time"] = localISOTime;

            // append lifecycle state
            itemCopy["lifecycle"]="";






            if (side === "B") {
                this.insertAddOrderAtPriceLevel(this.priceLevelsBuy, timeStamp, orderPrice, itemCopy);
                // now map this oderId to it's pricelevel, timeStamp
                this.orderIdToPriceLevelBuy[orderId] = {
                    priceLevel:orderPrice,
                    timeStamp: timeStamp.seconds
                }
            } else {
                this.insertAddOrderAtPriceLevel(this.priceLevelsSell, timeStamp, orderPrice, itemCopy);
                // this.orderIdToPriceLevelSell[orderId] = orderPrice;
                this.orderIdToPriceLevelSell[orderId] = {
                    priceLevel:orderPrice,
                    timeStamp: timeStamp.seconds
                };



            }



        });



    }


    sortArryForSide(items, side) {

        // sort will 

        if (side === "B") {
            // sort in descending order
            var sorted = items.sort(function(a, b){return b-a});
            return sorted;

        } else {
            // sort in ascending order
            var sorted = items.sort(function(a, b){return a-b});
            return sorted;

        }

    }


    buildOrderBookForSide(priceLevelSideObj, side) {

        // console.log("Building OB for side", side);



        // sort if needed. will determine order we fetch items in
        const pKeysRaw = Object.keys(priceLevelSideObj);
        // console.log(pKeysRaw);
        // console.log("sorted");
        // sort in descending order if buy side and ascending if sell side
        const pKeys = this.sortArryForSide(pKeysRaw, side);
        // console.log(pKeys);

        var rows = [];

        for (var i=0; i< pKeys.length; i++) {
            // this key will essentially be the price
            let keyi = pKeys[i];
            // value will be an object where key is timestamp and value us array of objects at timestamp
            let valuei = priceLevelSideObj[keyi];
            // console.log(valuei);

            // so go through each timestamp 
            let tsKeys = Object.keys(valuei);
            // we will always want this sorted in ascending order
            tsKeys.sort(function(a, b){return a-b});


            for (var j=0; j<tsKeys.length; j++) {
                // keyj will be the timestamp in seconds
                let keyj = tsKeys[j];
                // valuej will be the array of add orders for this ts
                var valuej = valuei[keyj];
                // console.log(valuej);

                // maybe sort by nanosecond to be sure?
                // and add second field
                rows = rows.concat(valuej);

            }


        }
        // console.log("Done building rows", rows);
        return rows

    }


    clearOrderState() {
        // remove all lifecycle states for the orders

        // first get all order ids in use

        const buyIds = Object.keys(this.orderIdToPriceLevelBuy);

        for(var i=0; i<buyIds.length; i++) {
            // get the current id
            const orderId = buyIds[i];

            // use lookup to check which side this order is placed at
            if (this.orderIdToPriceLevelBuy.hasOwnProperty(orderId) === true) {
                // console.log("Found in buy");

                // get the respective priceLevel for this order
                const priceTimeObj = this.orderIdToPriceLevelBuy[orderId];
                // we can now lookup the order in the priceLevel object
                // to get the array of add orders
                const addOrdersAtPrcTm = this.priceLevelsBuy[priceTimeObj.priceLevel][priceTimeObj.timeStamp];
                // now just clear state and assign

                const addOrdersAtPrcTmUpdated = addOrdersAtPrcTm.filter((itemOrd)=>{
                    if(itemOrd.order_id === orderId){
                        // clear state of all items here
                        itemOrd["lifecycle"]="";
                        return itemOrd;
                    } else {
                        itemOrd["lifecycle"]="";
                        return itemOrd;
                    }
                });

                this.priceLevelsBuy[priceTimeObj.priceLevel][priceTimeObj.timeStamp] = addOrdersAtPrcTmUpdated;

            } else if (this.orderIdToPriceLevelSell.hasOwnProperty(orderId) === true) {
                // console.log("Found in sell");

                // get the respective priceLevel for this order
                const priceTimeObj = this.orderIdToPriceLevelSell[orderId];
                // we can now lookup the order in the priceLevel object
                // to get the array of add orders
                const addOrdersAtPrcTm = this.priceLevelsSell[priceTimeObj.priceLevel][priceTimeObj.timeStamp];
                // now just filter out this order id and re-assign

                const addOrdersAtPrcTmUpdated = addOrdersAtPrcTm.filter((itemOrd)=>{
                    if(itemOrd.order_id === orderId){
                        // clear state of all items here
                        itemOrd["lifecycle"]="";
                        return itemOrd;
                    } else {
                        itemOrd["lifecycle"]="";
                        return itemOrd;
                    }
                });

                this.priceLevelsSell[priceTimeObj.priceLevel][priceTimeObj.timeStamp] = addOrdersAtPrcTmUpdated;



            }


        }

        // Now do sell
        const sellIds = Object.keys(this.orderIdToPriceLevelSell);

        for(var i=0; i<sellIds.length; i++) {
            // get the current id
            const orderId = sellIds[i];

            // use lookup to check which side this order is placed at
            if (this.orderIdToPriceLevelBuy.hasOwnProperty(orderId) === true) {
                // console.log("Found in buy");

                // get the respective priceLevel for this order
                const priceTimeObj = this.orderIdToPriceLevelBuy[orderId];
                // we can now lookup the order in the priceLevel object
                // to get the array of add orders
                const addOrdersAtPrcTm = this.priceLevelsBuy[priceTimeObj.priceLevel][priceTimeObj.timeStamp];
                // now just clear state and assign

                const addOrdersAtPrcTmUpdated = addOrdersAtPrcTm.filter((itemOrd)=>{
                    if(itemOrd.order_id === orderId){
                        // clear state of all items here
                        itemOrd["lifecycle"]="";
                        return itemOrd;
                    } else {
                        itemOrd["lifecycle"]="";
                        return itemOrd;
                    }
                });

                this.priceLevelsBuy[priceTimeObj.priceLevel][priceTimeObj.timeStamp] = addOrdersAtPrcTmUpdated;

            } else if (this.orderIdToPriceLevelSell.hasOwnProperty(orderId) === true) {
                // console.log("Found in sell");

                // get the respective priceLevel for this order
                const priceTimeObj = this.orderIdToPriceLevelSell[orderId];
                // we can now lookup the order in the priceLevel object
                // to get the array of add orders
                const addOrdersAtPrcTm = this.priceLevelsSell[priceTimeObj.priceLevel][priceTimeObj.timeStamp];
                // now just filter out this order id and re-assign

                const addOrdersAtPrcTmUpdated = addOrdersAtPrcTm.filter((itemOrd)=>{
                    if(itemOrd.order_id === orderId){
                        // clear state of all items here
                        itemOrd["lifecycle"]="";
                        return itemOrd;
                    } else {
                        itemOrd["lifecycle"]="";
                        return itemOrd;
                    }
                });

                this.priceLevelsSell[priceTimeObj.priceLevel][priceTimeObj.timeStamp] = addOrdersAtPrcTmUpdated;



            }


        }






    }


    clearEverything() {

      this.orderBook = [];

      // maintain object to group orders by price level
      this.priceLevelsBuy = {};
      this.priceLevelsSell = {};

      // aslso maintain lookup from orderid to priceLevel
      this.orderIdToPriceLevelBuy = {};
      this.orderIdToPriceLevelSell = {};

      // keep track of last executed order. So we can extract relevant fields
      this.lastOrderExecuted = {};

    }




    buildOrderBook() {
        // console.log("Building OB Hiigh");
        // first do buy side

        // deep copy current state so we can flush after
        var deepPriceLevelsBuy = _.cloneDeep(this.priceLevelsBuy);
        var deepPriceLevelsSell = _.cloneDeep(this.priceLevelsSell);


        var obBuys = this.buildOrderBookForSide(deepPriceLevelsBuy, "B");
        var obSell = this.buildOrderBookForSide(deepPriceLevelsSell, "S");

        var spread = 0;
        var vwapAsk = 0;

        if (obBuys.length > 0 && obSell.length > 0) {
          const bestBid = obBuys[0].price;
          const bestAsk = obSell[0].price;

          console.log("Best Bid", obBuys[0]);
          console.log("Best Ask", obSell[0]);
          spread = bestAsk - bestBid;

          // do vwap 
          var totalVol = 0;
          var sumBidQty = 0;


          for (var i=0; i<obBuys.length; i++) {
            sumBidQty += obBuys[i].price * obBuys[i].quantity;
            totalVol += obBuys[i].quantity;
          }
          const vwapBid = sumBidQty/totalVol;
          console.log(vwapBid);

        }

        var res = {
            buySide:obBuys,
            sellSide:obSell,
            lastExec:this.lastOrderExecuted,
            lastStatus:this.lastStatus,
            spread:spread,
        };

        // clearing state
        console.log("Done building", res);
        console.log("Clearing state");
        this.clearOrderState();
        console.log(res);

        // detect if we transitioned
        const cStatus = this.lastStatus;
        const nStatus = this.nextStatus;

        console.log("Current Status", cStatus);
        console.log("Next Status", nStatus);
        // Note the ininitial state changes from trading to regular to auction.
        // For now, detect change from auction -> regular.
        // so we can clear book

        if(cStatus.statusSymbol === "a" && nStatus.statusSymbol === "T") {
          console.log("About to TRANSITION FROM aution to Trading");
          this.clearEverything();
        }

        


        return res;


    }




}