import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';

import { 
  Layout,
  Menu,
  Breadcrumb,
  Divider,
  Card,
  Icon,
  Button,
  PageHeader,
  Input,
  Space,
  AutoComplete,
  Table,
  Descriptions,
  Select,
  Form,
  Slider, 
  InputNumber,
  Row,
  Col
} from 'antd';
import {
    AppstoreOutlined,
} from '@ant-design/icons';

import CONN from '../assets/data/conn.json';

// import DataView from './DataView';
// import pg from 'pg'

const BOLT_CONN = CONN.BOLT_CONN;
// const neo4j = require('neo4j-driver');
const axios = require('axios');

const { Header, Content, Footer } = Layout;

const { Search } = Input;

const FormItem = Form.Item;



class ReplayView extends Component {

    state = {
        options:[],
        symStatusCols:[],
        symStatusRows:[],
        addOrderRows:[],
        addOrderCols:[],
        addAttribOrderRows:[],
        addAttribOrderCols:[],
        orderDelRows:[],
        orderDelCols:[],
        orderModRows:[],
        orderModCols:[],
        orderExecRows:[],
        orderExecCols:[],
        orderExecPriceSzRows:[],
        orderExecPriceSzCols:[],
        tradeRows:[],
        tradeCols:[],
        auctionTradeRows:[],
        auctionTradeCols:[],
        offBookTradeRows:[],
        offBookTradeCols:[],
        timeRows:[],
        timeCols:[],
        inputValue:0,
        currentTime:"",
        selectedInstrument:"",
        currentSeqStart:0,
        currentSeqEnd:0,
        obBuyRows:[],
        obBuyCols:[],
        obSellRows:[],
        obSellCols:[],
    }

    formRef = React.createRef();



    componentDidMount() {
        document.title = "Replay";
        this.initData();

    }

    initData = () => {
        console.log("initing To quest");



        axios.get('http://127.0.0.1:8000/dbapi/search/').then(resp => {
            // console.log(resp.data);
            var respData = resp.data;

            console.log(respData);

            this.setState({
                options:respData.data
            });


            


        });


        
        axios.post('http://127.0.0.1:8000/dbapi/time/', {
          insid:"NED::201",
          msgType:"Test"
        }).then(resp => {

          var respData = resp.data;
          console.log(respData);

          this.setState({
              timeRows:respData.rows,
              timeCols:respData.columns,
          });

        });


    }

    handleSearch = (val) => {
        console.log(val);

        this.setState({
          selectedInstrument:val
        });



    }

    onSelect = (data) => {
      console.log('onSelect', data);
    };

    handleFrontSearch = (data) => {
        console.log('onSelect', data);
    }


    checkEmpty = (v) => {
        let type = typeof v;
        if (type === 'undefined') {
            return true;
        }
        if (type === 'boolean') {
            return !v;
        }
        if (v === null) {
            return true;
        }
        if (v === undefined) {
            return true;
        }
        if (v instanceof Array) {
            if (v.length < 1) {
                return true;
            }
        } else if (type === 'string') {
            if (v.length < 1) {
                return true;
            }
            if (v === '0') {
                return true;
            }
        } else if (type === 'object') {
            if (Object.keys(v).length < 1) {
                return true;
            }
            // check if object which has keys actually has values
            else {
                var ret = false;
                Object.values(v).forEach(x => {
                    if (x == null || x == "") {
                        ret = true;
                    }
                });
                return ret;
            }
        } else if (type === 'number') {
            if (v === 0) {
                return true;
            }
        }
        return false;
    }


    filterParams = (values) => {
        // Util func to filter out any 'null' or empty params

        // Filter the params so that we only consider non nulls and non empty strings ''
        var fVals = {};
        for (var param in values) {
            // console.log(param, values[param]);
            // if (values[param] == null || values[param] == "") {
            if (this.checkEmpty(values[param])) {
                // console.log("null param for", param);
            }
            else {
                fVals[param] = values[param];
            }
        }
        return fVals;
    }




    resetParamSelectionForm = () => {
        console.log("resetting form");

        // we can access the default form which ant created for us
        this.formRef.current.resetFields();
    }


    sliderOnChange = (value) => {
      console.log("New slider value", value);




      const timei = this.state.timeRows[value];
      var timeip1 = null;
      // check if we can access at value + 1
      if (this.state.timeRows[value+1] !== undefined) {
        timeip1 = this.state.timeRows[value+1];
      }


      console.log("Time data at ", value, timei, timeip1);

      var postData = {
        insid:this.state.selectedInstrument,
        seqidStart:timei.seq_id,
        seqidEnd:timeip1.seq_id
      };



      // get seconds in readible fmt
      var date = new Date(null);
      date.setSeconds(timei.seconds);
      var result = date.toISOString().substr(11, 8);
      console.log("Current time", result);
      console.log("Post data ", postData);

      axios.post('http://127.0.0.1:8000/dbapi/getaddordersseqids/', postData).then(resp => {
        var respData = resp.data;
        console.log(respData);

        // get buy and sell side into seperate arrays to pop table
        const addOrderRows = respData.rows;
        var buySide = addOrderRows.filter(item => item.side == "B");
        var sellSide = addOrderRows.filter(item => item.side == "S");
        // console.log(buySide);

        // build cols - note order is reversed for sell side
        // for col in df.columns:
        //     dcols  = {
        //         "title":str(col),
        //         "dataIndex":str(col),
        //         "key":str(col)
        //     }
        //     ls_cols.append(dcols)

        const buyCols = ["nanosecond", "order_id", "price", "quantity", "seq_id"];
        var buySideCols = []

        buyCols.forEach( item => {

          var d = {
            title:item,
            dataIndex:item,
            key:item,
            render(text, record) {
              return {
                props: {
                  style: { background: "#D9EAD3" }
                },
                children: <div>{text}</div>
              };
            }
          };
          buySideCols.push(d);

        });

        const sellCols = ["nanosecond", "order_id", "price", "quantity", "seq_id"];
        var sellSideCols = []

        sellCols.forEach( item => {

          var d = {
            title:item,
            dataIndex:item,
            key:item,
            render(text, record) {
              return {
                props: {
                  style: { background: "#F4CCCC" }
                },
                children: <div>{text}</div>
              };
            }
          };
          sellSideCols.push(d);

        });

        // cumulitively update the order
        const currentBuys = this.state.obBuyRows;
        var cumBuys = currentBuys.concat(buySide);


        const currentSells = this.state.obSellRows;
        var cumSells = currentSells.concat(sellSide);




        this.setState({
            addOrderRows:respData.rows,
            addOrderCols:respData.columns,
            obBuyCols:buySideCols,
            obBuyRows:cumBuys,
            obSellCols:sellSideCols,
            obSellRows:cumSells,
            // orderExecCols:respData.columnsExec,
            // orderExecRows:respData.rowsExec,

        });

      });


      console.log("getting exec orders");
      axios.post('http://127.0.0.1:8000/dbapi/getexecordersseqids/', postData).then(resp => {
        var respData = resp.data;
        console.log(respData);


        this.setState({
            orderExecCols:respData.columns,
            orderExecRows:respData.rows,

        });

      });



      this.setState({
        inputValue: value,
        currentTime:result,
        currentSeqStart:timei.seq_id,
        currentSeqEnd:timeip1.seq_id,
      });



    }


    handleClickStepBack = (event) => {
      console.log("Stepping back")
      // get current input value and decrease val by 1
      const cVal = this.state.inputValue;

      const back = cVal - 1;

      // trigger slider change event
      this.sliderOnChange(back);
    }


    handleClickStepFwd = (event) => {
      console.log("Stepping Fwd")
      // get current input value and decrease val by 1
      const cVal = this.state.inputValue;
      console.log(cVal, typeof cVal);

      const fwd = cVal + 1;
      console.log(fwd);

      this.sliderOnChange(fwd);

    }





    render() {

      // const { inputValue } = this.state;
      const { timeRows } = this.state;
      const { currentTime } = this.state;
      const { currentSeqStart } = this.state;
      const { currentSeqEnd } = this.state;

      var numTime = timeRows.length;


        return (
            <div>
                <Layout >
                  <Card style={{margin: '10px'}} className="cool-form-wrapper">

                        <Divider><h2>Select JSE Instrument to Replay</h2></Divider>
                        <AutoComplete
                          options={this.state.options}
                          style={{
                            width: '100%',
                          }}
                          onSelect={this.handleSearch}
                          filterOption={true}
                          
                        >
                          <Search size="large" placeholder="input here" allowClear enterButton />
                        </AutoComplete>


                    </Card>

                    <Content style={{ margin: '24px 16px 0'}}>
                        <br/>
                        <div className="cool-form-wrapper">                        
                          <Divider style={{ fontSize: '1.5em' }} orientation="left"> Time Slider </Divider>
                          <div>
                          <h3> {currentTime} </h3> <h5>SeqID {currentSeqStart} to {currentSeqEnd}</h5>

                          <Row>
                            <Col span={2}>
                              <Button style={{paddingRight:'10px'}} onClick={this.handleClickStepBack}> Step Back </Button>
                            </Col>
                            <Col span={20}>
                              <Slider
                                min={0}
                                max={numTime}
                                onChange={this.sliderOnChange}
                                value={this.state.inputValue}
                              />
                            </Col>
                            <Col span={2}>
                              <Button style={{paddingLeft:'10px'}} onClick={this.handleClickStepFwd}> Step Forward </Button>
                            </Col>
                          </Row>
                          </div>
                        </div>

                    </Content>


                    <Content style={{ margin: '24px 16px 0'}}>
                        <br/>
                        <div className="cool-form-wrapper">                        
                          <Divider style={{ fontSize: '1.5em' }} orientation="left"> Add Orders </Divider>
                          <div>
                          <Table dataSource={this.state.addOrderRows} columns={this.state.addOrderCols} size="small" bordered/>

                          </div>
                        </div>

                    </Content>


                    <Content style={{ margin: '24px 16px 0'}}>
                        <br/>
                        <div className="cool-form-wrapper">                        
                          <Divider style={{ fontSize: '1.5em' }} orientation="left"> Executed Orders </Divider>
                          <div>
                          <Table dataSource={this.state.orderExecRows} columns={this.state.orderExecCols} size="small" bordered/>

                          </div>
                        </div>

                    </Content>


                    <Content style={{ margin: '24px 16px 0'}}>
                        <br/>
                        <div className="cool-form-wrapper">                        
                          <Divider style={{ fontSize: '1.5em' }} orientation="left"> Order Book </Divider>
                          <Row>
                            <Col span={12}>

                              <div>
                              Buy
                              <Table dataSource={this.state.obBuyRows} columns={this.state.obBuyCols} size="small" bordered />

                              </div>
                            </Col>
                            <Col span={12}>

                              <div>
                              Sell
                              <Table dataSource={this.state.obSellRows} columns={this.state.obSellCols} size="small" bordered/>

                              </div>
                            </Col>

                          </Row>

                        </div>

                    </Content>






                </Layout>
            </div>
        );
    }
}

// map the state to props
const mapStateToProps = state => {
    return {
        token: state.token

    }
}


export default withRouter(connect(mapStateToProps)(ReplayView));