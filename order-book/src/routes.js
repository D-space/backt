import React from 'react';
import { Route } from 'react-router-dom';


import Login from './containers/Login';
import HomeView from './containers/HomeView';
import ReplayView from './containers/ReplayView';

const BaseRouter = () => (
    <div>
        <Route exact path="/" component={Login} />
        <Route exact path="/home" component={HomeView} />
        <Route exact path="/replay" component={ReplayView} />
    </div>
);

export default BaseRouter;