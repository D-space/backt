//Helper file to update object data 
//Below will clone and replace old obj props with new props
export const updateObject = (oldObject, updatedProperties) => {
    return {
        ...oldObject,
        ...updatedProperties
    }
}