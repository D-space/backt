class ParserGen
  def self.generate unit_header, admin_msgs, app_msgs, dir
    destdir = File.join(dir)
    Dir.mkdir(destdir) unless File.exists? destdir

    # basemsgstr = gen_basemsg(destdir)
    # basemsg_path = File.join(destdir, "Message.cs")
    # puts "generate #{basemsg_path}"
    # File.open(basemsg_path, 'w') {|f| f.puts(basemsgstr) }

    puts "generate parser #{destdir}*"

    parserstr = gen_parser(unit_header, admin_msgs, app_msgs)
    parser_path = File.join(destdir, 'MITCHParserGen.cs')
    # puts "generate #{msg_path}"
    File.open(parser_path, 'w') {|f| f.puts(parserstr) }
  end




  def self.gen_parser unit_header, admin_msgs, app_msgs

    # puts "Fields #{fields}"

    s = <<HERE
// This is a generated file.  Don't edit it directly!

using System;
using System.IO;
using System.Text;

namespace backt
{
    public static class MITCHParserGen
    {
        public static void ParseMessage(ExtractionMessage? messageWithHeader, QuestConnGen qdb)
        {
            using (var memoryStream = new MemoryStream(messageWithHeader.Value.Data))
            {
                using (var dataBinaryReader = new BinaryReader(memoryStream))
                {
                    switch(messageWithHeader.Value.MessageType)
                    {
#{gen_cases_app_msgs(app_msgs)}
HERE


s += <<HERE2
                    }
                }
            }
        }
#{gen_display_app_msgs(app_msgs)}
    }
}
HERE2
    s
  end



  def self.gen_display_app_msgs app_msgs
    app_msgs.map { |fld| build_display(fld) }.join("\n")
  end


  def self.build_display msg
    msg_typ_hex = msg[:messagetypehex].delete('0x').to_i
    c = <<HERE
        public static void DisplayMessage(#{msg[:alias]}Message msg) 
        {
            var s = $@"
#{build_display_fields(msg[:fields])}
            ";
            Console.WriteLine(s);
        }
HERE
  c
  end


  def self.build_display_fields msg_flds
    msg_flds.map { |fld| build_display_fields_str(fld) }.join("\n")
  end

  def self.build_display_fields_str fld
    if fld[:type] == ""
      c = ""
    else
      c = <<HERE
            #{fld[:alias]} = {msg.#{fld[:alias]}}
HERE
    end
  c
  end


  def self.gen_cases_app_msgs app_msgs
    puts "Hi"
    app_msgs.map { |fld| build_case(fld) }.join("\n")
  end

  def self.build_case msg
    msg_typ_hex = msg[:messagetypehex].delete('0x').to_i
    c = <<HERE
                        case "#{msg_typ_hex}":
                            var #{msg[:alias]}MessageBody = #{msg[:alias]}Message.Build#{msg[:alias]}Message(dataBinaryReader, messageWithHeader.Value.Id);
                            DisplayMessage(#{msg[:alias]}MessageBody);
                            qdb.Insert_#{msg[:alias]}(#{msg[:alias]}MessageBody, messageWithHeader.Value.Id);
                            break;
HERE
  c
  end




  def self.gen_msg_builder_uheader msg_name, fields, prepend_spaces
    # TODO: maybe process unitheader
    str = []
    # just read for now, dont decode. As per example
    str << "var UnitHeader = binaryReader.ReadBytes(8);"
    # Then add decode code
    # str << "var #{fld_alias} = Encoding.ASCII.GetString(#{fBuffer});"
    str << "\n"



    str.map! {|s| ' '*prepend_spaces + s}
    str.join("\n")

  end


end