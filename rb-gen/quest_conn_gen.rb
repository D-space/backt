class QuestConnGen
  def self.generate unit_header, admin_msgs, app_msgs, dir
    destdir = File.join(dir)
    Dir.mkdir(destdir) unless File.exists? destdir

    # basemsgstr = gen_basemsg(destdir)
    # basemsg_path = File.join(destdir, "Message.cs")
    # puts "generate #{basemsg_path}"
    # File.open(basemsg_path, 'w') {|f| f.puts(basemsgstr) }

    puts "generate quest connection #{destdir}*"

    qconnstr = gen_quest_conn(unit_header, admin_msgs, app_msgs)
    qconn_path = File.join(destdir, 'QuestConnGen.cs')
    # puts "generate #{msg_path}"
    File.open(qconn_path, 'w') {|f| f.puts(qconnstr) }
  end




  def self.gen_quest_conn unit_header, admin_msgs, app_msgs

    # puts "Fields #{fields}"

    s = <<HERE
// This is a generated file.  Don't edit it directly!

using System;
using System.IO;
using System.Text;
using Npgsql;

namespace backt
{
    public class QuestConnGen
    {

        public NpgsqlConnection conn;


        public QuestConnGen(string username, string password, string database, int port)
        {

            string connectionString = $"host=127.0.0.1;port={port};username={username};password={password};database={database};ServerCompatibilityMode=NoTypeLoading;";
            Console.WriteLine("New Quest connection");
            conn = new NpgsqlConnection(connectionString);
            conn.Open();
            Console.WriteLine("Opened");
        }


        public void DeleteTables()
        {
#{gen_delete_app_tables(app_msgs)}
        }


        public void SetupTables()
        {
#{gen_setup_app_tables(app_msgs)}
HERE


s += <<HERE2
        }
#{gen_insert_sql_app_msgs(app_msgs)}
    }
}
HERE2
    s
  end


  def self.gen_insert_sql_app_msgs app_msgs
    app_msgs.map { |fld| build_insert_funcs(fld) }.join("\n")
  end


  def self.build_insert_funcs msg
    msg_alias = msg[:alias]
    c = <<HERE
        public void Insert_#{msg_alias}(#{msg_alias}Message msgObj, long msgOrderSeqId)
        {
            using( var cmd = new NpgsqlCommand("INSERT INTO #{msg_alias} (seq_id, #{build_msg_fields_insert_funcs_query(msg[:fields])}) VALUES (@s, #{build_msg_values_insert_funcs_query(msg[:fields])}) ;", conn))
            {
                cmd.Parameters.AddWithValue("s", msgOrderSeqId);

#{build_msg_param_insert_funcs_query(msg[:fields], 16)}

                cmd.ExecuteNonQuery();
            }
        }
HERE
  c
  end

  def self.build_msg_param_insert_funcs_query msg_flds, prepend_spaces
    msg_flds.map { |fld| build_msg_param_insert_funcs_query_str(fld, prepend_spaces) }.join("\n")
  end

  def self.build_msg_param_insert_funcs_query_str fld, prepend_spaces
    fld_alias = fld[:alias].delete(' ').downcase
    if fld[:type] == ""
      # If type is not properly defined skip inserting
      str = []
      str << ""
      str.map! {|s| ' '*prepend_spaces + s}
      str.join("\n")
    else
      # Map c# field type to questdb types
      case fld[:type]
      when 'alpha'
        # map back to str
        str = []
        str << "cmd.Parameters.AddWithValue(\"#{fld_alias}\", (string) msgObj.#{fld[:alias]});"
        str.map! {|s| ' '*prepend_spaces + s}
        str.join("\n")

      when 'bitfield'
        str = []
        str << "cmd.Parameters.AddWithValue(\"#{fld_alias}\", (byte) msgObj.#{fld[:alias]});"
        str.map! {|s| ' '*prepend_spaces + s}
        str.join("\n")

      when 'byte'
        str = []
        str << "cmd.Parameters.AddWithValue(\"#{fld_alias}\", (string) msgObj.#{fld[:alias]});"
        str.map! {|s| ' '*prepend_spaces + s}
        str.join("\n")

      when 'date'
        str = []
        str << "cmd.Parameters.AddWithValue(\"#{fld_alias}\", (string) msgObj.#{fld[:alias]});"
        str.map! {|s| ' '*prepend_spaces + s}
        str.join("\n")

      when 'time'
        str = []
        str << "cmd.Parameters.AddWithValue(\"#{fld_alias}\", (string) msgObj.#{fld[:alias]});"
        str.map! {|s| ' '*prepend_spaces + s}
        str.join("\n")

      when 'price'
        str = []
        str << "cmd.Parameters.AddWithValue(\"#{fld_alias}\", (long) msgObj.#{fld[:alias]});"
        str.map! {|s| ' '*prepend_spaces + s}
        str.join("\n")

      when 'uint8'
        str = []
        str << "cmd.Parameters.AddWithValue(\"#{fld_alias}\", (int) msgObj.#{fld[:alias]});"
        str.map! {|s| ' '*prepend_spaces + s}
        str.join("\n")

      when 'uint16'
        str = []
        str << "cmd.Parameters.AddWithValue(\"#{fld_alias}\", (int) msgObj.#{fld[:alias]});"
        str.map! {|s| ' '*prepend_spaces + s}
        str.join("\n")


      when 'uint32'
        str = []
        str << "cmd.Parameters.AddWithValue(\"#{fld_alias}\", (long) msgObj.#{fld[:alias]});"
        str.map! {|s| ' '*prepend_spaces + s}
        str.join("\n")

      when 'uint64'
        str = []
        str << "cmd.Parameters.AddWithValue(\"#{fld_alias}\", (string) msgObj.#{fld[:alias]});"
        str.map! {|s| ' '*prepend_spaces + s}
        str.join("\n")

      else
        # unsure of datatype so just map back to og
        str = []
        str << "cmd.Parameters.AddWithValue(\"#{fld_alias}\", msgObj.#{fld[:alias]});"
        str.map! {|s| ' '*prepend_spaces + s}
        str.join("\n")
      end

    end
  end

  def self.build_msg_values_insert_funcs_query msg_flds
    msg_flds.map { |fld| build_msg_values_insert_funcs_query_str(fld) }.join(", ")
  end

  def self.build_msg_values_insert_funcs_query_str fld
    fld_alias = fld[:alias].delete(' ').downcase
    s = "@#{fld_alias}"
    s
  end

  def self.build_msg_fields_insert_funcs_query msg_flds
    msg_flds.map { |fld| build_msg_fields_insert_funcs_query_str(fld) }.join(", ")
  end

  def self.build_msg_fields_insert_funcs_query_str fld
    fld_alias = fld[:alias].delete(' ').downcase
    s = "#{fld_alias}"
    s
  end


  def self.gen_delete_app_tables app_msgs
    app_msgs.map { |fld| build_delete_table(fld) }.join("\n")
  end


  def self.build_delete_table msg
    msg_alias = msg[:alias]
    c = <<HERE
            using( var cmd = new NpgsqlCommand("DROP TABLE #{msg_alias}", conn))
            {
                cmd.ExecuteNonQuery();
            }
HERE
  end



  def self.gen_setup_app_tables app_msgs
    app_msgs.map { |fld| build_setup_table(fld) }.join("\n")
  end


  def self.build_setup_table msg
    msg_alias = msg[:alias]
    c = <<HERE
            using( var cmd = new NpgsqlCommand("CREATE TABLE IF NOT EXISTS #{msg_alias} (seq_id LONG, #{build_msg_fields_query(msg[:fields])});", conn))
            {
                cmd.ExecuteNonQuery();
            }
HERE
  end

  def self.build_msg_fields_query msg_flds
    msg_flds.map { |fld| build_msg_fields_query_str(fld) }.join(", ")
  end


  def self.build_msg_fields_query_str fld
    fld_alias = fld[:alias].delete(' ').downcase
    s = ""
    case fld[:type]
    when 'alpha'
      # bitfield maps to a single byte in c#
      s = "#{fld_alias} STRING"
    when 'bitfield'
      # bitfield maps to a single byte in c#
      s = "#{fld_alias} BYTE"
    when 'byte'
      s = "#{fld_alias} STRING"
    when 'date'
      # date maps to 8bytes
      s = "#{fld_alias} STRING"

    when 'time'
      # date maps to 8bytes
      s = "#{fld_alias} STRING"
    when 'price'
      s = "#{fld_alias} LONG"

    when 'uint8'
      s = "#{fld_alias} INT"

    when 'uint16'
      s = "#{fld_alias} INT"


    when 'uint32'
      # Use long for now
      s = "#{fld_alias} LONG"

    when 'uint64'
      # Store as string. Only OrderID and TradeID use this
      s = "#{fld_alias} STRING"

    else
      # type unknown skip
      puts "Bad value #{fld[:type]}"
      # s = ""

    end
    s
  end


  def self.build_display_fields msg_flds
    msg_flds.map { |fld| build_display_fields_str(fld) }.join("\n")
  end

  def self.build_display_fields_str fld
    if fld[:type] == ""
      c = ""
    else
      c = <<HERE
            #{fld[:alias]} = {msg.#{fld[:alias]}}
HERE
    end
  c
  end


  def self.gen_cases_app_msgs app_msgs
    puts "Hi"
    app_msgs.map { |fld| build_case(fld) }.join("\n")
  end

  def self.build_case msg
    msg_typ_hex = msg[:messagetypehex].delete('0x').to_i
    c = <<HERE
                        case "#{msg_typ_hex}":
                            var #{msg[:alias]}MessageBody = #{msg[:alias]}Message.Build#{msg[:alias]}Message(dataBinaryReader, messageWithHeader.Value.Id);
                            DisplayMessage(#{msg[:alias]}MessageBody);
                            break;
HERE
  c
  end




  def self.gen_msg_builder_uheader msg_name, fields, prepend_spaces
    # TODO: maybe process unitheader
    str = []
    # just read for now, dont decode. As per example
    str << "var UnitHeader = binaryReader.ReadBytes(8);"
    # Then add decode code
    # str << "var #{fld_alias} = Encoding.ASCII.GetString(#{fBuffer});"
    str << "\n"



    str.map! {|s| ' '*prepend_spaces + s}
    str.join("\n")

  end


end